import MCAPI from '../data/mycard';

function setUserData(userData) {
  return {
    type : 'SET_USER_DATA',
    userData
  };
};

function setLocation(locationData){
  return {
    type: 'SET_CURRENT_LOCATION',
    locationData
  };
};

function _updateVcard(alt_doc_id, newData){
  return {
    type: 'UPDATE_VCARD',
    alt_doc_id,
    newData,
  };
};

function _setVcards(vcards){
  return {
    type: 'SET_VCARDS',
    vcards,
  };
};

function _addHistory(data){
  return {
    type: 'ADD_HISTORY',
    data,
  };
};


// Fetch and set the user from the server
export function fetchUserData(userid) {
  return function (dispatch, getState) {
    console.log("Getting user data: " + userid);
    return MCAPI.getUserData(userid)
      .then((response) => response.json())
      .then((userData) => dispatch(setUserData(userData)))
      .catch((err) => console.log(err));
  };
};

// Set the user from passed data
export function setUserDataState(data) {
  return function (dispatch, getState) {
    console.log("Setting user data state: " + data.userid);
    dispatch(setUserData(data));
  };
};

// Set the location from passed data
export function setCurrentLocation(data){
  return function(dispatch, getState){
    dispatch(setLocation(data));
  };
};

export function updateVcard(alt_doc_id, newData){
  return function(dispatch, getState){
    dispatch(_updateVcard(alt_doc_id, newData));
  };
};

export function setVcards(vcards){
  return function(dispatch, getState){
    dispatch(_setVcards(vcards));
  };
};

export function addHistory(data){
  return function(dispatch, getState){
    dispatch(_addHistory(data));
  };
};


