import React, { Component } from 'react';
import {
  AppRegistry,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
  Linking,
  StyleSheet,
  Text,
  View,
  Alert,
  TextInput,
  Image,
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators} from 'redux';
import * as mycardActions from '../actions/account';



import utils from '../utils'
import Button from 'react-native-button'
import { API_URL } from 'react-native-dotenv'

import {AppEventsLogger} from 'react-native-fbsdk';
var ls = require('react-native-local-storage');

import TokenSendCode from '../components/TokenSendCode';

class SignIn extends Component {
  static navigationOptions = {
    header: null 
  }

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      loginInProgress: false,
      showTokenModal: false,
      failedLoginCount: 0,
      loginButtonTimeoutId: null,
    };

    this.__signIn = this.__signIn.bind(this);
  }

  componentDidMount(){
    this.setState({loginInProgress: false});
    AppEventsLogger.logEvent('Viewed SignIn Page');
    utils.logEvent('viewed_signin_page');
  }

  __signIn(token){
    const { navigate } = this.props.navigation;

    if(this.state.loginInProgress){
      if(this.state.loginButtonTimeoutId !== null){
        return;
      }

      let loginButtonTimeoutId = setTimeout( () => {
        this.setState({loginButtonTimeoutId: null});
        this.setState({loginInProgress: false});
      }, 10000);
      return;
    }

    if(this.state.email === ''){
      Alert.alert('Please enter your Vistaprint email address');
      return;
    }

    if(this.state.password === '' && (token === null || token === '')){
      Alert.alert('Please enter your Vistaprint password');
      return;
    }

    this.setState({loginInProgress: true});

    fetch(API_URL + '/login', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email,
        token: token,
        password: this.state.password,
      })
    })
    .then(function(response){
      return response.json();
    })
    .then(function(response){
      if(typeof response.userid !== 'undefined'){
        this.setState({loginInProgress: false});

        ls.save('serverData', response);
        this.props.setUserDataState(response);

        utils.bugsnagUser(response.userid, response.first_name + ' ' + response.last_name, response.email);
        navigate('Tutorial');

      }else{
        this.setState({failedLoginCount: this.state.failedLoginCount++});
        throw 'Login failed';
      }
    }.bind(this))
    .catch(function(e){
      this.setState({loginInProgress: false});
      this.setState({password: ''});

      if(this.state.loginButtonTimeoutId !==  null){
        clearTimeout(this.state.loginButtonTimeoutId);
        this.setState({loginButtonTimeoutId: null});
      }

      this.setState({failedLoginCount: this.state.failedLoginCount+1});
      if(this.state.failedLoginCount >= 2){
        this.setState({showTokenModal: true});
      }

      Alert.alert("Login failed: Please try again");
    }.bind(this));

  }

  render() {
    const { navigate } = this.props.navigation;
    let buttonText = 'ADD MY CARD';

    if(this.state.loginInProgress){
      buttonText = <ActivityIndicator size='large' color='#ffffff' />
    }

    let tokenModal = <View></View>;
    if(this.state.showTokenModal){
      tokenModal = <TokenSendCode
        email={this.state.email}
        close={ () => this.setState({showTokenModal: false}) }
        signIn={ this.__signIn }
        loginInProgress={ this.state.loginInProgress }
      />
    }

    return (
      <View style={styles.container}>
        <View style={styles.backgroundContainer}>
          <Image
            source={require('../images/background.png')}
            style={styles.background}
          />
        </View>

        <Image
          source={require('../images/logo.png')}
          style={styles.logo}
        />

        <ScrollView style={{width: '100%'}}>

        <TextInput
          style={styles.inputField}
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
          autoCapitalize='none'
          keyboardType='email-address'
          placeholder='Vistaprint login email'
          placeholderTextColor='#c5dbe9'
          underlineColorAndroid='rgba(0,0,0,0)'
          selectionColor='white'
          autoCorrect={false}
        />

        <TextInput
          style={styles.inputField}
          onChangeText={(password) => this.setState({password})}
          secureTextEntry={true}
          value={this.state.password}
          autoCapitalize='none'
          placeholder='Password'
          placeholderTextColor='#c5dbe9'
          underlineColorAndroid='rgba(0,0,0,0)'
          selectionColor='white'
          onSubmitEditing={ () => this.__signIn() }
        />

        <Button
          onPress={() => this.__signIn()}
          accessibilityLabel="Sign in using your vistaprint credentials"
          containerStyle={{marginTop: 37, padding:14, height:50, overflow:'hidden', borderRadius:4, backgroundColor: '#1ea7e2', width: '100%'}}
          style={{fontSize: 16, color: 'white'}}>
            { buttonText }
        </Button>

        <TouchableOpacity onPress={() => this.setState({showTokenModal: true})}>
          <Text style={styles.footer}>Having login trouble? <Text style={{fontWeight: '600'}}>Get a temporary pin</Text></Text>
        </TouchableOpacity>

      </ScrollView>

      <View>
        <TouchableOpacity onPress={() => navigate('CreateAccount')}>
          <Text style={styles.footer}>Don&apos;t have a Vistaprint Account?</Text>
          <Text style={styles.footerStrong}>Create one now</Text>
        </TouchableOpacity>
      </View>

      {tokenModal}
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: 40,
    alignItems: 'center',
    backgroundColor: '#006098',
    padding: 20,
  },
  backgroundContainer: {
    position: 'absolute',
    left: -10,
    right: 0,
  },
  background: {
    flex: 1,
    width: '100%',
    resizeMode: 'cover',
  },
  logo: {
    resizeMode: 'contain',
    marginBottom: 30,
    width: 150,
  },
  inputField: {
    height: 45,
    width: '100%',
    borderColor: 'gray',
    borderWidth: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.15)',
    borderRadius: 4,
    padding: 10,
    marginTop: 7,
    color: '#ffffff',
  },
  button: {
    backgroundColor: "#FF0000"
  },
  footer: {
    color: '#FFFFFF',
    marginTop: 20,
    textAlign: 'center',
  },
  footerStrong: {
    color: '#FFFFFF',
    marginTop: 5,
    fontWeight: '600',
    textAlign: 'center',
  },
});



export default connect(
  state => ({
    userData: state.mycard.userData
  }),
  dispatch => bindActionCreators(mycardActions, dispatch)
)(SignIn);
