import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  Text,
  View,
  Button,
  Alert,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import utils from '../../utils';

import Header from '../../components/Header';
import FullWidthLink from '../../components/FullWidthLink';
var ls = require('react-native-local-storage');
import {AppEventsLogger} from 'react-native-fbsdk';

export default class SecretSettings extends Component {
  static navigationOptions = {
    header: null 
  };

  constructor(props) {
    super(props);
    this.state = {
      savedState: {},
    };
  }

  componentDidMount(){
    AppEventsLogger.logEvent('Viewed Secret Settings Page');
    utils.logEvent('viewed_secret_settings_page');

    ls.get('serverData').then( (serverData) => {
      this.setState({savedState: serverData});
    });
  }

  render() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;

    return (
      <View>
        <Header title='Developer Settings' />

        <View style={styles.container}>
          <Text>Saved State</Text>
          <ScrollView>
            <Text>{JSON.stringify(this.state.savedState, null, 2)}</Text>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
    height: '100%',
    paddingTop: 20,
  },
});

module.exports = SecretSettings;
