import React, { Component } from 'react';
import {
  AppRegistry,
  ActivityIndicator,
  TouchableOpacity,
  Linking,
  StyleSheet,
  Text,
  View,
  Alert,
  TextInput,
  Image,
} from 'react-native';
import utils from '../../utils'
import Button from 'react-native-button'
import { API_URL } from 'react-native-dotenv'

import {AppEventsLogger} from 'react-native-fbsdk';
var ls = require('react-native-local-storage');

import TokenSendCode from '../../components/TokenSendCode';

module.exports = class CreateAccountConfirmation extends Component {
  static navigationOptions = {
    header: null 
  }

  constructor(props) {
    super(props);
  }

  componentDidMount(){
    AppEventsLogger.logEvent('Viewed CreateAccount Confirmation Page');
    utils.logEvent('viewed_createaccount_confirmation_page');
  }

  render() {
    let buttonText = 'GET STARTED';

    return (
      <View style={styles.container}>
        <View style={styles.backgroundContainer}>
          <Image
            source={require('../../images/background.png')}
            style={styles.background}
          />
        </View>
        <Image
          source={require('../../images/logo.png')}
          style={styles.logo}
        />

        <Text style={{color: '#ffffff', fontSize: 21, fontWeight: '600', textAlign: 'center'}}>Congratulations!</Text>
        <Text style={{color: '#ffffff', fontSize: 21, textAlign: 'center', marginTop: 10}}>We just sent you a confirmation email</Text>


        <Text style={{color: '#ffffff', fontSize: 15, textAlign: 'center', marginTop: 65, marginLeft: 60, marginRight: 60}}>Want to start designing your business card now?</Text>
        <Button
          onPress={() => Linking.openURL('https://www.vistaprint.com') }
          accessibilityLabel="Get started"
          containerStyle={{marginTop: 27, padding:14, height:50, overflow:'hidden', borderRadius:4, backgroundColor: '#1ea7e2', width: '100%'}}
          style={{fontSize: 16, color: 'white'}}>
            { buttonText }
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 40,
    alignItems: 'center',
    backgroundColor: '#006098',
    padding: 20,
  },
  backgroundContainer: {
    position: 'absolute',
    left: -10,
    right: 0,
  },
  background: {
    flex: 1,
    width: '100%',
    resizeMode: 'cover',
  },
  logo: {
    marginBottom: 30
  },
  label: {
    textAlign: 'left',
    color: '#333333',
    marginTop: 10,
  },
  inputField: {
    height: 45,
    width: '100%',
    borderColor: 'gray',
    borderWidth: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.15)',
    borderRadius: 4,
    padding: 10,
    marginTop: 7,
    color: '#ffffff',
  },
  button: {
    backgroundColor: "#FF0000"
  },
});
