import React, { Component } from 'react';
import {
  AppRegistry,
  Platform,
  Dimensions,
  Keyboard,
  StyleSheet,
  Text,
  ScrollView,
  WebView,
  View,
  Alert,
  TextInput,
  Image,
  TouchableOpacity,
  Linking,
  KeyboardAvoidingView,
} from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators} from 'redux';
import * as mycardActions from '../actions/account';


import Button from 'react-native-button'

import Header from '../components/Header';

var ls = require('react-native-local-storage');

import {AppEventsLogger} from 'react-native-fbsdk';
import { SERVER_ROOT, API_URL } from 'react-native-dotenv'
import utils from '../utils';

class Settings extends Component {

  static navigationOptions = {
    header: null 
  };

  constructor(props) {
    super(props);

    let vcard = {};
    if(this.props.vcardIndex >= 0){
      vcard = this.props.vcards[this.props.vcardIndex];
    }

    this.state = {
      name: vcard.name || '',
      business_name: vcard.business_name || '',
      phone: vcard.phone || '',
      email: vcard.email || '',
      website: vcard.website || '',
      keyboardShown: false,
      kavKey: Date.now(),
      scrollOffset: 0,
    };

    this.handleTextUpdate = this.handleTextUpdate.bind(this);
    this.handleFinish = this.handleFinish.bind(this);
    this.keyboardWillShow = this.keyboardWillShow.bind(this);
    this.keyboardWillHide = this.keyboardWillHide.bind(this);
    this.scrollTo = this.scrollTo.bind(this);
  }

  componentWillMount(){
    console.log("registering");
    Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
    Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);
  }


  keyboardWillShow (e) {
    this.setState({keyboardShown: true});

    if(Platform.OS !== 'ios'){
      this.setState({kavKey: Date.now()});
    }
  }

  keyboardWillHide (e) {
    this.setState({keyboardShown: false});
    this.setState({scrollOffset: 0});

    if(Platform.OS !== 'ios'){
      this.setState({kavKey: Date.now()});
    }
  }

  handleTextUpdate(name, value){
    switch(name){
      case 'name':
        this.setState({name: value});
        break;
      case 'business_name':
        this.setState({business_name: value});
        break;
      case 'phone':
        this.setState({phone: value});
        break;
      case 'email':
        this.setState({email: value});
        break;
      case 'website':
        this.setState({website: value});
        break;
    }
  }

  handleFinish(){
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;

    utils.logEvent('clicked_contact_finish');

    let alt_doc_id = '';
    
    try{
      if(this.props.userData.default_card){
        alt_doc_id = this.props.userData.default_card.alt_doc_id;
      }else if(params.card){
        alt_doc_id = params.card.alt_doc_id;
      }else{
        // uh oh
        utils.logEvent('contact_edit_no_card');
      }
  
      let newData = this.state;
      this.props.updateVcard(alt_doc_id, newData);
  
      if(params.goHome){
        navigate('CardHome');
      }else{
        this.props.navigation.goBack();
      }
    }catch(ex){
      utils.error('error_contact_info');
      navigate('CardHome');
    }
  }

  scrollTo(ref){
    this.setState({scrollOffset: this.state.scrollOffset + 60});
    ref.focus();
  }

  render(){
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;

    let vcard = {};
    if(this.props.vcardIndex >= 0){
      vcard = this.props.vcards[this.props.vcardIndex];
    }

    let containerStyle = {flex: 1};
    let inputHeight = 52;
    let iconTop = 50;
    let scrollViewStyle = {};

    let headerText = 'Enter your contact information that will be sent with your business card';
    if(Platform.OS === 'ios' && this.state.keyboardShown){
      containerStyle.paddingBottom = 350;
      scrollViewStyle.paddingBottom = 350;
      //inputHeight = 50;
      //headerText = '';
      //iconTop = 42;
    }else{
      containerStyle.paddingBottom = 0;
      scrollViewStyle.paddingBottom = 0;
    }

    return(

      <View style={containerStyle}>
        <Header title='Your information' rightIcon='skip' rightPress={() => params.goHome ? navigate('CardHome') : this.props.navigation.goBack()} />

        <KeyboardAvoidingView key={this.state.kavKey} behavior='height' style={{flex: 1, flexDirection: 'column', justifyContent: 'space-around'}}>
          <ScrollView contentOffset={{ x: 0, y: this.state.scrollOffset}} contentContainerStyle={scrollViewStyle}>
          <Text style={styles.title}>{headerText}</Text>

          <View>
            <Text style={styles.headerText}>Name</Text>
            <TextInput
              ref='name'
              returnKeyType='next'
              style={[styles.input, {height: inputHeight}, {fontStyle: (this.state.name === '' ? 'italic' : 'normal')}]}
              placeholder='Enter your name'
              placeholderTextColor='#b5babf'
              autoCapitalize='none'
              autoCorrect={false}
              value={this.state.name}
              underlineColorAndroid='rgba(0,0,0,0)'
              onChangeText={ (val) => this.handleTextUpdate('name', val) }
              onSubmitEditing={ (evt) => { this.scrollTo(this.refs.business) }}
              blurOnSubmit={false}
            />
            <Image source={require('../images/contact-edit-icon-user.png')} style={{position: 'absolute', top: iconTop, left: 30, width: 17, height: 18}} />
          </View>

          <View>
            <Text style={styles.headerText}>Business Name</Text>
            <TextInput
              ref='business'
              returnKeyType='next'
              style={[styles.input, {height: inputHeight}, {fontStyle: (this.state.business_name === '' ? 'italic' : 'normal')}]}
              placeholder='Enter your business name'
              placeholderTextColor='#b5babf'
              autoCapitalize='none'
              autoCorrect={false}
              value={this.state.business_name}
              underlineColorAndroid='rgba(0,0,0,0)'
              onChangeText={ (val) => this.handleTextUpdate('business_name', val) }
              onSubmitEditing={ (evt) => { this.scrollTo(this.refs.phone) }}
              blurOnSubmit={false}
            />
            <Image source={require('../images/contact-edit-icon-business.png')} style={{position: 'absolute', top: iconTop, left: 30, width: 17, height: 18}} />
          </View>

          <View>
            <Text style={styles.headerText}>Phone Number</Text>
            <TextInput
              ref='phone'
              returnKeyType='next'
              style={[styles.input, {height: inputHeight}, {fontStyle: (this.state.phone === '' ? 'italic' : 'normal')}]}
              placeholder='Enter your contact number'
              //keyboardType='phone-pad'
              placeholderTextColor='#b5babf'
              autoCapitalize='none'
              autoCorrect={false}
              value={this.state.phone}
              underlineColorAndroid='rgba(0,0,0,0)'
              onChangeText={ (val) => this.handleTextUpdate('phone', val) }
              onSubmitEditing={ (evt) => { this.scrollTo(this.refs.email) }}
              blurOnSubmit={false}
            />
            <Image source={require('../images/contact-edit-icon-phone.png')} style={{position: 'absolute', top: iconTop, left: 30, width: 18, height: 18}} />
          </View>

          <View>
            <Text style={styles.headerText}>Email Address</Text>
            <TextInput
              ref='email'
              returnKeyType='next'
              style={[styles.input, {height: inputHeight}, {fontStyle: (this.state.email === '' ? 'italic' : 'normal')}]}
              placeholder='Enter your email address'
              keyboardType='email-address'
              placeholderTextColor='#b5babf'
              autoCapitalize='none'
              autoCorrect={false}
              underlineColorAndroid='rgba(0,0,0,0)'
              value={this.state.email}
              onChangeText={ (val) => this.handleTextUpdate('email', val) }
              onSubmitEditing={ (evt) => { this.scrollTo(this.refs.website) }}
              blurOnSubmit={false}
            />
            <Image source={require('../images/contact-edit-icon-email.png')} style={{position: 'absolute', top: iconTop, left: 30, width: 24, height: 15}} />
          </View>

          <View>
            <Text style={styles.headerText}>Website URL</Text>
            <TextInput
              ref='website'
              returnKeyType='done'
              style={[styles.input, {height: inputHeight}, {fontStyle: (this.state.website === '' ? 'italic' : 'normal')}]}
              placeholder='Enter your website URL'
              placeholderTextColor='#b5babf'
              autoCapitalize='none'
              autoCorrect={false}
              underlineColorAndroid='rgba(0,0,0,0)'
              value={this.state.website}
              onChangeText={ (val) => this.handleTextUpdate('website', val) }
              //onSubmitEditing={this.handleFinish}
            />
            <Image source={require('../images/contact-edit-icon-website.png')} style={{position: 'absolute', top: iconTop, left: 30, width: 22, height: 18}} />
          </View>


        <Button
          onPress={this.handleFinish}
          accessibilityLabel="Finish editing contact"
          containerStyle={{marginTop: 30, padding:14, height:50, overflow:'hidden', borderTopLeftRadius:4, borderBottomLeftRadius: 4, borderTopRightRadius: 4, backgroundColor: '#1ea7e2', marginBottom: 20, width: '80%', marginLeft: '10%', justifyContent: 'center', alignItems: 'center'}}
          style={{fontSize: 16, color: 'white'}}>
            FINISH
        </Button>

          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    color: '#38454f',
    fontSize: 15,
    marginTop: 35,
    marginBottom: 25,
    paddingLeft: 40,
    paddingRight: 40,
    fontWeight: '500',
    textAlign: 'center',
  },
  headerText: {
    fontSize: 15,
    color: '#858f97',
    paddingTop: 10,
    paddingLeft: 16,
    marginBottom: 4,
  },
  input: {
    width: '90%',
    backgroundColor: '#ffffff',
    marginLeft: 16,
    borderWidth: 1,
    borderColor: '#d8d9db',
    color: '#38454f',
    paddingLeft: 60,
    marginBottom: 20,
  }
});

export default connect(
  state => ({
    userData: state.mycard.userData,
    vcardIndex: state.mycard.vcards.findIndex( i => i.alt_doc_id === state.mycard.userData.default_card.alt_doc_id ),
    vcards: state.mycard.vcards,
  }),
  dispatch => bindActionCreators(mycardActions, dispatch)
)(Settings);

