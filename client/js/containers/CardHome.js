import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Animated,
  TouchableWithoutFeedback,
  Text,
  Easing,
  ScrollView,
  ActivityIndicator,
  View,
  Alert,
  TextInput,
  TouchableOpacity,
  Platform,
  Image,
} from 'react-native';

var Orientation = require('react-native-orientation');

import Button from 'react-native-button'
import {AppEventsLogger} from 'react-native-fbsdk';

import Geocoder from 'react-native-geocoder';
Geocoder.fallbackToGoogle('AIzaSyB5rMlqQzxIhpVYlyciqAXOOjQQTY1-tCU');

import { connect } from 'react-redux';
import { bindActionCreators} from 'redux';
import * as mycardActions from '../actions/account';

import Header from '../components/Header';
import FeedbackModal from '../components/FeedbackModal';
import utils from '../utils'

var ls = require('react-native-local-storage');


import { SERVER_ROOT, API_URL, S3_URL_BASE } from 'react-native-dotenv'
import SocketIOClient from 'socket.io-client';

class CardHome extends Component {
  static navigationOptions = {
    header: null,
  };

  componentWillMount(){
  }

  geoSuccess(pos){
    Geocoder.geocodePosition({lng: pos.coords.longitude, lat: pos.coords.latitude}).then(res => {
      var location = {
        rawLocation: pos,
        geoLocation: res[0],
        value: res[0].streetName + ', ' + res[0].locality + ' ' + res[0].adminArea,
        updated: true,
      }

      this.props.setCurrentLocation(location);
      utils.beacon(this.props.userData.userid, location, this.socket);
    });
  }

  geoFail(err){
    // Alert.alert("Failed to get location: " + err.message);
  }

  componentDidMount(){

    this.setState({shareInProgress: false});
    Orientation.lockToPortrait();

    AppEventsLogger.logEvent('Viewed Card Home');
    utils.logEvent('viewed_card_home');

    utils.sync(this.props.userData, function(newUser){
      this.props.setUserDataState(newUser);
      ls.save('serverData', newUser);

      if(newUser.history.length === 2 && (typeof newUser.feedback === 'undefined' || newUser.feedback.length === 0)){
        utils.logEvent('show_feedback_modal');
        utils.showAppReview();
      }
    }.bind(this));
  }

  constructor(props) {
    super(props);

    this.state = {
      sendAddress: '',
      message: '',
      currentLocation: {value: 'unknown', updated: false},
      keyboardShown: false,
      inputShown: false,
      focusSendAddress: false,
      focusMessage: false,
      keyboardTimerStarted: false,
      isFlipped: false,
      showFeedback: false,
      message: '',
      ctaText: 'SHARE YOUR CARD',
      shareInProgress: false,
      sendLink: true,
      frontPrimary: true,
      fadeAnim: new Animated.Value(0),
    };


    this.getDefaultBusinessCard = this.getDefaultBusinessCard.bind(this);
    this.geoSuccess = this.geoSuccess.bind(this);
    this.saveFeedback = this.saveFeedback.bind(this);
    this.closeFeedback = this.closeFeedback.bind(this);
    this.shareCard = this.shareCard.bind(this);
    this.handleFrontPrimary = this.handleFrontPrimary.bind(this);

    this.socket = SocketIOClient.connect(SERVER_ROOT, {transports: ['websocket'] });

    this.socket.on('message', function(msg){
      console.log("message: " + msg);
    });

    navigator.geolocation.getCurrentPosition(this.geoSuccess, this.geoFail, { enableHighAccuracy: false, timeout: 30000, maximumAge: 0 });
    navigator.geolocation.watchPosition(this.geoSuccess); //, this.geoFail, {enableHighAccuracy: false, timeout: 30000, maximumAge: 0 });

    Animated.timing(
      this.state.fadeAnim,
      {
        toValue: 1,
        duration: 500,
      },
    ).start();
  }

  getDefaultBusinessCard(){
    if(this.props.userData){
      return this.props.userData.default_card;
    }

    // Default to show until the props are filled in
    // eventually should show a spinner or something
    return {
      alt_doc_id: 'xyz',
      combined_images: {},
    };
  }

  saveFeedback(upDown, message){
    let serverData = this.state;

    if(typeof serverData.feedback === 'undefined' || serverData.feedback === null){
      serverData.feedback = [];
    }

    serverData.feedback.push({
      ts: new Date(),
      rating: upDown,
      message: message
    });

    ls.save('serverData', serverData);
  }

  shareCard(){
    this.setState({shareInProgress: true});
    AppEventsLogger.logEvent('Clicked Share Card');
    utils.logEvent('clicked_share_card');

    const { navigate } = this.props.navigation;

    let historyLength = this.props.userData.history.length || 0;
    let shorturl = (SERVER_ROOT + '/u/'+this.props.userData.userid+'/'+(historyLength+1));

    let altdocid = this.getDefaultBusinessCard().alt_doc_id;

    utils.shareCard(shorturl, this.props.userData.default_card, this.state.sendLink, function(success){
      // Android always returns success=true
      if(success){
        let timeout = 500;
        if(Platform.OS === 'android'){
          timeout = 5000;
        }

        setTimeout(function(){
          utils.sendSuccess(this, ls, 'Shared your card', shorturl, '', this.props.currentLocation, altdocid, function(){
            console.log("navigating to success");
            navigate('CongratulationShare');
          }.bind(this));
        }.bind(this), timeout);

      }else{
        utils.logEvent('canceled_ios_share_card');
        //this.setState({ctaText: 'SHARE YOUR CARD'});
      }
    }.bind(this));
  }

  closeFeedback(){
    this.setState({showFeedback: false});
    utils.logEvent('close_feedback');
  }

  handleFrontPrimary(is_front_primary){
    let alt_doc_id = this.getDefaultBusinessCard().alt_doc_id;

    this.props.updateVcard(alt_doc_id, {alt_doc_id, is_front_primary});
  }

  handleVCardChanged(item){
  }

  render() {
    const { navigate } = this.props.navigation;

    let userData = this.props.userData;

    /* Try to get the current vcard selected
     * If you can't find it, report an error and continue on
     * 
     * The current guess is that this is a timing issue where
     * redux hasn't loaded the vcard information yet for some
     * reason.  The hope is that if this is the case, it will
     * just cause a momentary 'blip' on the users screen and
     * then show the correct information
    **/
    let currVcardIndex = -1;
    try{
      currVcardIndex = this.props.vcards.findIndex( i => { return i.alt_doc_id === userData.default_card.alt_doc_id } );
    }catch(ex){
      utils.error('error getting vcard index for ' + this.props.userData.userid);
    }
    let vcard = {
      is_front_primary: true
    };
    if(currVcardIndex >= 0){
      vcard = this.props.vcards[currVcardIndex];
    }

    let cardImage = <Image style={styles.bcImage} resizeMode='contain' source={{uri: this.getDefaultBusinessCard().combined_images.overlap_diagonal}} />
    let cardImageRev = <Image style={styles.bcImage} resizeMode='contain' source={{uri: this.getDefaultBusinessCard().combined_images.overlap_diagonal_rev}} />
    let shareIcon = <Image style={{width: 18, height: 18, marginRight: 10}} source={require('../images/share-and.png')} />

    if(Platform.OS === 'ios'){
      shareIcon = <Image style={{width: 14, height: 18, marginRight: 10}} source={require('../images/share-ios.png')} />
    }

    if(this.state.ctaText !== 'SHARE YOUR CARD'){
      shareIcon = <Text></Text>
    }

    let shareButtonDisplay = [shareIcon, this.state.ctaText]
    if(this.state.shareInProgress){
      shareButtonDisplay = <ActivityIndicator size='large' color='#ffffff' />
    }

    let uncheckedImage = <Image source={require('../images/blue-unchecked-checkbox.png')} style={{width: 13, height: 13}} />
    let checkedImage = <Image source={require('../images/blue-checked-checkbox.png')} style={{width: 13, height: 13}} />


    let viewingFront = (!vcard.alt_doc_id || vcard.is_front_primary);

    return (
      <Animated.View style={{backgroundColor: '#006098', flex: 1, opacity: this.state.fadeAnim}}>
        <View style={{flex: 1, justifyContent: 'flex-start', backgroundColor: 'white'}}>
          <Header 
            title='Your Card'
            titleIcon={<Image source={require('../images/header-caret.png')} style={{width: 15, height: 9, marginLeft: 9}} />}
            titlePress={() => navigate('BC', {skipTutorial: true, showBack: true})}
            rightIcon='gear'
            rightPress={ () => navigate('Settings') }
            rightLongPress={ () => navigate('SecretSettings') }
          />
  
          <ScrollView>
          <Image source={require('../images/homescreen-wood.jpg')} style={{width: '120%', maxHeight: '38%', marginLeft: -30, paddingTop: 40, resizeMode:'cover'}}>
            <TouchableOpacity
              onPress={ () => navigate('CardBig', {image: viewingFront ? this.getDefaultBusinessCard().combined_images.wood_vertical : this.getDefaultBusinessCard().combined_images.wood_vertical_rev  }) }>
              { viewingFront ? cardImage : cardImageRev }
            </TouchableOpacity>
          </Image>
  
          <View style={{flex: 1, justifyContent: 'flex-start', maxHeight: 40, minHeight: 40, paddingLeft: 20, paddingRight: 20}}>
              <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',}}>
                <Text style={{fontSize: 15, color: '#5c666f', fontWeight: '500'}}>Primary side</Text>
    
                <View style={{flex: 1, maxWidth: 130, flexDirection: 'row', justifyContent: 'flex-end'}}>
                  <TouchableOpacity style={{flex: 1, marginRight: 25, flexDirection: 'row'}} onPress={ () => this.handleFrontPrimary(true) }>
                    {vcard.is_front_primary ? checkedImage : uncheckedImage}
                    <Text style={{fontSize: 13, color: '#5c666f'}}> Front</Text>
                  </TouchableOpacity>
  
                  <TouchableOpacity style={{flex: 1, flexDirection: 'row'}} onPress={ () => this.handleFrontPrimary(false) }>
                    {!vcard.is_front_primary ? checkedImage : uncheckedImage}
                    <Text style={{fontSize: 13, color: '#5c666f'}}> Back</Text>
                  </TouchableOpacity>
                </View>
              </View>
    
              <View style={{borderColor: '#6c666f', width: '100%', height: 1, borderWidth: 1}} />
            </View>
  
            <View style={{minHeight: 40, paddingLeft: 20, paddingRight: 20, backgroundColor: 'white'}}>
              <View style={{flex: 1, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'space-between', marginTop: 16,}}>
                <Text style={{fontSize: 15, color: '#5c666f', fontWeight: '500'}}>Info delivered with your card</Text>
    
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
                  <TouchableOpacity onPress={ () => navigate('ContactInfo', this.handleVCardChanged) }>
                    <Text style={{fontSize: 15, fontWeight: '500', color: '#399dcb'}}>Edit</Text>
                  </TouchableOpacity>
                </View>
              </View>
  
            </View>

            <View style={{minHeight: 160, paddingLeft: 20, paddingRight: 20, backgroundColor: 'white'}}>
              <View style={{flex: 1, flexBasis: 40, flexDirection: 'row', alignItems: 'center'}}>
                <Image source={require('../images/vcard-edit-1.png')} style={{width: 17, height: 18}} />
  
                <View style={{flex: 1, flexBasis: 50, paddingLeft: 25}}>
                  <Text style={{color: '#5c666f'}}>{vcard.name || 'name'}</Text>
                  <Text style={{color: '#5c666f'}}>{vcard.business_name || 'business name'}</Text>
                </View>
              </View>
  
              <View style={{flex: 1, flexBasis: 30,flexDirection: 'row', alignItems: 'center'}}>
                <Image source={require('../images/vcard-edit-2.png')} style={{width: 18, height: 18}} />
  
                <View style={{flex: 1, paddingLeft: 24}}>
                  <Text style={{color: '#5c666f'}}>{vcard.phone || 'phone'}</Text>
                </View>
              </View>
  
              <View style={{flex: 1, flexBasis: 30, flexDirection: 'row', alignItems: 'center'}}>
                <Image source={require('../images/vcard-edit-3.png')} style={{width: 22, height: 13}} />
  
                <View style={{flex: 1, paddingLeft: 20}}>
                  <Text style={{color: '#5c666f'}}>{vcard.email || 'email'}</Text>
                </View>
              </View>
  
              <View style={{flex: 1, flexBasis: 30, flexDirection: 'row', alignItems: 'center'}}>
                <Image source={require('../images/vcard-edit-4.png')} style={{width: 22, height: 18}} />
  
                <View style={{flex: 1, paddingLeft: 20}}>
                  <Text style={{color: '#5c666f'}}>{vcard.website || 'website'}</Text>
                </View>
              </View>
            </View>
  
  
  
            <View style={{paddingBottom: 0, backgroundColor: 'white'}}>
              <Button
                onPress={ this.shareCard }
                accessibilityLabel="SHARE YOUR CARD"
                containerStyle={{marginTop: 30, padding:14, height:50, overflow:'hidden', borderRadius: 4, backgroundColor: '#1ea7e2', marginBottom: 0, width: '80%', marginLeft: '10%', justifyContent: 'center', alignItems: 'center'}}
                style={{fontSize: 16, color: 'white'}}>
                  {shareButtonDisplay}
              </Button>
            </View>

          {this.state.showFeedback ? <FeedbackModal email={userData.email} saveFeedback={this.saveFeedback} close={ this.closeFeedback } /> : <View></View>}
          </ScrollView>
        </View>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  bcImage: {
    minHeight: 125,
    height: '100%',
    maxHeight: '75%',
  },
});



export default connect(
  state => ({
    userData: state.mycard.userData,
    currentLocation: state.mycard.currentLocation,
    vcards: state.mycard.vcards,
  }),
  dispatch => bindActionCreators(mycardActions, dispatch)
)(CardHome);



