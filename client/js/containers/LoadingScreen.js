import React, { Component } from 'react';
import {
  AppRegistry,
  Animated,
  Linking,
  ActivityIndicator,
  Platform,
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  Alert,
  TextInput,
  Image,
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import Button from 'react-native-button'
import codePush from "react-native-code-push";

var utils = require('../utils');
import { API_URL, GOOGLE_GEOCODER_API_KEY } from 'react-native-dotenv'

var ls = require('react-native-local-storage');

import Geocoder from 'react-native-geocoder';
Geocoder.fallbackToGoogle(GOOGLE_GEOCODER_API_KEY);

import { connect } from 'react-redux';
import { bindActionCreators} from 'redux';
import * as mycardActions from '../actions/account';

import VersionNumber from 'react-native-version-number';

class LoadingScreen extends Component {
  static navigationOptions = {
    header: null,
  }

  componentDidMount(){
  }

  constructor(props) {
    super(props);
    const { navigate } = this.props.navigation;

    this.state = {
      fadeAnim: new Animated.Value(0),
    };

    codePush.sync({
      updateDialog: false,
      installMode: codePush.InstallMode.IMMEDIATE
    });

    Animated.timing(
      this.state.fadeAnim,
      {
        toValue: 1,
        duration: 1500,
      },
    ).start();

    ls.get('serverData').then(function(data){

      // Local Storage has the data we are looking for
      if(typeof data !== 'undefined' && data !== null && typeof data.default_card !== 'undefined' && data.default_card !== null){
        utils.bugsnagUser(data.userid, data.first_name + ' ' + data.last_name, data.email);

        const resetAction = NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'CardHome'})
          ]
        })

        // If this is a new version sign the user out to force a new
        // state pulled from the server
        let appVersion = VersionNumber.appVersion;
        let buildVersion = VersionNumber.buildVersion;

        let logout = false;

        // if we are upgrading from <= version 0.0.36 force logout
        if(Platform.OS === 'ios' && (!data.buildVersion || data.buildVersion <= 41)){
          logout=true;
        }
        if(Platform.OS === 'android' && (!data.buildVersion || data.buildVersion <= 716)){
          logout = true;
        }

        if(logout){
          // clear the old data out
          ls.save('serverData', {});
          ls.save('capturedAb', 'false');
          ls.save('datauris', []);

          const resetActionSignin = NavigationActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({ routeName: 'SignIn'})
            ]
          });

          this.props.navigation.dispatch(resetActionSignin);
          return;
        }

        this.props.setUserDataState(data);

        // TODO: Dirty hack to not have data flash on the home screen
        // should review this function and make it easier and cleaner
        setTimeout( () => {
          setTimeout( () => this.props.navigation.dispatch(resetAction), 3000);
        }, 1000);

        setTimeout( () => {
          Animated.timing(
            this.state.fadeAnim,
            {
              toValue: 0,
              duration: 1000,
            },
          ).start();
        }, 3000);

        ls.get('vcards').then( vcards => {
          if(!vcards){
            utils.getVcards(data.userid)
              .then( resp => {
                this.props.setVcards(resp);
              });
          }else{
            this.props.setVcards(vcards);
          }
        });

        return;

      // Local Storage does not have data
      }else{
        const resetAction = NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'SignInIntro'})
          ]
        })

        this.props.navigation.dispatch(resetAction)

        return;
      }


      //this.props.fetchUserData(data.userid).then( rvalue => {
        //console.log("user data callback: " + rvalue.userData.userid);
      //});


    }.bind(this))
    .catch( err => {
      console.log("ERROR: " + err);
      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'SignInIntro'})
        ]
      })
      this.props.navigation.dispatch(resetAction)
    });
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <Animated.View style={StyleSheet.flatten([styles.parentContainer, {opacity: this.state.fadeAnim}])}>
        <View style={styles.container}>
          <View style={styles.backgroundContainer}>
            <Image
              source={require('../images/background.png')}
              style={styles.background}
            />
          </View>
          <Image
            source={require('../images/logo.png')}
            style={styles.logo}
          />
  
          <Text style={{fontSize: 20, color: '#ffffff', backgroundColor: 'transparent', maxWidth: 330, paddingLeft: 20, paddingRight: 30, textAlign: 'center'}}>
            Your business card just learned how to text.
          </Text>
        </View>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  parentContainer: {
    backgroundColor: '#006098',
    flex: 1,
  },
  container: {
    flex: 1,
    paddingTop: 100,
    alignItems: 'center',
    backgroundColor: '#006098',
    padding: 20,
  },
  backgroundContainer: {
    position: 'absolute',
    left: -10,
    right: 0,
  },
  background: {
    position: 'absolute',
    width: '100%',
    resizeMode: 'cover',
  },
  logo: {
    marginBottom: 30
  },
});


export default connect(
  state => ({
    userData: state.userData
  }),
  dispatch => bindActionCreators(mycardActions, dispatch)
)(LoadingScreen);





