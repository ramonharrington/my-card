import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Linking,
  View,
  Text,
  ScrollView,
  Alert,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mycardActions from '../actions/account';

import utils from '../utils'
import { API_URL } from 'react-native-dotenv'

import {AppEventsLogger} from 'react-native-fbsdk';
import Button from 'react-native-button';
var ls = require('react-native-local-storage');

import Header from '../components/Header';


class BusinessCardSelect extends Component {
  static navigationOptions = {
    header: null
  }

  constructor(props) {
    super(props);

    this.state = {
      refreshInterval: null,
    };

    if(this.props.userData && this.props.userData.business_cards.length === 1 && (!this.props.userData.unordered_business_cards || this.props.userData.unordered_business_cards.length === 0)){
      utils.logEvent('select_first_business_card');
      this.bcPressed(this.props.userData.business_cards[0].doc_id);
    }
  }

  componentDidMount(){
    AppEventsLogger.logEvent('Viewed BC Select Page');
    utils.logEvent('viewed_bc_select_page');

    this.setState({refreshInterval: setInterval(function(){
      if(!this.props.userData || !this.props.userData.userid){
        return;
      }

      utils.getUser(this.props.userData.userid, function(newData){
        this.props.setUserDataState(newData);
      }.bind(this));

    }.bind(this), 5000)});
  }

  pushIfUnique(cards, cardToPush) {
    for (var i = 0; i < cards.length; i++) {
      if (cards[i].doc_id == cardToPush.doc_id) {
        // Card already exists, skip
        return;
      }
    }

    cards.push(cardToPush);
  }

  stopUserPolling(){
    if(this.state.refreshInterval !== null){
      clearInterval(this.state.refreshInterval);
      this.setState({refreshInterval: null});
    }
  }

  bcPressed(doc_id){
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
    utils.logEvent('selected_business_card');

    // clone the store
    var serverData = JSON.parse(JSON.stringify(this.props.userData));
    
    // I know there's a better way
    var bc = null;

    for(let i=0; i<serverData.business_cards.length; i++){
      let bc = serverData.business_cards[i];

      if(bc.doc_id === doc_id){
        serverData.default_card = bc;
      }
    }

    this.props.setUserDataState(serverData);

    ls.save('serverData', serverData);
    this.stopUserPolling();

    // If we already have vcard data then skip
    // the contact edit screen
    try{
      var vcardIndex = this.props.vcards.findIndex( i => { return i.alt_doc_id === serverData.default_card.alt_doc_id } );
      console.log("vcardindex: " + vcardIndex);

      if(vcardIndex >= 0){
        navigate('CardHome');
      }else{
        navigate('ContactInfo', {goHome: true, card: serverData.default_card});
      }
    }catch(ex){
      utils.error('Error navigating from BCSelect to next page');
      navigate('CardHome');
    }
  }

  unorderedBcPressed(alt_doc_id){
    Linking.openURL('https://www.vistaprint.com/vp/gateway.aspx?S=7164994172&preurl=/basket-flow-controller.aspx?alt_doc_id='+alt_doc_id+'&origin=11')
    AppEventsLogger.logEvent('Clicked Order BC');
    utils.logEvent('clicked_order_bc');
  }


  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;

    let rawcards = [];

    for(let i=0; i<this.props.userData.business_cards.length; i++){
      let card = this.props.userData.business_cards[i];
      card.ordered = true;
      this.pushIfUnique(rawcards, card);
    }

    for(let i=0; i<this.props.userData.unordered_business_cards.length; i++){
      let card = this.props.userData.unordered_business_cards[i];
      card.ordered = false;
      this.pushIfUnique(rawcards, card);
    }

    rawcards.sort( (a,b) => a.created > b.created ? -1 : 1 );

    let cards = rawcards.map(function(bc){
      let opacity = this.state.selectedCard ? .3 : 1;
      if(this.state.selectedCard){
        if(bc.doc_id === this.state.selectedCard){
          opacity = 1;
        }
      }

      if(this.state.selectedCard && bc.doc_id === this.state.selectedCard){
        return (
          <TouchableOpacity
            key={bc.doc_id}
            onPress={ () => this.bcPressed(bc.doc_id) }
            style={styles.card}
          >
          <Image style={{flex: 1, opacity}} source={require('../images/shadow.png')}>
            <Image
              style={{flex: 1, margin: 20}}
              resizeMode='contain'
              source={{uri: bc.front_img}}>
                <Image style={{flex: 1, opacity: 0}} resizeMode='cover' source={require('../images/cardlock.png')} />
            </Image>
          </Image>
          <Image style={{flex: 1, opacity}} source={require('../images/shadow.png')}>
            <Image
              style={{flex: 1, margin: 20}}
              resizeMode='contain'
              source={{uri: bc.back_img}}>
                <Image style={{flex: 1, opacity: 0}} resizeMode='cover' source={require('../images/cardlock.png')} />
            </Image>
          </Image>
            <Button
              onPress={ () => this.bcPressed(bc.doc_id) }
              accessibilityLabel="SHARE YOUR CARD"
              containerStyle={{marginTop: 30, padding:14, marginRight: '10%', height:50, overflow:'hidden', borderRadius: 4, backgroundColor: '#1ea7e2', marginBottom: 0, minWidth: '80%', marginLeft: '10%', justifyContent: 'center', alignItems: 'center'}}
              style={{fontSize: 16, color: 'white'}}>
                USE THIS CARD
            </Button>
  
          </TouchableOpacity>
        );
      }else if(bc.ordered){
        return(
          <TouchableOpacity
            key={bc.doc_id}
            onPress={ () => this.setState({selectedCard: bc.doc_id}) }
            style={styles.card}
          >
          <Image style={{flex: 1, opacity}} source={require('../images/shadow.png')}>
            <Image
              style={{flex: 1, margin: 20}}
              resizeMode='contain'
              source={{uri: (bc.combined_images ? bc.combined_images.overlap_diagonal : bc.front_img)}}>
                <Image style={{flex: 1, opacity: 0}} resizeMode='cover' source={require('../images/cardlock.png')} />
            </Image>
          </Image>
  
          </TouchableOpacity>
        );
      }else{
        return(
          <TouchableOpacity
            key={bc.doc_id}
            onPress={ () => this.unorderedBcPressed(bc.alt_doc_id) }
            style={styles.card}
          >
          <Image style={{flex: 1, opacity}} source={require('../images/shadow.png')}>
            <Image
              style={{flex: 1, margin: 20}}
              resizeMode='contain'
              source={{uri: (bc.combined_images ? bc.combined_images.overlap_diagonal : bc.front_img)}}>
  
              <Image style={{flex: 1, alignSelf: 'center'}} resizeMode='cover' source={require('../images/cardlock.png')} />
            </Image>
          </Image>
  
          </TouchableOpacity>
        );
      }
    }.bind(this));

    return (
      <View style={{flex: 1}}>
        <Header
          title='Card Selection'
          rightIcon={params && params.showBack ? 'back' : null}
          rightPress={ () => this.props.navigation.goBack() }
        />
        <ScrollView style={styles.container}>
          <Text style={styles.header}>We found {cards.length} cards in your profile</Text>
          <Text style={styles.title}>Select a card to add</Text>
          { cards }
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: 10,
    backgroundColor: '#F0F2F3',
    padding: 20,
  },
  card: {
    alignSelf: 'center',
    marginBottom: 21,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
  header: {
    marginTop: 25,
    textAlign: 'center',
    fontSize: 14,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#38454F',
    marginBottom: 24,
  },
  subtitle: {
    fontSize: 14,
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  label: {
    textAlign: 'left',
    color: '#333333',
    marginTop: 10,
  },
  inputField: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    backgroundColor: 'white',
    borderRadius: 4,
    padding: 10
  },
  button: {
    backgroundColor: "#FF0000"
  },
});



export default connect(
  state => ({
    userData: state.mycard.userData,
    vcards: state.mycard.vcards,
  }),
  dispatch => bindActionCreators(mycardActions, dispatch)
)(BusinessCardSelect);
