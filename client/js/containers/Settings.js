import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  WebView,
  View,
  Alert,
  TextInput,
  Image,
  TouchableOpacity,
  Linking,
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators} from 'redux';
import * as mycardActions from '../actions/account';

import Button from 'react-native-button'

import FullWidthLink from '../components/FullWidthLink';
import FullWidthToggle from '../components/FullWidthToggle';
import Header from '../components/Header';

var ls = require('react-native-local-storage');

import {AppEventsLogger} from 'react-native-fbsdk';
import { SERVER_ROOT, API_URL } from 'react-native-dotenv'
import utils from '../utils';

class Settings extends Component {

  static navigationOptions = {
    header: null,
  };

  componentDidMount(){
    AppEventsLogger.logEvent('Viewed Settings Page');
    utils.logEvent('viewed_settings_page');
  }

  constructor(){
    super();

    this.eraseData = this.eraseData.bind(this);
  }

  eraseData(){
    const { navigate } = this.props.navigation;
    ls.save('serverData', {});
    ls.save('capturedAb', 'false');
    ls.save('datauris', []);
    
    navigate('Main');
  }

  render(){
    const { navigate } = this.props.navigation;
    
    return(
      <View>
        <Header title='General Settings' rightIcon='done' rightPress={ () => this.props.navigation.goBack() } />

        <TouchableOpacity onPress={() => navigate('BC', {skipTutorial: true})}>
          <View style={styles.changeCardContainer}>
            <Image style={{flex: 0, width: 92, height: 61}}  source={{uri: this.props.userData.default_card.front_img}} />
            <Text style={styles.heading}>Change your card ></Text>
          </View>
        </TouchableOpacity>
        <FullWidthLink title='Switch Accounts' onPress={ this.eraseData } />

        <Text style={styles.header}>General Info</Text>
        <FullWidthLink title='View Tutorial' onPress={() => navigate('Tutorial') } />
        <FullWidthLink title='Rate Us in the App Store' onPress={ () => utils.showAppReview(true) } />

        <Text style={styles.header}>Support</Text>

        <FullWidthLink title='Contact mycard support' onPress={() => Linking.openURL('mailto:mycard@vistaprint.com?subject=MyCard Inquiry') } />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    fontSize: 14,
    color: '#6d6d72',
    height: 40,
    paddingTop: 10,
    paddingLeft: 16,
  },
  heading: {
    fontSize: 14,
  },
  changeCardContainer: {
    flex: 1,
    flexGrow: 1,
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    width: '100%', 
    paddingLeft: 10,
    paddingRight: 10,
    height: 80,
    flexBasis: 80,
  },
});

export default connect(
  state => ({
    userData: state.mycard.userData
  }),
  dispatch => bindActionCreators(mycardActions, dispatch)
)(Settings);
