import React, { Component } from 'react';
import {
  AppRegistry,
  Linking,
  ActivityIndicator,
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  Alert,
  TextInput,
  Image,
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import Button from 'react-native-button'

import Header from '../components/Header';

module.exports = class SignInIntro extends Component {
  static navigationOptions = {
    header: null 
  }

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.container}>
        <Header />
        <View style={styles.backgroundContainer}>
          <Image
            source={require('../images/background.png')}
            style={styles.background}
          />
        </View>

        <View style={{flex: 1, alignItems: 'center'}}>
        <Image
          source={require('../images/logo.png')}
          style={styles.logo}
        />

        <Text style={{fontSize: 20, color: '#ffffff', backgroundColor: 'transparent', maxWidth: 330, paddingLeft: 20, paddingRight: 30, textAlign: 'center'}}>
          Your business card just learned how to text
        </Text>
        </View>

        <Button
          onPress={() => navigate('SignIn')}
          accessibilityLabel="IMPORT YOUR CARD NOW"
          containerStyle={{marginBottom: 10, flex: 1, alignSelf: 'flex-end',  paddingTop:18, maxHeight:60, overflow:'hidden', borderRadius:4, backgroundColor: '#1ea7e2', width: '100%'}}
          style={{fontSize: 16, color: 'white'}}>
            GET STARTED
          </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#006098',
    padding: 20,
    paddingTop: 0,
  },
  backgroundContainer: {
    position: 'absolute',
    left: -10,
    right: 0,
  },
  background: {
    position: 'absolute',
    width: '100%',
    resizeMode: 'cover',
  },
  logo: {
    marginBottom: 15,
  },
});

