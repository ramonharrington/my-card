import React, { Component } from 'react';
import {
  AppRegistry,
  ActivityIndicator,
  StyleSheet,
  Text,
  ScrollView,
  View,
  Alert,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import utils from '../utils'
import Button from 'react-native-button'

import { connect } from 'react-redux';
import { bindActionCreators} from 'redux';
import * as mycardActions from '../actions/account';
import Header from '../components/Header';

var ls = require('react-native-local-storage');

import Geocoder from 'react-native-geocoder';
Geocoder.fallbackToGoogle('AIzaSyB5rMlqQzxIhpVYlyciqAXOOjQQTY1-tCU');

import {AppEventsLogger} from 'react-native-fbsdk';
import { S3_URL_BASE } from 'react-native-dotenv'

class Tutorial extends Component {
  static navigationOptions = {
    header: null 
  }

  constructor(props) {
    super(props);
    this.state = {
      serverData: {business_cards: [], default_card: {front_image: '', alt_doc_id: ''}},
      message: 'Turn on notifications',
    };

    ls.get('serverData').then(function(data){
      this.setState({serverData: data})
      utils.sync(data, function(){});
    }.bind(this));


    this.getDefaultBusinessCard = this.getDefaultBusinessCard.bind(this);
    this.skipTutorial = this.skipTutorial.bind(this);
  }
  
  componentDidMount(){
    AppEventsLogger.logEvent('Viewed Tutorial Page');
    utils.logEvent('viewed_tutorial_page');
  }

  getDefaultBusinessCard(){
    return this.state.serverData.default_card;
  }

  skipTutorial(){
    const { navigate } = this.props.navigation;

    utils.logEvent('clicked_get_started');

    if(!this.props.userData || this.props.userData.business_cards.length == 0 && this.props.userData.unordered_business_cards.length === 0){
      navigate('BCEmpty');
    }else{
      navigate('BC');
    }
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={{flex: 1}}>

        <Header title='Welcome' />
        <ScrollView>
          <View contentContainerStyle={styles.container}>
  
            <Image source={require('../images/logo-tutorial.png')} style={{width: 114, height: 80, marginLeft: 'auto', marginRight: 'auto', marginTop: 18, marginBottom: 16}} />
            <Text style={styles.title}>Here&apos;s how mycard works</Text>
  
            <View style={{flex: 1, flexDirection: 'column', flexGrow: 1, marginLeft: 43, marginRight: 40, minHeight: 200}}>
  
              <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                <Image source={require('../images/tutorial-1.png')} style={{width: 41, height: 41}} />
                <Text style={styles.bulletText}>Select your business card.</Text>
              </View>
  
              <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                <Image source={require('../images/tutorial-2.png')} style={{width: 30, height: 32}} />
                <Text style={styles.bulletText}>Add contact information that will be sent with your card.</Text>
              </View>
  
              <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                <Image source={require('../images/tutorial-3.png')} style={{width: 41, height: 41}} />
                <Text style={styles.bulletText}>Share over text, email or any of your social media platforms.</Text>
              </View>
  
            </View>
  
            <View style={{width: '100%'}}>
              <Button
                onPress={ this.skipTutorial }
                  accessibilityLabel="Go to your card"
                  containerStyle={{marginBottom: 20, marginTop: 60, padding:10, height:44, overflow:'hidden', borderRadius:4, backgroundColor: '#1ea7e2', marginLeft: '10%', width: '80%'}}
                  style={{fontSize: 16, color: 'white'}}>
                      GET STARTED
              </Button>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 0,
    alignItems: 'center',
    backgroundColor: '#f0f2f3',
    paddingLeft: 25,
    paddingRight: 25,
    paddingBottom: 100,
    justifyContent: 'flex-start',
  },
  title: {
    fontSize: 19,
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 30,
    fontWeight: '600',
    color: '#38454f',
  },
  bulletText: {
    fontSize: 16,
    color: '#38454f',
    paddingLeft: 12,
    width: 250,
  }
});

export default connect(
  state => ({
    userData: state.mycard.userData,
    vcards: state.mycard.vcards,
  }),
  dispatch => bindActionCreators(mycardActions, dispatch)
)(Tutorial);
