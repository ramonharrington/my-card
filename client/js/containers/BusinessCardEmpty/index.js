import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  Text,
  Linking,
  ScrollView,
  Alert,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import utils from '../../utils'
import { API_URL } from 'react-native-dotenv'

import {AppEventsLogger} from 'react-native-fbsdk';
import Button from 'react-native-button';
var ls = require('react-native-local-storage');


export default class BusinessCardEmpty extends Component {
  static navigationOptions = {
    header: null
  }

  constructor(props) {
    super(props);
    this.state = {
      serverData: {business_cards: []},
    };


    this.openGetHelp = this.openGetHelp.bind(this);
    this.signInAgain = this.signInAgain.bind(this);
  }

  componentDidMount(){
    AppEventsLogger.logEvent('Viewed BCSelect Empty Page');
    utils.logEvent('viewed_bcselect_empty_page');

    ls.get('serverData').then(function(data){
      utils.sync(data, function(){});
      this.setState({serverData: data});

      utils.sendSlack(data.userid, 'I just saw the empty business card page');
    }.bind(this));
  }

  openBCPage(){
    AppEventsLogger.logEvent('Open BC Category Page');
    Linking.openURL('http://vistaprint.com/category/business-cards.aspx')
    utils.logEvent('clicked_order_bc_now');
  }

  openGetHelp(){
    AppEventsLogger.logEvent('Report a problem');
    utils.logEvent('clicked_report_a_problem');
    Linking.openURL('mailto:mycard@vistaprint.com?subject=MyCard Inquiry&body=login email: ' + this.state.serverData.email)
  }

  signInAgain(){
    AppEventsLogger.logEvent('Sign in again');
    utils.logEvent('clicked_sign_in_again');
    this.props.navigation.goBack()
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={{flex: 1, paddingLeft: 25, paddingRight: 25, backgroundColor: '#f0f2f3'}}>
        <ScrollView style={styles.container}>
          <Text style={styles.header}>We couldn&apos;t find a card in your account</Text>
          <Image source={require('../../images/no_card.png')} style={{width: '100%', height: 190}} />
          <Text style={styles.title}>In order to use mycard you need to have ordered a business card with the vistaprint acccount you signed in with.</Text>
          <Button
            onPress={ this.openBCPage }
            accessibilityLabel="ORDER BUSINESS CARDS NOW"
            containerStyle={{marginBottom: 20, padding:15, height:54, overflow:'hidden', borderRadius:4, backgroundColor: '#1ea7e2'}}
            style={{fontSize: 16, color: 'white'}}>
              ORDER BUSINESS CARDS NOW
          </Button>

          <TouchableOpacity onPress={ this.signInAgain }>
            <Text style={styles.title}>Sign in with a different account</Text>
          </TouchableOpacity>

        </ScrollView>
        <View>
          <TouchableOpacity onPress={ this.openGetHelp }>
            <Text style={{fontSize: 14, fontWeight: '500', textAlign: 'center', color: '#858f97', paddingBottom: 20}}>Having an issue?  Report your problem now</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: 10,
    backgroundColor: '#F0F2F3',
    padding: 20,
  },
  card: {
    alignSelf: 'center',
    marginBottom: 21,
  },
  header: {
    marginTop: 25,
    textAlign: 'center',
    fontSize: 25,
    fontWeight: '600',
    marginBottom: 16,
    marginTop: '20%',
    color: '#38454F',
  },
  title: {
    fontSize: 15,
    fontWeight: 'normal',
    textAlign: 'center',
    color: '#38454F',
    marginBottom: 24,
    marginTop: 10,
  },
  subtitle: {
    fontSize: 14,
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  label: {
    textAlign: 'left',
    color: '#333333',
    marginTop: 10,
  },
  inputField: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    backgroundColor: 'white',
    borderRadius: 4,
    padding: 10
  },
  button: {
    backgroundColor: "#FF0000"
  },
});

module.exports = BusinessCardEmpty;
