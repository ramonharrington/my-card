/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  Linking,
  StyleSheet,
  Text,
  ScrollView,
  View,
  WebView,
  Alert,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';

var ls = require('react-native-local-storage');
import Button from 'react-native-button'

import { connect } from 'react-redux';
import { bindActionCreators} from 'redux';
import * as mycardActions from '../actions/account';

import Header from '../components/Header'
var utils = require('../utils');

import {AppEventsLogger} from 'react-native-fbsdk';
import { SERVER_ROOT, API_URL } from 'react-native-dotenv'


class CongratulationShare extends Component {
  static navigationOptions = {
    header: null 
  };

  constructor(props) {
    super(props);

    this.state = {
      selected: null,
      message: '',
    };

    this.__finish = this.__finish.bind(this);
    this.shareFB = this.shareFB.bind(this);
  }

  componentDidMount(){
    AppEventsLogger.logEvent('Viewed Congratulation Share Page');
    utils.logEvent('viewed_congratulation_share_page');
  }

  __finish(){
    const { navigate } = this.props.navigation;
    navigate('CardHome');
  }

  shareFB(){
    AppEventsLogger.logEvent('Clicked Share Facebook');
    utils.logEvent('clicked_share_fb');

    const { navigate } = this.props.navigation;

    let historyLength = this.props.userData.history.length || 0;
    let shorturl = (SERVER_ROOT + '/u/'+this.props.userData.userid+'/'+(historyLength+1));
    let altdocid = this.props.userData.default_card.alt_doc_id;

    utils.shareFB(shorturl, this.props.userData.default_card, function(success){
      // Android always returns success=true
      if(success){
        let timeout = 500;
        if(Platform.OS === 'android'){
          timeout = 5000;
        }

        setTimeout(function(){
          utils.sendSuccess(this, ls, 'Shared your card', shorturl, '', this.props.currentLocation, altdocid, function(){
            navigate('CardHome');
          }.bind(this));
        }.bind(this), timeout);

      }else{
        utils.logEvent('canceled_ios_share_fb');
        //this.setState({ctaText: 'SHARE YOUR CARD'});
      }
    }.bind(this));
  }

  render() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    let shareToFBButton =  <Button
            onPress={ this.shareFB }
            accessibilityLabel="share to facebook"
            containerStyle={{padding:10, height:44, marginBottom: 10, overflow:'hidden', borderRadius:4, backgroundColor: '#1ea7e2', width: '90%', marginLeft: '5%'}}
            style={{fontSize: 16, color: 'white'}}>
              SHARE TO FACEBOOK
          </Button>
  let postShareMessaging = <View style={{flex: 1, flexDirection: 'column', justifyContent: 'flex-start', paddingTop: 30, alignItems: 'center', width: '100%', minHeight: '100%' }}>
            <Image source={require('../images/facebook-app-logo.png')} style={{width: 38, height: 38, marginTop: 0, marginBottom: 0}} />
            <Text style={styles.header}>Have you tried sharing to Facebook?</Text>

            <Text style={{textAlign: 'center', fontSize: 15, marginTop: 15, paddingLeft: 60, paddingRight: 60,}}>Connect to a larger audience and give your card more visibility. Add <Text style={{fontWeight: '600'}}>#mycard</Text> to be seen in our social community.</Text>
          </View>
  
    if(!this.props.userData || !this.props.userData.history || this.props.userData.history.length === 0){
      return <View style={styles.container}><Button onPress={() => navigate('CardHome')}>go to home screen</Button></View>
    }

    let lastSend = this.props.userData.history[this.props.userData.history.length-1];

    return (
      <View style={styles.container}>
        <Header title='Success' rightIcon='home' rightPress={ () => navigate('CardHome') } />

        <ScrollView contentContainerStyle={{alignItems: 'stretch'}} style={{flex: 1,  flexDirection: 'column'}}>
          <Text style={styles.header}>Your card has been shared</Text>
          <Image style={{flex: 1}} source={require('../images/shadow.png')}>
            <Image style={{width: '80%', height: 200, marginTop: 10, resizeMode: 'contain', alignSelf: 'center'}} source={{uri: this.props.userData.default_card.front_img}} />
          </Image>

          <View style={{height: 2, borderWidth: 1, borderColor: '#d3d5d6', marginTop: 20,}} />

          {postShareMessaging}

        </ScrollView>
        <View style={{alignSelf: 'stretch'}}>
          <Image source={require('../images/mycard-background-outline.png')} style={{position: 'absolute', left: 0, bottom: 50, resizeMode: 'stretch', width: 271, height: 190}} />
    
          {shareToFBButton}
      
          <TouchableOpacity onPress={ () => Linking.openURL('mailto:mycard@vistaprint.com?subject=MyCard Inquiry') }>
            <Text style={{marginTop: 5, marginBottom: 20, textAlign: 'center'}}>Have feedback? <Text style={{color: '#1ea7e2'}}>Let us know</Text></Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    backgroundColor: '#F5FCFF',
    backgroundColor: '#f8f8f8',
  },
  normal: {
    fontSize: 16,
    alignSelf: 'center',
  },
  header: {
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 20,
    alignSelf: 'center',
    color: '#38454f',
  },
});

export default connect(
  state => ({
    userData: state.mycard.userData,
    currentLocation: state.mycard.currentLocation,
    vcards: state.mycard.vcards,
  }),
  dispatch => bindActionCreators(mycardActions, dispatch)
)(CongratulationShare);

