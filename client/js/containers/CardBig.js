import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  ScrollView,
  View,
  Alert,
  TextInput,
  Image,
  TouchableOpacity,
  Platform,
} from 'react-native';

import {AppEventsLogger} from 'react-native-fbsdk';

import utils from '../utils'
import Header from '../components/Header'

class CardBig extends Component {
  static navigationOptions = {
    header: null 
  };

  componentDidMount(){
    AppEventsLogger.logEvent('Viewed Card Big Page');
    utils.logEvent('viewed_card_big_page');
  }

  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;

    return (
      <View style={{flex: 1, alignItems: 'stretch'}}>
        <Header title='mycard' rightIcon='done' rightPress={ () => navigate('CardHome') } />
          <Image
            style={{flex:1, resizeMode: 'contain'}}
            source={{uri: params.image}}
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
});

export default CardBig;

