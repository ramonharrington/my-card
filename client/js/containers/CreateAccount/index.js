import React, { Component } from 'react';
import {
  AppRegistry,
  ActivityIndicator,
  TouchableOpacity,
  Linking,
  StyleSheet,
  Text,
  View,
  Alert,
  TextInput,
  Image,
} from 'react-native';
import utils from '../../utils'
import Button from 'react-native-button'
import { API_URL } from 'react-native-dotenv'

import {AppEventsLogger} from 'react-native-fbsdk';
var ls = require('react-native-local-storage');

module.exports = class CreateAccount extends Component {
  static navigationOptions = {
    header: null 
  }

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      password2: '',
      loginInProgress: false,
      emailOptIn: true,
    };

    this.__signIn = this.__signIn.bind(this);
  }

  componentDidMount(){
    this.setState({loginInProgress: false});
    AppEventsLogger.logEvent('Viewed CreateAccount Page');
    utils.logEvent('viewed_createaccount_page');
  }

  __signIn(email, token){
    const { navigate } = this.props.navigation;

    if(email === ''){
      Alert.alert('Please enter your Vistaprint email address');
      return;
    }
    this.setState({email: email});

    if( this.state.password === '' || this.state.password2 === ''){
      Alert.alert('Your password cannot be blank.  Please enter a valid password');
      return;
    }else if(this.state.password != this.state.password2){
      Alert.alert('Your passwords do not match');
      return;
    }else if(this.state.password.length < 6){
      Alert.alert('Your password must be at least 6 characters');
      return;
    }

    this.setState({loginInProgress: true});

    fetch(API_URL + '/createAccount', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: this.state.password,
        emailOptIn: this.state.emailOptIn,
      })
    })
    .then(function(response){
      return response.json();
    })
    .then(function(response){
      this.setState({loginInProgress: false});

      if(response.error){
        throw "Unable to create account";
      }else{
        navigate('CreateAccountConfirmation');
      }

    }.bind(this))
    .catch(function(e){
      this.setState({loginInProgress: false});
      this.setState({password: ''});
      this.setState({password2: ''});

      Alert.alert("Account Creation Failed: Please try again");
    }.bind(this));

  }

  render() {
    const { navigate } = this.props.navigation;
    let buttonText = 'SUBMIT';

    if(this.state.loginInProgress){
      buttonText = <ActivityIndicator size='large' color='#ffffff' />
    }

    let uncheckedImage = require('../../images/empty-checkbox.png');
    let checkedImage = require('../../images/checkbox-checked.png');

    return (
      <View style={styles.container}>
        <View style={styles.backgroundContainer}>
          <Image
            source={require('../../images/background.png')}
            style={styles.background}
          />
        </View>

        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'flex-start', width: '100%'}}>
        <Image
          source={require('../../images/logo.png')}
          style={styles.logo}
        />

        <Text style={styles.header}>Create a Vistaprint account</Text>

        <TextInput
          style={styles.inputField}
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
          autoCapitalize='none'
          keyboardType='email-address'
          placeholder='Email address'
          placeholderTextColor='#c5dbe9'
          underlineColorAndroid='rgba(0,0,0,0)'
          selectionColor='white'
          autoCorrect={false}
        />

        <TextInput
          style={styles.inputField}
          onChangeText={(password) => this.setState({password})}
          secureTextEntry={true}
          value={this.state.password}
          autoCapitalize='none'
          placeholder='Password (min. 6 characters)'
          placeholderTextColor='#c5dbe9'
          underlineColorAndroid='rgba(0,0,0,0)'
          selectionColor='white'
        />

        <TextInput
          style={styles.inputField}
          onChangeText={(password2) => this.setState({password2})}
          secureTextEntry={true}
          value={this.state.password2}
          autoCapitalize='none'
          placeholder='Re-type Password'
          placeholderTextColor='#c5dbe9'
          underlineColorAndroid='rgba(0,0,0,0)'
          selectionColor='white'
        />

        <TouchableOpacity onPress={(emailOptIn) => this.setState({emailOptIn: !this.state.emailOptIn})}>
          <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', flexBasis: 20, marginTop: 20}}>
            <Image
              source={this.state.emailOptIn ? checkedImage : uncheckedImage}
              style={{width: 25, height: 20, marginRight: 10, resizeMode: 'contain'}}
            />
            <Text style={{fontSize: 13, color: '#ffffff'}} >
              Yes, I want discounts and marketing tips by e-mail
            </Text>
          </View>
        </TouchableOpacity>

        <Button
          onPress={() => this.__signIn()}
          accessibilityLabel="Sign in using your vistaprint credentials"
          containerStyle={{marginTop: 37, padding:14, height:50, overflow:'hidden', borderRadius:4, backgroundColor: '#1ea7e2', width: '100%'}}
          style={{fontSize: 16, color: 'white'}}>
            { buttonText }
        </Button>

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 40,
    alignItems: 'center',
    backgroundColor: '#006098',
    padding: 20,
  },
  backgroundContainer: {
    position: 'absolute',
    left: -10,
    right: 0,
  },
  background: {
    flex: 1,
    width: '100%',
    resizeMode: 'cover',
  },
  logo: {
    marginBottom: 30,
    marginLeft: 'auto',
    marginRight: 'auto',
    width: 150,
    height: 95,
  },
  header: {
    color: '#ffffff',
    fontSize: 21,
    marginBottom: 29,
    textAlign: 'center',
  },
  label: {
    textAlign: 'left',
    color: '#333333',
    marginTop: 10,
  },
  inputField: {
    height: 45,
    width: '100%',
    borderColor: 'gray',
    borderWidth: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.15)',
    borderRadius: 4,
    padding: 10,
    marginTop: 7,
    color: '#ffffff',
  },
  button: {
    backgroundColor: "#FF0000"
  },
  footer: {
    color: '#FFFFFF',
    marginTop: 20,
  }
});
