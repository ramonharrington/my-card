import React, { Component } from 'react';
import {
  StyleSheet,
  StatusBar,
  TextInput,
  Text,
  ActivityIndicator,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image,
  Platform, 
} from 'react-native';

import Button from 'react-native-button'
import { API_URL } from 'react-native-dotenv'

export default class TokenSendCode extends Component {
  componentDidMount(){
    this.setState({email: this.props.email});
  }

  constructor(props) {
    super(props);

    this.state = {
      email: '',
      sentToken: false,
      token: '',
    };

    this.__sendToken = this.__sendToken.bind(this);
  }

  __sendToken() {
    fetch(API_URL + '/token', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email,
      })
    })
    .then(function(response){
      return response.json();
    })
    .then(function(response){
      this.setState({sentToken: true});
    }.bind(this));
  }

  render(){
    let emailScreen = (
      <View style={styles.veilContainer}>
        <View style={styles.modalContainer}>
          <Text style={styles.title}>Having login trouble?</Text>
          <Text style={styles.subTitle}>Get a temporary pin emailed to you for easy sign in</Text>

          <TouchableOpacity style={styles.closeTextContainer} onPress={() => this.props.close()}>
            <Text style={styles.closeText}>+</Text>
          </TouchableOpacity>

          <TextInput
            style={styles.emailInput}
            placeholder='your email address'
            value={this.state.email}
            onChangeText={(email) => this.setState({email})}
            autoCapitalize='none'
            keyboardType='email-address'
            placeholder='Vistaprint login email'
            placeholderTextColor='#d4dde1'
            underlineColorAndroid='rgba(0,0,0,0)'
            selectionColor='#d4dde1'
            autoCorrect={false}
          />

          <Button
            onPress={this.__sendToken}
            accessibilityLabel="Sign in using your vistaprint credentials"
            containerStyle={{marginTop: 20, padding:14, height:50, overflow:'hidden', borderRadius:4, backgroundColor: '#1ea7e2', width: '100%'}}
            style={{fontSize: 16, color: 'white'}}>

              SEND ME MY CODE

          </Button>

          <TouchableOpacity onPress={() => this.props.close()}>
            <Text style={styles.footerText}>Try logging in again</Text>
          </TouchableOpacity>
        </View>
      </View>
    );

    let buttonText = 'SIGN IN';
    if(this.props.loginInProgress){
      buttonText = <ActivityIndicator size='large' color='#ffffff' />
    }

    let tokenScreen = (
      <View style={styles.veilContainer}>
        <View style={styles.modalContainer}>
          <Text style={styles.title}></Text>
          <Text style={styles.subTitle}>We sent your temporary pin to <Text style={{fontWeight: 'bold'}}>{this.state.email}</Text></Text>

          <TouchableOpacity style={styles.closeTextContainer} onPress={() => this.props.close()}>
            <Text style={styles.closeText}>+</Text>
          </TouchableOpacity>

          <TextInput
            style={styles.emailInput}
            placeholder='enter new temporary pin'
            keyboardType='numeric'
            value={this.state.token}
            onChangeText={(token) => this.setState({token})}
            autoCapitalize='none'
            placeholderTextColor='#d4dde1'
            underlineColorAndroid='rgba(0,0,0,0)'
            selectionColor='black'
            autoCorrect={false}
          />

          <Button
            onPress={() => this.props.signIn(this.state.token)}
            accessibilityLabel="Sign in using your vistaprint credentials"
            containerStyle={{marginTop: 20, padding:14, height:50, overflow:'hidden', borderRadius:4, backgroundColor: '#1ea7e2', width: '100%'}}
            style={{fontSize: 16, color: 'white'}}>

            { buttonText }

          </Button>

          <TouchableOpacity onPress={() => this.props.close()}>
            <Text style={styles.footerText}>Try a different email address</Text>
          </TouchableOpacity>
        </View>
      </View>
    )

    return this.state.sentToken ? tokenScreen : emailScreen;
  }
}

const styles = StyleSheet.create({
  veilContainer: {
    position: 'absolute',
    width: '110%',
    height: '120%',
    left: 0,
    top: 0,
    backgroundColor: 'rgba(1,1,1,0.7)',
    flex: 1,
  },

  modalContainer: {
    width: '95%',
    height: 338,
    marginTop: 80,
    borderRadius: 6,
    padding: 20,
    backgroundColor: 'white',
    marginLeft: 'auto',
    marginRight: 'auto',
    opacity: 1,
  },

  closeTextContainer: {
    position: 'absolute',
    top: 0,
    right: 10,
  },

  closeText: {
    fontSize: 40,
    fontWeight: '300',
    color: '#38454f',
    transform: [{ rotate: '-315deg' }],
  },

  title: {
    fontSize: 20,
    color: '#38454f',
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 30,
    marginBottom: 12,
  },

  subTitle: {
    fontSize: 16,
    color: '#38454f',
    textAlign: 'center',
    paddingLeft: 20,
    paddingRight: 20,
  },

  emailInput: {
    width: '100%',
    height: 50,
    borderRadius: 5,
    backgroundColor: '#f8f8f8',
    borderWidth: 1,
    borderColor: '#d4dde1',
    textAlign: 'center',
    color: '#858f97',
    marginTop: 20,
  },

  footerText: {
    fontSize: 14,
    color: '#38454f',
    textAlign: 'center',
    marginTop: 27,
  }

});


