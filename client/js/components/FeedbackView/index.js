import React, { Component } from 'react';
import {
  StyleSheet,
  StatusBar,
  TextInput,
  Text,
  ActivityIndicator,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image,
  Platform, 
} from 'react-native';
import utils from '../../utils'

import Button from 'react-native-button'
import { API_URL } from 'react-native-dotenv'

export default class FeedbackView extends Component {
  componentDidMount(){
  }

  constructor(props) {
    super(props);
  }


  render(){

    var thumbs = {
      up: <Image
        source={require('../../images/thumbs-up-white.png')}
        style={{width: 80, height: 80, marginLeft: 'auto', marginRight: 'auto'}}
      />,

      upSelected: <Image
        source={require('../../images/thumbs-up-selected.png')}
        style={{width: 80, height: 80, marginLeft: 'auto', marginRight: 'auto'}}
      />,

      down: <Image
        source={require('../../images/thumbs-down-white.png')}
        style={{width: 80, height: 80, marginLeft: 'auto', marginRight: 'auto'}}
      />,

      downSelected: <Image
        source={require('../../images/thumbs-down-selected.png')}
        style={{width: 80, height: 80, marginLeft: 'auto', marginRight: 'auto'}}
      />,
    }

    
    return(
        <View style={styles.modalContainer}>
          <Text style={styles.title}>Thanks for using mycard</Text>
          <Text style={styles.subTitle}>How do you like the app so far?</Text>

          <View style={{width: '100%', flex: 1, flexDirection: 'row', marginTop: 25, marginBottom: 10, paddingLeft: 30, paddingRight: 30, justifyContent: 'space-around', alignItems: 'stretch'}}>
          
            <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', width: '100%'}}>
              <TouchableOpacity onPress={() => this.props.changeThumb('up') }>
                {this.props.selected === 'up' ? thumbs.upSelected : thumbs.up}
                <Text style={styles.like}>I like it</Text>
              </TouchableOpacity>
            </View>

            <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', width: '100%'}}>
              <TouchableOpacity onPress={() => this.props.changeThumb('down') }>
                {this.props.selected === 'down' ? thumbs.downSelected : thumbs.down}
                <Text style={styles.dislike}>Could be better</Text>
              </TouchableOpacity>
            </View>

          </View>

          <View style={styles.feedbackContainer}>
            <TextInput
              style={styles.feedbackInput}
              placeholder='Leave some feedback'
              value={this.props.message}
              onChangeText={(message) => this.props.changeFeedback(message)}
              placeholderTextColor='#858f97'
              underlineColorAndroid='rgba(0,0,0,0)'
              selectionColor='black'
              autoCorrect={false}
              multiline={true}
            />
          </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    width: '95%',
    marginTop: 0,
    borderRadius: 6,
    padding: 20,
    marginLeft: 'auto',
    marginRight: 'auto',
  },

  title: {
    fontSize: 16,
    color: '#38454f',
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 5,
  },

  subTitle: {
    fontSize: 15,
    color: '#858f97',
    textAlign: 'center',
    paddingLeft: 20,
    paddingRight: 20,
  },

  like: {
    color: '#04a34a',
    textAlign: 'center',
    marginTop: 7,
  },

  dislike: {
    color: '#d35452',
    textAlign: 'center',
    marginTop: 7,
  },

  feedbackContainer: {
    marginTop: 5,
    padding: 10,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#d4dde1',
  },

  feedbackInput: {
    width: '100%',
    height: 75,
    borderRadius: 5,
    color: '#38454f',
    fontSize: 14,
    textAlign: 'center',
  },

  footerText: {
    fontSize: 14,
    color: '#38454f',
    textAlign: 'center',
    marginTop: 27,
  }

});


