import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';


export default class FullWidthToggle extends Component {
  render(){
    var opts = this.props.options.map(function(option, ele){
      return <TouchableOpacity key={option.title} onPress={option.onPress}>
        <View style={styles.container}>
          <Text style={styles.header}>
            {option.title}
            <Text style={styles.description}>  {option.description}</Text>
          </Text>
          {option.selected ? <Text style={styles.arrow}>&#x2714;</Text> : <Text></Text>}
        </View>
      </TouchableOpacity>
    });

    return(
      <View>
        {opts}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexGrow: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    width: '100%', 
    paddingLeft: 10,
    paddingRight: 10,
    height: 50,
    flexBasis: 50,
    borderColor: '#e4e4e4',
    borderWidth: 1,
  },
  header: {
    fontSize: 14,
    flex: 1,
    flexDirection: 'column',
  },
  arrow: {
    fontSize: 20,
    color: '#1ea7e2',
    fontWeight: '600',
  },
  description: {
    fontSize: 12,
    paddingTop: 10,
  }
});


