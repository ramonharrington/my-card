import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from 'react-native';


export default class FullWidthText extends Component {
  render(){
    return(
      <View style={styles.container}>
        <TextInput style={styles.textInput} {...this.props} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexGrow: 0,
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    width: '100%', 
    paddingLeft: 10,
    paddingRight: 10,
    height: 50,
    flexBasis: 50,
    borderColor: '#e4e4e4',
    borderWidth: 0,
    marginBottom: 5,
  },
  textInput: {
    width: '100%',
  }
});


