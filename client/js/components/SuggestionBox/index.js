import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  ScrollView,
  View,
  TouchableOpacity,
} from 'react-native';


export default class SuggestionBox extends Component {
  constructor(){
    super();

    this.state = {
    };
  }

  getSuggestions(){

    if(this.props.input === null || this.props.input === ''){
      return [];
    }

    return this.props.data.reduce( (suggestions, contact, index) => {
      if(
        new RegExp(this.props.input.replace(/\+\d+/,'').replace(')','\\)').replace('(','\\(').toLowerCase()).test(contact.text.toLowerCase())
        && contact.text[0] !== '#'
      ){

        contact.key = index;
        suggestions.push(contact);
        return suggestions;
      }
      return suggestions;
    }, []);
  }

  render(){
    let suggestedContacts = this.getSuggestions();
    let suggestions = suggestedContacts.map( (suggestion) => {
      return <TouchableOpacity key={suggestion.key} onPress={() => this.props.onPress(suggestion)}>
        <View style={styles.suggestionRow}>
          <Text style={{color: '#38454f', fontSize: 16, fontWeight: 'bold'}}>{suggestion.name}</Text>
          <Text style={{color: '#38454f', fontSize: 14}}>{suggestion.type}  {suggestion.phone}</Text>
        </View>
      </TouchableOpacity>
    });


    if(suggestions.length === 0 || (suggestedContacts.length >= 1 && suggestedContacts[0].phone === this.props.input)){
      return <View style={{backgroundColor: 'red'}}></View>;
    }


    return (
      <ScrollView style={{width: '100%'}}>
        <View style={styles.innerContainer}>
          {suggestions}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  innerContainer: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    flexShrink: 0,
    flexGrow: 3,
    minHeight: 100,
  },
  suggestionRow: {
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'white',
    marginTop: 2,
    width: '100%',
    paddingLeft: 100,
    minHeight: 60,
    maxHeight: 60,
    flexShrink: 0,
  }
});


