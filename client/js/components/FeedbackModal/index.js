import React, { Component } from 'react';
import {
  StyleSheet,
  StatusBar,
  TextInput,
  Text,
  ActivityIndicator,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image,
  Platform, 
} from 'react-native';
import utils from '../../utils'

import Button from 'react-native-button'
import { API_URL } from 'react-native-dotenv'

export default class FeedbackModal extends Component {
  componentDidMount(){
    this.setState({email: this.props.email});
  }

  constructor(props) {
    super(props);

    this.state = {
      selected: null,
      message: '',
    };

    this.__sendFeedback = this.__sendFeedback.bind(this);
    this.__changeThumb = this.__changeThumb.bind(this);
    this.saveFeedback = this.saveFeedback.bind(this);
  }

  __sendFeedback() {
    fetch(API_URL + '/feedback', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userid: this.props.userid,
        email: this.props.email,
        rating: this.state.selected,
        message: this.state.message
      })
    })
  }

  saveFeedback(){
    utils.logEvent('send_feedback');
    this.__sendFeedback();
    this.props.saveFeedback(this.state.selected, this.state.message);
  }

  __changeThumb(upDown){
    utils.logEvent('send_feedback');
    this.setState({selected: upDown}, function(){
      this.__sendFeedback();
    }.bind(this));
  }

  render(){

    var thumbs = {
      up: <Image
        source={require('../../images/thumbs-up.png')}
        style={{width: 100, height: 100}}
      />,

      upSelected: <Image
        source={require('../../images/thumbs-up-selected.png')}
        style={{width: 100, height: 100}}
      />,

      down: <Image
        source={require('../../images/thumbs-down.png')}
        style={{width: 100, height: 100}}
      />,

      downSelected: <Image
        source={require('../../images/thumbs-down-selected.png')}
        style={{width: 100, height: 100}}
      />,
    }

    
    return(
      <View style={styles.veilContainer}>
        <View style={styles.modalContainer}>
          <Text style={styles.title}>Thanks for using mycard</Text>
          <Text style={styles.subTitle}>How do you like the app so far?</Text>

          <TouchableOpacity style={styles.closeTextContainer} onPress={() => this.props.close()}>
            <Text style={styles.closeText}>+</Text>
          </TouchableOpacity>

          <View style={{width: '100%', flex: 1, flexDirection: 'row', marginTop: 25, marginBottom: 100, paddingLeft: 30, paddingRight: 30, justifyContent: 'space-around', alignItems: 'stretch'}}>
          
            <TouchableOpacity onPress={() => this.__changeThumb('up') }>
              {this.state.selected === 'up' ? thumbs.upSelected : thumbs.up}
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.__changeThumb('down') }>
              {this.state.selected === 'down' ? thumbs.downSelected : thumbs.down}
            </TouchableOpacity>

          </View>

          <TextInput
            style={styles.feedbackInput}
            placeholder='please share some feedback with us'
            value={this.state.message}
            onChangeText={(message) => this.setState({message})}
            placeholderTextColor='#d4dde1'
            underlineColorAndroid='rgba(0,0,0,0)'
            selectionColor='black'
            autoCorrect={false}
            multiline={true}
          />

          <Button
            onPress={ this.saveFeedback }
            accessibilityLabel="Sign in using your vistaprint credentials"
            containerStyle={{marginTop: 20, padding:14, height:50, overflow:'hidden', borderRadius:4, backgroundColor: '#1ea7e2', width: '100%'}}
            style={{fontSize: 16, color: 'white'}}>

            SUBMIT FEEDBACK

          </Button>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  veilContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
    backgroundColor: 'rgba(1,1,1,0.7)',
    flex: 1,
  },

  modalContainer: {
    width: '95%',
    marginTop: 80,
    borderRadius: 6,
    padding: 20,
    backgroundColor: 'white',
    marginLeft: 'auto',
    marginRight: 'auto',
  },

  closeTextContainer: {
    position: 'absolute',
    top: 0,
    right: 10,
  },

  closeText: {
    fontSize: 40,
    fontWeight: '300',
    color: '#38454f',
    transform: [{ rotate: '-315deg' }],
  },

  title: {
    fontSize: 20,
    color: '#38454f',
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 30,
    marginBottom: 5,
  },

  subTitle: {
    fontSize: 16,
    color: '#38454f',
    textAlign: 'center',
    paddingLeft: 20,
    paddingRight: 20,
  },

  feedbackInput: {
    width: '100%',
    height: 100,
    borderRadius: 5,
    backgroundColor: '#f8f8f8',
    borderWidth: 1,
    borderColor: '#d4dde1',
    color: '#858f97',
    marginTop: 20,
    fontSize: 18,
  },

  footerText: {
    fontSize: 14,
    color: '#38454f',
    textAlign: 'center',
    marginTop: 27,
  }

});


