import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';


export default class FullWidthAdd extends Component {
  render(){
    return(
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={styles.container}>
          <Text style={styles.header}>{this.props.title}</Text>
          <Text style={styles.arrow}>{this.props.linkText ? this.props.linkText : '+ Add'}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexGrow: 1,
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    width: '100%', 
    paddingLeft: 10,
    paddingRight: 10,
    height: 50,
    flexBasis: 50,
    borderColor: '#e4e4e4',
    borderWidth: 0,
    marginBottom: 5,
  },
  header: {
    fontSize: 14,
  },
  arrow: {
    fontSize: 16,
    color: '#1ea7e2',
    paddingRight: 10,
  }
});


