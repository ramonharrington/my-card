import React, { Component } from 'react';
import {
  StyleSheet,
  StatusBar,
  Text,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image,
  Platform, 
} from 'react-native';


export default class Header extends Component {
  render(){

    // default is null
    let rightEle = <Text style={styles.headerText}></Text>
    let leftEle = <Text style={styles.fakeText}></Text>

    let ios = Platform.OS === 'ios';

    switch(this.props.rightIcon){
      case 'gear':
        let gearIcon = require('../../images/gear.png');
        rightEle = <Image style={styles.settingsIcon} source={gearIcon} />
        leftEle = <Text style={styles.fakeText}>Fake Tex</Text>
        break;
      case 'done':
        rightEle = <Text style={{color: '#ffffff', fontSize: 16, paddingRight: 20}}>Done</Text>
        leftEle = <Text style={styles.fakeText}>Done</Text>
        break;
      case 'send':
        rightEle = <Text style={{color: '#ffffff', fontSize: 16, paddingRight: 20}}>Send</Text>
        leftEle = <Text style={{color: '#ffffff', fontSize: 16, paddingLeft: 20}}>Cancel</Text>
        break;
      case 'skip':
        leftEle = <Text style={styles.fakeText}>      Skip</Text>
        rightEle = <Text style={{color: '#ffffff', fontSize: 16, paddingRight: 20}}>Skip</Text>
        break;
      case 'back':
        leftEle = <Text style={styles.fakeText}>      Back</Text>
        rightEle = <Text style={{color: '#ffffff', fontSize: 16, paddingRight: 20}}>Back</Text>
        break;
      case 'home':
        leftEle = <Text style={styles.fakeText}>      Home</Text>
        rightEle = <Text style={{color: '#ffffff', fontSize: 16, paddingRight: 20}}>Home</Text>
        break;
    }

    let headerStyle = {
      backgroundColor: '#006098',
      height: Platform.OS === 'ios' ? 70 : 50,
      paddingTop: Platform.OS === 'ios' ? 30 : 15,
      paddingBottom: 10,
      flexWrap: 'nowrap',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    };

    return(
      <View style={headerStyle}>
        <StatusBar backgroundColor='#006098' barStyle='light-content' />

        <TouchableOpacity onPress={this.props.leftPress}>
          { leftEle }
        </TouchableOpacity>

        <TouchableOpacity onPress={this.props.titlePress}>
          <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
            <Text style={styles.headerText}>{this.props.title}</Text>
            { this.props.titleIcon ? this.props.titleIcon : <View /> }
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={this.props.rightPress} onLongPress={ this.props.rightLongPress }>
          { rightEle }
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  settingsIcon: {
    alignSelf: 'flex-end',
    width: 24,
    height: 24,
    marginRight: 20,
  },
  headerText: {
    color: '#FFFFFF',
    fontSize: 18,
    fontWeight: '600',
    textAlign: 'center',
  },
  fakeText: {
    color: '#006098',
  }
});


