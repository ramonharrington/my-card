import { createStore, applyMiddleware } from "redux";
import {composeWithDevTools} from 'remote-redux-devtools';

import thunk from "redux-thunk";
import getRootReducer from "./reducers";

export default function getStore(navReducer) {
    const store = createStore(
        getRootReducer(navReducer),
        undefined,
        composeWithDevTools(applyMiddleware(thunk))
    );

    return store;
}

