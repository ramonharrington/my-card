var utils = require('../utils');


const initialState = {
  userData: {},
  vcards: [],
};

function insertItem(array, action) {
      let newArray = array.slice();
          newArray.splice(action.index, 0, action.item);
              return newArray;
}

function removeItem(array, action) {
      let newArray = array.slice();
          newArray.splice(action.index, 1);
              return newArray;
}

export default function mycard(state = initialState, action) {
  switch (action.type){


    case 'SET_USER_DATA':
      console.log("get user data reducer: " + action.userData.userid);

      return {
        ...state,
        userData : action.userData
      };

    case 'SET_CURRENT_LOCATION':
      console.log("setting current location");

      // Beacon support
      //utils.beacon(state.userData.userid, action.locationData);

      return{
        ...state,
        currentLocation: action.locationData,
      };

    case 'UPDATE_VCARD':
      let vcards = state.vcards;
      let index = vcards.findIndex( i => i.alt_doc_id === action.alt_doc_id);

      let newData = action.newData;
      newData.alt_doc_id = action.alt_doc_id;

      if(index < 0){
        vcards = insertItem(vcards, {index: 0, item: action.newData});
      }else{
        newData = vcards[index];
        Object.assign(newData, action.newData);

        vcards = removeItem(vcards, index);
        vcards = insertItem(vcards, {index, item: newData});
      }

      utils.updateVCard(state.userData.userid, newData);

      return{...state, vcards};


    case 'SET_VCARDS':
      return {
        ...state,
        vcards: action.vcards,
      };

    case 'ADD_HISTORY':
      let history = insertItem(state.userData.history, {index: state.userData.history.length, item: action.data});
      console.log("history: " + history.length);

      let newState = {
        ...state,
        userData: {
          ...state.userData,
          history
        }
      };

      utils.sync(newState.userData);
      return newState;



    default:
      return state;
  }
};
