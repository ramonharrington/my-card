/**
 * Created by stan229 on 5/27/16.
 */
import { combineReducers } from "redux";
import mycard from './mycard';

export default function getRootReducer(navReducer) {
  return combineReducers({
    nav: navReducer,
    mycard: mycard,
  });
}

