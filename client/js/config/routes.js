import LoadingScreen from '../containers/LoadingScreen';

import SignInIntro from '../containers/SignInIntro';
import SignIn from '../containers/SignIn';
import Tutorial from '../containers/Tutorial';


import CardHome from '../containers/CardHome';
import Settings from '../containers/Settings';
import ContactInfo from '../containers/ContactInfo';
import CardBig from '../containers/CardBig';

import CongratulationShare from '../containers/CongratulationShare';

import BusinessCardSelect from '../containers/BusinessCardSelect';

const Routes = {
  Main:                         {screen: LoadingScreen},
  SignInIntro:                  {screen: SignInIntro},
  SignIn:                       {screen: SignIn},
  BC:                           {screen: BusinessCardSelect},
  BCEmpty:                      {screen: require('../containers/BusinessCardEmpty')},
  Tutorial:                     {screen: Tutorial},
  CardHome:                     {screen: CardHome},
  CardBig:                      {screen: CardBig},
  CongratulationShare:          {screen: CongratulationShare},
  Settings:                     {screen: Settings},
  SecretSettings:               {screen: require('../containers/SecretSettings')},
  CreateAccount:                {screen: require('../containers/CreateAccount')},
  CreateAccountConfirmation:    {screen: require('../containers/CreateAccountConfirmation')},
  ContactInfo:                  {screen: ContactInfo},
}

export default Routes;
