'use strict';

const BASE_URL = 'https://vpmycard.ngrok.io/api/v1/';

const MCAPI = {
  getUserData(userid) {
    console.log('data: fetching user' + userid);
    return fetch(BASE_URL + '/user/' + userid);
  },
}

export default MCAPI;
