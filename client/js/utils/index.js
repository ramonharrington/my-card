import { Platform, Alert, Linking } from 'react-native';
import VersionNumber from 'react-native-version-number';
import AppStoreReview from 'react-native-app-store-review';
import Share from 'react-native-share';

import { Client } from 'bugsnag-react-native';
const bugsnag = new Client();
var ls = require('react-native-local-storage');

import { SERVER_ROOT, API_URL, S3_URL_BASE } from 'react-native-dotenv'

module.exports = new class{
  error(err){
    if(typeof err === 'string'){
      bugsnag.notify(new Error(err));
    }else{
      bugsnag.notify(err);
    }
  }

  bugsnagUser(userid, name, email){
    bugsnag.setUser(userid, name, email);
  }

  breadcrumb(type, msg){
    bugsnag.leaveBreadcrumb(msg, {type: type});
  }

  getUser(userid, callback){
    fetch(API_URL + '/user/' + userid, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    }).then(function(response){
      return response.json();
    }).then(function(response){
      if(typeof callback !== 'undefined' && callback !== null){
        callback(response);
      }
    });
  }


  sync(serverData, callback){
    serverData.platform = Platform.OS;
    serverData.appVersion = VersionNumber.appVersion;
    serverData.buildVersion = VersionNumber.buildVersion;
    serverData.timezoneOffset = new Date().getTimezoneOffset();

    //ls.get('tracking_data').then(function(tracking_data){
      //serverData.tracking_data = tracking_data;

      fetch(API_URL + '/updateUser', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(serverData)
      }).then(function(response){
        return response.json();
      }).then(function(response){
        //ls.save('tracking_data', []);

        if(typeof callback !== 'undefined' && callback !== null){
          callback(response);
        }
      });
    //});

  }

  logEvent(event_name){
    //ls.get('tracking_data').then(function(data){
      //if(typeof data === 'undefined' || data === null){
        //data = [];
      //}

      //data.push({
        //ts: new Date(),
        //event: event_name
      //});

      //ls.save('tracking_data', data);
    //});

    this.breadcrumb('navigation', event_name.substring(0,30));
  }

  sendSuccess(this2, ls, sendAddress, sendUrl, message, currentLocation, altDocId, callback){
    let serverData = this2.props.userData;

    if(typeof serverData.history === 'undefined'){
      serverData.history = [];
    }

    this2.props.addHistory({
      ts: Date.now(),
      recipient: sendAddress,
      message: message,
      location: currentLocation || {value: 'unknown', updated: false},
      card: altDocId,
      url: sendUrl,
    });

    callback();
  }

  dateFormatter(dateLongFormat){
    let dt = new Date(dateLongFormat);

    var day = dt.getDate();
    if(day < 10){
      day = '0'+day;
    }

    var month = dt.getMonth()+1;
    if(month < 10){
      month = '0'+month;
    }

    var year = dt.getFullYear();

    return year + '-' + month + '-' + day;
  }

  shareFB(shorturl, card, callback){
    let altdocid = card.alt_doc_id;
    let datauri = null;

    var shareImageUrl = S3_URL_BASE + altdocid + '/combined.png';

    if(card.combined_images && card.combined_images.wood_vertical){
      shareImageUrl = card.combined_images.wood_vertical;
    }
    console.log("sharing card " + altdocid);

    ls.get('datauris').then(function(uris){
      if(uris){
        for(let i=0; i<uris.length; i++){
          if(uris[i].alt_doc_id === altdocid){
            datauri = uris[i].datauri;
          }
        }
      }else{
        uris = [];
      }


      if(datauri === null){
        fetch(API_URL + '/datauri', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            url: shareImageUrl,
          })
        }).then(function(response){
          return response.json();
        }).then(function(response){
          datauri = response.datauri;
          
          uris.push({
            alt_doc_id: altdocid,
            datauri: datauri,
          });

          ls.save('datauris', uris);

          Share.shareSingle({
            title: 'My business card',
            subject: 'My business card',
            message: '',
            url: datauri,
            social: 'facebook'
          })
          .then( () => callback(true) )
          .catch(() => callback(false)) 
        });
      }else{
        Share.shareSingle({
          title: 'My business card',
          subject: 'My business card',
          message: '',
          url: datauri,
          social: 'facebook'
        })
        .then( () => callback(true) )
        .catch(() => callback(false)) 
      }
    }).catch( err => {
        fetch(API_URL + '/datauri', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            url: shareImageUrl,
          })
        }).then(function(response){
          return response.json();
        }).then(function(response){
          datauri = response.datauri;
          
          let uris = [];
          uris.push({
            alt_doc_id: altdocid,
            datauri: datauri,
          });

          ls.save('datauris', uris);

          Share.shareSingle({
            title: 'My business card',
            subject: 'My business card',
            message: '',
            url: datauri,
            social: 'facebook',
          })
          .then( () => callback(true) )
          .catch(() => callback(false)) 
        });
    });
  }

  shareCard(shorturl, card, sendLink, callback){
    let altdocid = card.alt_doc_id;
    let datauri = null;

    var shareImageUrl = S3_URL_BASE + altdocid + '/combined.png';

    if(card.combined_images && card.combined_images.wood_vertical){
      shareImageUrl = card.combined_images.wood_vertical;
    }
    console.log("sharing card " + altdocid);

    ls.get('datauris').then(function(uris){
      if(uris){
        for(let i=0; i<uris.length; i++){
          if(uris[i].alt_doc_id === altdocid){
            datauri = uris[i].datauri;
          }
        }
      }else{
        uris = [];
      }


      if(datauri === null){
        fetch(API_URL + '/datauri', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            url: shareImageUrl,
          })
        }).then(function(response){
          return response.json();
        }).then(function(response){
          datauri = response.datauri;
          
          uris.push({
            alt_doc_id: altdocid,
            datauri: datauri,
          });

          ls.save('datauris', uris);

          Share.open({
            title: 'My business card',
            subject: 'My business card',
            message: sendLink ? shorturl : '',
            url: datauri,
          })
          .then( () => callback(true) )
          .catch(() => callback(false)) 
        });
      }else{
        Share.open({
          title: 'My business card',
          subject: 'My business card',
          message: sendLink ? shorturl : '',
          url: datauri,
        })
        .then( () => callback(true) )
        .catch(() => callback(false)) 
      }
    }).catch( err => {
        fetch(API_URL + '/datauri', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            url: shareImageUrl,
          })
        }).then(function(response){
          return response.json();
        }).then(function(response){
          datauri = response.datauri;
          
          let uris = [];
          uris.push({
            alt_doc_id: altdocid,
            datauri: datauri,
          });

          ls.save('datauris', uris);

          Share.open({
            title: 'My business card',
            subject: 'My business card',
            message: sendLink ? shorturl : '',
            url: datauri,
          })
          .then( () => callback(true) )
          .catch(() => callback(false)) 
        });
    });
  }

  sendSlack(userid, msg){
    fetch(API_URL + '/slack', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userid: userid,
        message: msg,
      })
    });
  }

  showAppReview(force){
    if(Platform.OS === 'ios'){
      AppStoreReview.requestReview('1244185969');
    }else{
      let url = 'market://details?id=com.vistaprint.mycard';

      if(force){
        Linking.openURL(url);
        return;
      }

      ls.get('seen_review').then( seen_review => {
        ls.save('seen_review', true);

        if(seen_review){
          return;
        }

        Alert.alert(
          'Thanks for using mycard',
          'Would you like to leave us a review?',
          [
            {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'OK', onPress: () => Linking.openURL(url) },
          ],
          { cancelable: true }
        );

      });
    }
  }

  beacon(userid, currentLocation, socket){
    let platform = Platform.OS;
    let appVersion = VersionNumber.appVersion;
    let buildVersion = VersionNumber.buildVersion;

    socket.emit('beacon', {
      userid,
      currentLocation,
      platform,
      appVersion,
      buildVersion,
    });

    //fetch(API_URL + '/beacon', {
      //method: 'POST',
      //headers: {
        //'Accept': 'application/json',
        //'Content-Type': 'application/json',
      //},
      //body: JSON.stringify({
        //userid,
        //currentLocation,
        //platform,
        //appVersion,
        //buildVersion,
      //})
    //});
  }

  updateVCard(userid, data){
    fetch(API_URL + '/vcard', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({userid, ...data})
    }).then(function(response){
      return response.json();
    }).then(function(response){
    }).catch(function(err){
    });
  }

  getVcards(userid){
    return new Promise( resolve => {
      fetch(API_URL + '/vcard/' + userid, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
      }).then(function(response){
        return response.json();
      }).then(function(response){
        resolve(response);
      }).catch(function(err){
      });
    });
  }
}

