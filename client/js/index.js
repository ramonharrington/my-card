import React, { Component } from "react";
import { Text, BackHandler, Platform } from "react-native";
import { Provider, connect } from "react-redux";
import { StackNavigator, addNavigationHelpers } from "react-navigation";

import Routes from "./config/routes";
import getStore from "./store";

import { Client } from 'bugsnag-react-native';
const bugsnag = new Client();

const AppNavigator = StackNavigator(Routes, {
    navigationOptions: {
        //title: ({ state }) => {
            //if (state.params) {
                //return `${state.params.title}`;
            //}
        //}
    }
});

const navReducer = (state, action) => {
    const newState = AppNavigator.router.getStateForAction(action, state);
    return newState || state;
};

@connect(state => ({
    nav: state.nav
}))
class AppWithNavigationState extends Component {
    componentWillMount(){
      if (Platform.OS === 'android') {

        this.backButtonListener = BackHandler.addEventListener('hardwareBackPress', () => {
          const { dispatch, navigation, nav } = this.props;

          console.log("length: " + nav.routes.length);
          console.log("name: " + nav.routes[0].routeName);
          if (nav.routes.length === 1 && (nav.routes[0].routeName === 'SignInIntro' || nav.routes[0].routeName === 'CardHome')) {
            return false;
          }

          dispatch({ type: 'Navigation/BACK' });
          return true;
        });
      }
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress');
    }

    render() {
        return (
            <AppNavigator
                navigation={addNavigationHelpers({
                    dispatch: this.props.dispatch,
                    state: this.props.nav
                })}
            />
        );
    }
}

const store = getStore(navReducer);

export default function NCAP() {
    return (
        <Provider store={store}>
            <AppWithNavigationState />
        </Provider>
    );
}
