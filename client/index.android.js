/** Entry file for IOS **/
import { AppRegistry } from 'react-native';
import App from './js';

AppRegistry.registerComponent('BusinessCardApp', () => App);
