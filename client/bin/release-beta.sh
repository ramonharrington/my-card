#!/bin/bash
#####################################################
## Release script for getting beta version deployed
## 
## Release new version to ios and android
## Create the tag for this version
#####################################################

VERSION=$(grep version package.json | head -n 1 |cut -d'"' -f4)

cd android/
fastlane android beta

cd ../ios/
fastlane ios beta

echo $VERSION
git tag $VERSION
git push --tags

npm version patch
git add package.json ios/myCard/Info.plist
git commit -m "Version bump"
