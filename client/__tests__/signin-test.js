import 'react-native';
import React from 'react';
import SignIn from '../app/containers/SignIn';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const tree = renderer.create(
    <SignIn />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});
