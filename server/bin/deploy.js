#!/usr/bin/env node

var program = require('commander');
var execSync = require('child_process').execSync;
var chalk = require('chalk');
var Spinner = require('cli-spinner').Spinner;

program
  .version('0.0.1')
  .option('-b, --build <build>', 'Build Number')
  .option('-g, --group <group>', 'Deployment Group')
  .parse(process.argv);

if (!program.build) {
  console.log('Build number missing');
  return 1;
}

if (!program.group) {
  console.log('Deployment group missing: DEV or PROD: got: ' + program.group);
  return 1;
}

// make sure all files have a timestamp after 1980; otherwise the "aws deploy push" step fails with
// "ZIP does not support timestamps before 1980"
console.log(chalk.green('*') + ' Touching files to avoid ancient timestamps');
execSync(
  "find . -type f  -exec touch {} +",
  { stdio: 'inherit' });

console.log(chalk.green('*') + ' Uploading new deploy to s3://vpmycard/builds/build_v' + program.build + '.zip');
execSync(
  'aws deploy push --no-ignore-hidden-files --application-name MyCard --s3-location s3://vpmycard/builds/build_v' + program.build + '.zip --source .',
  { stdio: 'inherit' });

console.log(chalk.green('*') + ' Deploying version ' + program.build + ' to ' + program.group);
var createDeploymentOutput = execSync(
  'aws deploy create-deployment --application-name MyCard --s3-location bucket=vpmycard,key=builds/build_v' + program.build + '.zip,bundleType=zip --deployment-group-name ' + program.group
).toString();
console.log(createDeploymentOutput);

var deployId = JSON.parse(createDeploymentOutput).deploymentId;

var spinner = new Spinner(chalk.blue('\t* ') + ' deploying %s');

setInterval(function () {
  var getDeploymentOutput = execSync('aws deploy get-deployment --deployment-id ' + deployId).toString();
  console.log(getDeploymentOutput);

  var status = JSON.parse(getDeploymentOutput);

  if (status.deploymentInfo.status === 'Failed') {
    spinner.stop();
    console.log(chalk.red('\n** DEPLOYMENT FAILED ** ') + status.deploymentInfo.errorInformation.message);
    process.exit(1);
  }

  if (status.deploymentInfo.status === 'Succeeded') {
    spinner.stop();
    console.log(chalk.green('\n** DEPLOYMENT SUCCESS **'));
    process.exit(0);
  }

  console.log(chalk.green('\t*') + ' Deployment status: ' + status.deploymentInfo.status);
  spinner.start();
}, 20000);