var metrics = require('../lib/metrics');
var queue = require('../lib/queue').queue;

var logger = require('../lib/logger');
var migrateReporting = require('../lib/migrateReporting');

metrics.inc('worker_migrate_reporting_data');
queue.create('migrate_reporting_data', {}).save(function(err){});
