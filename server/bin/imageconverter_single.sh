#!/bin/bash
## Take an alt doc id for a business card and return the trimmed versions and combined

## Step 1: Create a temporary place to work in
basedir=$(pwd)
mydir=$(mktemp -d)
cd $mydir

## Step 2: Get the card
curl -Ls "http://www.vistaprint.com/any/lp.aspx?nochrome=0&alt_doc_id=$1&page=1&width=2000" -o page1.jpg
curl -Ls "http://www.vistaprint.com/any/lp.aspx?nochrome=0&alt_doc_id=$1&page=2&width=2000" -o page2.jpg

convert page1.jpg -shave 40x40 page1_trimmed.jpg
convert page2.jpg -shave 40x40 page2_trimmed.jpg

convert page1_trimmed.jpg page1_trimmed.png
convert page2_trimmed.jpg page2_trimmed.png

## Step 3: the combined image is really just the first card

## Step 4:  Put on wood background
montage -gravity center -shadow page1_trimmed.jpg -texture $basedir/textures/wood.png -geometry 400x300+30+30 combined_wood_orig.png
montage -gravity center -shadow page1_trimmed.jpg -texture $basedir/textures/wood.png -geometry 1200x900+60+60 combined_wood.jpg

montage -gravity center -shadow page2_trimmed.jpg -texture $basedir/textures/wood.png -geometry 1200x900+60+60 combined_wood_reverse.jpg

convert page1_trimmed.png combined_orig.png


## Step 5: Compress PNGs
pngquant combined_orig.png --output combined.png > /dev/null
pngquant combined_wood_orig.png --output combined_wood.png > /dev/null


## Step 5:  Return JSON with the file paths
echo "{\"altdocid\": \"$1\", \"tempdir\":\"$mydir\", \"front\":\"$mydir/page1_trimmed.jpg\", \"back\":\"$mydir/page2_trimmed.jpg\", \"combined\":\"$mydir/page1_trimmed.jpg\", \"combined_reverse\":\"$mydir/page2_trimmed.jpg\", \"combined_wood\":\"$mydir/combined_wood.jpg\", \"combined_wood_reverse\": \"$mydir/combined_wood_reverse.jpg\"}"
