#!/bin/bash
## Take an alt doc id for a business card and return the trimmed versions and combined

## Step 1: Create a temporary place to work in
basedir=$(pwd)
mydir=$(mktemp -d)
cd $mydir

## Step 2: Get the card
curl -Ls "http://www.vistaprint.com/any/lp.aspx?nochrome=0&alt_doc_id=$1&page=1&width=2000" -o page1.jpg
curl -Ls "http://www.vistaprint.com/any/lp.aspx?nochrome=0&alt_doc_id=$1&page=2&width=2000" -o page2.jpg

convert page1.jpg -shave 40x40 page1_trimmed.jpg
convert page2.jpg -shave 40x40 page2_trimmed.jpg

convert page1_trimmed.jpg page1_trimmed.png
convert page2_trimmed.jpg page2_trimmed.png

## Step 3:  Combine the images into one overlapping diagonally
convert \
  -size 8000x8000 xc:transparent \
  page2_trimmed.jpg -geometry +100+100 -composite \
  page1_trimmed.jpg -geometry +0+0 -composite -shave 17x17  \
  -trim combined_orig.png

convert \
  -size 8000x8000 xc:transparent \
  page1_trimmed.jpg -geometry +100+100 -composite \
  page2_trimmed.jpg -geometry +0+0 -composite -shave 17x17  \
  -trim combined_reverse_orig.png



## Step 4:  Combine onto wood background
montage -gravity center -shadow page1_trimmed.jpg page2_trimmed.jpg -tile 1x2 -texture $basedir/textures/wood.png -geometry 400x300+30+30 combined_wood_orig.png
montage -gravity center -shadow page1_trimmed.jpg page2_trimmed.jpg -tile 1x2 -texture $basedir/textures/wood.png -geometry 800x600+30+12 combined_wood.jpg

montage -gravity center -shadow page2_trimmed.jpg page1_trimmed.jpg -tile 1x2 -texture $basedir/textures/wood.png -geometry 800x600+30+12 combined_wood_reverse.jpg


## Step 5: Compress PNGs
pngquant combined_orig.png --output combined.png > /dev/null
pngquant combined_reverse_orig.png --output combined_reverse.png > /dev/null
pngquant combined_wood_orig.png --output combined_wood.png > /dev/null


## Step 6:  Return JSON with the file paths
echo "{\"altdocid\": \"$1\", \"tempdir\":\"$mydir\", \"front\":\"$mydir/page1_trimmed.jpg\", \"back\":\"$mydir/page2_trimmed.jpg\", \"combined\":\"$mydir/combined.png\", \"combined_reverse\":\"$mydir/combined_reverse.png\", \"combined_wood\":\"$mydir/combined_wood.jpg\", \"combined_wood_reverse\": \"$mydir/combined_wood_reverse.jpg\"}"
