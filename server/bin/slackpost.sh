#!/bin/bash

# Usage: slackpost <channel> <message>

# Enter the name of your slack host here - the thing that appears in your URL:
# https://slackhost.slack.com/
slackhost="hatchery-team"

channel=$1
if [[ $channel == "" ]]
then
        echo "No channel specified"
        exit 1
fi

shift

text=$*

if [[ $text == "" ]]
then
        echo "No text specified"
        exit 1
fi

escapedText=$(echo $text | sed 's/"/\"/g' | sed "s/'/\'/g" )
json="{\"icon_emoji\": \":jenkins_ci:\", \"username\": \"Jenkins\", \"channel\": \"#$channel\", \"text\": \"$escapedText\"}"

echo "payload: " + $json
curl -s -d "payload=$json" "https://hooks.slack.com/services/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"
