#!/usr/bin/env node

// Daily stats for mycard
// Run at 12:01 at night for previous day
const mongoose = require('mongoose');
const db = require('../models/db');
const User = mongoose.model('User');
const Stat = mongoose.model('AppStat');


async function getStats(){

  return new Promise( async (resolve) => {
    // total users on each platform / version
    let stats = {};
    stats.buildVersions = {};

    let results = await User.find().distinct('buildVersion');

    for(let i=0; i<results.length; i++){
      let ver = results[i];
      stats.buildVersions[ver] = await User.find({buildVersion: ver}).count();
    }


    // Total active users in last 24 hours
    let yest = new Date(new Date() - (1000 * 60 * 60 * 24));
    stats.activeUsers = await User.find({modified: {$gt: yest} }).count();

    resolve(stats);
  });
}


getStats().then( async (stats) => {
  //console.log("stats is: " + JSON.stringify(stats));
  var stat = new Stat({
    created: Date.now(),
    stats,
  });

  await stat.save();
  process.exit();
});

