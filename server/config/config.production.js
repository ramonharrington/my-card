var config = require('./config.global');

config.env = 'production';
config.urlbase = 'https://mycard.vistaprint.com';
config.monolithurl = 'http://www.vistaprint.com';
config.forks = 4;

config.cs.endpoint = 'search-mycard-prod-2tlh52gieb4paclzvlmhigozwm.us-east-1.cloudsearch.amazonaws.com';

config.connections.mongo = 'mongodb://mycard:hatchery1!@ds149031-a0.mlab.com:49031,ds149031-a1.mlab.com:49031/mycard_prod?replicaSet=rs-ds149031';
config.connections.redis = 'redis://mycard-prod.dlxrb1.ng.0001.use1.cache.amazonaws.com:6379';

config.s3.folder = 'bizcards',
config.sqs.url = 'https://sqs.us-east-1.amazonaws.com/712043751429/mycard-prod-2';

config.mandrill.key = 'Oi-wV5R51E060FCVOTd0cA';

config.sumologic.endpoint = 'https://endpoint1.collection.us2.sumologic.com/receiver/v1/http/ZaVnC4dhaV1dZt6mscWpxVDNQx3vlUiJ0kaZ8782OmjiPDZpIkXkRB7v8UfvzPx7FNMpwtiF3zo1WbHZqyB0tGqRol7rduwIdYrSh0tL2VAYMiHnNtiSDQ==';

module.exports = config;
