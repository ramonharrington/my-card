var config = require('./config.global');
config.env = 'test';

config.urlbase = 'https://test.mycard.vistaprint.com';
config.monolithurl = 'http://www.vptest.com';
config.forks = 2;

//config.connections.mongo = 'mongodb://mycard:hatchery1!@ds161960.mlab.com:61960/mycard_dev';
config.connections.mongo = 'mongodb://mycard:hatchery1!@mycard-tst.cluster-cigogjiyqq2f.us-east-1.docdb.amazonaws.com:27017/mycard_prod?ssl_ca_certs=rds-combined-ca-bundle.pem&replicaSet=rs0';
config.connections.redis = 'redis://mycard-test-2.dlxrb1.0001.use1.cache.amazonaws.com:6379';

config.s3.folder = 'bizcards_test',
config.sqs.url = 'https://sqs.us-east-1.amazonaws.com/712043751429/mycard-test';

config.sumologic.endpoint = 'https://endpoint1.collection.us2.sumologic.com/receiver/v1/http/ZaVnC4dhaV0v1hEGTGuYJg6TdkucEGuY-7fS5aQWp947qmZIq8LoQTfRlOtP3MtwueXcwOjZ1dqitg-HWCSr0YS-pJAbjwudkQHHs-4TLfw_RaN4qNbNUw==';

module.exports = config;
