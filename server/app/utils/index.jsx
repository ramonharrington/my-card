import axios from 'axios';
import async from 'async';

var dateFormat = require('dateformat');

class Utils {
  static dateFormat(dt, dts){
    return dateFormat(dt, dts);
  }

  static formatDate(dts){
    var dt = new Date(dts);

    var formatted = dt.getFullYear() + '-';
    if(dt.getMonth() < 10){
      formatted += '-0' + dt.getMonth();
    }else{
      formatted += '-' + dt.getMonth();
    }

    if(dt.getDate() < 10){
      formatted += '-0' + dt.getDate();
    }else{
      formatted += '-' + dt.getDate();
    }

    if(dt.getHours() < 10){
      formatted += ' 0' + dt.getHours();
    }else{
      formatted += ' ' + dt.getHours();
    }

    if(dt.getMinutes() < 10){
      formatted += ':0' + dt.getMinutes();
    }else{
      formatted += ':' + dt.getMinutes();
    }

    if(dt.getSeconds() < 10){
      formatted += ':0' + dt.getSeconds();
    }else{
      formatted += ':' + dt.getSeconds();
    }

    return formatted;
  }

  static getAllUsers(callback){
    var calls = [];

    // temporary until I add in a user count api call
    calls.push(function(callback){ this.getUsers(0, 3000, callback) }.bind(this));
    calls.push(function(callback){ this.getUsers(3000, 3000, callback) }.bind(this));
    calls.push(function(callback){ this.getUsers(6000, 3000, callback) }.bind(this));

    async.series(calls, function(err, result){
      callback([].concat.apply([], result));
    });
  }

  static getUsers(offset, limit, callback){
    axios.get('/api/admin/users?offset='+offset+'&limit='+limit)
    .then(function(resp){
      if(!resp.data){
        callback(null, []);
        return;
      }

      callback(null, resp.data.sort(function(a,b){ return a.modified < b.modified ? 1 : -1; }));
    });
  }
};

module.exports = Utils;


