import React from 'react';
import styles from './SmallStat.scss'
import { Loader } from 'semantic-ui-react'

module.exports = React.createClass({

  render: function(){
    return(
      <div className='smallstat-container'>

        <div className='image'><img src={this.props.icon} /></div>
        <div className='title'>{this.props.title}</div>
        <div className='value'>{this.props.value === 0 ? <Loader active /> : this.props.value}</div>
        <div className='footer'>{this.props.footer}</div>

      </div>
    );
  }
});
