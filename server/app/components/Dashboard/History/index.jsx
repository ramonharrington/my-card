import React, {Component} from 'react';
import { Accordion, Grid, Header, Table, Button, Icon, Radio, Dimmer, Segment, Modal } from 'semantic-ui-react'
import style from './History.scss';


class HistoryBox extends Component{
  render(){
    let s3bucket = /mycard\.vistaprint\.com/.test(window.location.host) ? 'bizcards' : 'bizcards_dev';
    let s3host = 'https://s3.amazonaws.com/vpmycard/' + s3bucket + '/';

    let history = this.props.user.history.map(function(history){
      return <div className='history-container-item'>
        <img src={s3host + history.card + '/front.png'} />
        <div className='location'>{history.location ? history.location.value : 'unknown'}</div>
        <div className='date'>{new Date(history.ts).getMonth()+1}/{new Date(history.ts).getDate()}</div>
      </div>
    });


    return (
      <div className='history-container'>
        <h2>History ({this.props.user.history.length})</h2>
        {this.props.user.history.length > 0 ? history : <div className='white-container'>No sends</div>}
      </div>
    );
  }
}

export default HistoryBox
