import React, {Component} from 'react';
import { Accordion, Grid, Header, Table, Button, Icon, Radio, Dimmer, Segment, Modal } from 'semantic-ui-react'


class DashboardUserBox extends Component{
  render(){

    let tokenAdminDiv = <div style={{marginTop: 20}}>
      Login token: {this.props.token}
      <div>
        <Button size='small' onClick={this.props.requestToken}>generate new token</Button>
      </div>
    </div>

    let tokenDiv = <div style={{marginTop: 20}}>
      Login token: {this.props.token}
    </div>

    let showBadges = false;
    if(this.props.user.platform === 'ios' && this.props.user.buildVersion < 45){
      showBadges = true;
    }else if(this.props.user.platform === 'android' && this.props.user.buildVersion < 750){
      showBadges = true;
    }

    let badges_earned = this.props.user.badges.collected.map(function(badge){
      return <div className='badge'>
        <img
          src={'/images/badges/'+badge.icon}
          title={badge.title}
          alt={badge.description}
        />
      </div>
    });

    let badges_available = this.props.user.badges.available.map(function(badge){
      return <div className='badge available'>
        <img
          src={'/images/badges/'+badge.icon}
          title={badge.title}
          alt={badge.description}
          style={{ opacity: badge.percentage+0.1 }}
        />
      </div>
    });

    return (
      <div className='white-container'>
        <div>{this.props.user.first_name} {this.props.user.last_name} (<a href={'http://www.us.vpcustomercare.com/vp/manager/customercare_ns/results.aspx?institution_id=1&criteria=shopper_key&scriteria=' + this.props.user.shopperkey} target='_blank'>CCT</a>)</div> 
        <div className='userid' style={{position: 'absolute', right: 5, top: 5}}>{this.props.user.userid}</div>
        <div>{this.props.user.email}</div>
        <div>Version: <strong>{this.props.user.platform}</strong> version <strong>{this.props.user.appVersion}</strong>  build <strong>{this.props.user.buildVersion}</strong></div>
  
        <div className='badges-container'>
          { this.props.user.buildVersion < 45 ? badges_earned : [] }
          { this.props.user.buildVersion < 45 ? badges_available : [] }
        </div>
  
        {this.props.admin ? tokenAdminDiv : tokenDiv}
        <div className='user-platform'>
        <Icon size='massive' name={this.props.user.platform === 'ios' ? 'apple' : !this.props.user.platform ? 'question' : 'android'} />
        </div>
  
      </div>
    );
  }
}

export default DashboardUserBox
