import React from 'react'
import styles from './AppStoreLinks.sass'

export default class AppStoreLinks extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      appStoreLink: 'https://itunes.apple.com/us/app/mycard-your-business-cards-mobile-companion/id1244185969?ls=1&mt=8',
      playStoreLink: 'https://play.google.com/store/apps/details?id=com.vistaprint.mycard&hl=en'
    }
  }

  goToAppStore() {
    return this.__trackClick(this.state.appStoreLink);
  }

  goToPlayStore() {
    return this.__trackClick(this.state.playStoreLink);
  }

  __trackClick(target) {
    ga('send', 'event', 'outbound', 'click', target, {
     'transport': 'beacon',
     'hitCallback': function(){document.location = target;}
   });
   return false;
  }

  render() {
    return (
      <div className="app-store-links">
        <a href={this.state.appStoreLink} target='_blank' onClick={this.goToAppStore.bind(this)}>
          <img src="/images/pp/app-store/Apple@2x.png" srcSet="/images/pp/app-store/Apple@2x.png 2x, /images/pp/app-store/Apple@3x.png 3x" alt="mycard on the App Store"/>
        </a>
        <a href={this.state.playStoreLink} target='_blank' onClick={this.goToPlayStore.bind(this)}>
          <img src="/images/pp/app-store/Google@2x.png" srcSet="/images/pp/app-store/Google@2x.png 2x, /images/pp/app-store/Google@3x.png 3x" alt="mycard on the Google Play Store"/>
        </a>
      </div>
    );
  }
}