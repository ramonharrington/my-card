import React from 'react';
import styles from './LinkSmsInput.sass';

import axios from 'axios';
import classNames from 'classnames';

export default class LinkSmsInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      recipient: '',
      hasSent: false
    };
  }

  render() {
    if (this.state.hasSent) {
      return (
        <div className="sent-link">
          <img className="check" src="images/check.png" srcset="images/check@2x.png 2x,images/check@3x.png 3x" />
          <span className="sent-text">Your download link has been sent</span>
        </div>
      );
    }

    return (
      <div className="get-link">
        <input placeholder="Enter your phone number"
                defaultValue={this.state.recipient}
                onChange={(evt) => this.setState({ recipient: evt.target.value}) }/>
        <button className={classNames({sent: this.state.hasSent})} onClick={ this.sendLink.bind(this) }>Send me the link</button>
      </div>
    );
  }

  sendLink(){
    ga('send', 'event', 'sms-link', 'send', this.state.recipient);

    axios.get('/api/v1/sendLink/' + this.state.recipient)
      .then(function(resp){
        this.setState({hasSent: true});
      }.bind(this));
  }
}