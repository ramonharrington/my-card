import React from 'react';
import axios from 'axios'

import { Search, Grid, Header, Table, Button, Icon } from 'semantic-ui-react'
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps";
import {default as MarkerClusterer} from 'react-google-maps/lib/addons/MarkerClusterer'

var utils = require('../../utils/index.jsx');

module.exports = React.createClass({
  displayName: "Users Map",

  componentDidMount: function(){
    this.updateData();
  },

  updateData: function(){
    axios.get('/api/admin/stats/locations')
      .then(function(resp){
        this.setState({coords: resp.data});
      }.bind(this));
  },

  getInitialState: function(){
    return {
      largeMap: false,
      coords: [],
    };
  },

  shouldComponentUpdate: function(nextProps, nextState){
    if(this.state.coords.length !== nextState.coords.length){
      return true;
    }

    if(this.state.largeMap !== nextState.largeMap){
      return true;
    }

    return false;
  },

  render: function(){
    var coords = this.state.coords.map(function(coord){
      return <Marker
        key={coord.key}
        position={{lat: coord.lat, lng: coord.lng}}
        icon='/images/pin.png?'
      />
    }.bind(this));

    const GettingStartedGoogleMap = withGoogleMap(props => (
      <GoogleMap
        ref={props.onMapLoad}
        defaultZoom={3}
        defaultCenter={{ lat: 40.850033, lng: -97.6500523 }}
      >
        <MarkerClusterer clusterClass='cluster-group'>
          { coords }
        </MarkerClusterer>
      </GoogleMap>
    ));


    return <div id='user-map-wrapper'>
      <Icon className='maximize-icon' name='maximize' size='large' onClick={ () => this.setState({largeMap: !this.state.largeMap}) } />

      {this.state.largeMap ? <div className='veil' onClick={ () => this.setState({largeMap: false}) }></div> : <div></div>}

      <GettingStartedGoogleMap
        containerElement={
          <div className={this.state.largeMap ? 'large-map' : ''} style={{height: (this.state.largeMap ? '80%' : '300px'), width: '100%'}} />
        }
        mapElement={
          <div style={{ height: '100%', borderRadius: '5px' }} />
        }
      />
    </div>
  }
})

