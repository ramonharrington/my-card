import React from 'react';
import styles from './UserSearch.scss'
import axios from 'axios'
import io from 'socket.io-client';
import { Link } from 'react-router';

import { Search, Grid, Header, Table, Button, Icon } from 'semantic-ui-react'

import { browserHistory } from 'react-router';

module.exports = React.createClass({
  displayName: "UserSearch",

  getInitialState: function(){
    return {
      searchTimerId: null,
      searchTimeout: null,
      searchResults: [],
    };
  },

  render: function(){

    return <div id='user-search-container'>
      <Search
        loading={this.state.amSearching}
        onResultSelect={ (e, results) => {
          this.props.onResultSelect(results.result.description);
        }}
        placeholder="Search for user's name, email, or shopper key"
        onSearchChange={ e => {

          clearTimeout(this.state.searchTimerId);
          this.setState({searchTimerId: null});

          var query = e.target.value;
          if(query === ''){
            return;
            this.setState({amSearching: false});
          }

          let timerId = setTimeout( () => {
            this.setState({amSearching: true});

            axios.get('/api/v1/search?q='+query).then(function(resp){
              var results = resp.data.map( user => {
                return {
                  key: user.userid,
                  title: user.first_name + ' ' + user.last_name,
                  description: user.email,
                  image: user.default_card ? user.default_card.front_img : '',
                };
              });

              this.setState({searchResults: results});
              this.setState({amSearching: false});
            }.bind(this));
          }, 500);

          this.setState({searchTimerId: timerId});
        }}
        results={this.state.searchResults}
        value={this.state.searchValue}
      />
    </div>
  }
})

