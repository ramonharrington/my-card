import React from 'react';
import { Link, browserHistory } from 'react-router';
import styles from './DashboardNav.scss'

module.exports = React.createClass({

  render: function(){
    const navItems = [
      {
          title: 'Dashboard',
          link: '/dashboard',
      },
      {
        title: 'Pushes',
        link: '/dashboard/pushes',
      },
      {
        title: 'Sends',
        link: '/dashboard/sends',
      },
      {
        title: 'Referrals',
        link: '/dashboard/referrals',
      },
      {
        title: 'Login Performance',
        link: '/dashboard/perf',
      },
      {
        title: 'System Status',
        link: '/dashboard/status',
      },
      {
        title: 'Kue',
        link: '/kueapi',
        external: true
      }
    ];


    let navItemObj = navItems.map( navItem => {
      let classes = ['menuitem'];
      if(this.props.active === navItem.title){
        classes.push('active');
      }

      if(navItem.external){
        return (
          <span className={classes.join(' ')} onClick={() => window.location = navItem.link }>{navItem.title}</span>
        );
      }

      return (
        <span className={classes.join(' ')} onClick={() => browserHistory.push(navItem.link) }>{navItem.title}</span>
      );
    });

    return(
      <div className='dashboardnav-container'>
        { navItemObj }
      </div>
    );
  }
});




