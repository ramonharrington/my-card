import React from 'react';
import styles from './PieChart.scss'

import {PieChart, Pie, Sector, Cell, Legend, ResponsiveContainer} from 'recharts'

module.exports = React.createClass({
	render () {
      let colors = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
      if (typeof this.props.data == 'undefined' || this.props.data == null) {
          return <div></div>;
      }

      var cells = this.props.data.map((entry, index) => <Cell fill={colors[index % colors.length]}/>);
      
      return (
        <div>
          <PieChart width={350} height={200}>
            <Pie data={this.props.data} dataKey="name" cx={75} cy={75} outerRadius={70} fill="#8884d8" width={350} height={200}>
              {cells}
            </Pie>
          </PieChart>
        </div>
    );
  }
});
