import React from 'react';
import styles from './VideoModal.scss';


export default class VideoModal extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div>
        <div className='veil' onClick={this.props.close}></div>
        <div className='video-modal-container'>
          <div className='video-modal'>
            <div className='close' onClick={this.props.close}>X</div>

            <video
              controls="true"
              autoPlay
              src="https://s3.amazonaws.com/vpmycard/pp/about.mp4"
            />
          </div>
        </div>
      </div>
    );
  }
}

