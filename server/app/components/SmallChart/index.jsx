import React from 'react';
import styles from './SmallChart.scss'

import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts'

module.exports = React.createClass({

  render: function(){
    const data = [
      {name: 'Page A', uv: 4000, pv: 2400, amt: 2400},
      {name: 'Page B', uv: 3000, pv: 1398, amt: 2210},
      {name: 'Page C', uv: 2000, pv: 9800, amt: 2290},
      {name: 'Page D', uv: 2780, pv: 3908, amt: 2000},
      {name: 'Page E', uv: 1890, pv: 4800, amt: 2181},
      {name: 'Page F', uv: 2390, pv: 3800, amt: 2500},
      {name: 'Page G', uv: 3490, pv: 4300, amt: 2100},
    ];

    let mydata = this.props.value.reduce(function(total, ele){
      var dt = new Date(ele.time);
      var today= new Date();
      var oneWeekAgo = new Date(today.getTime() - (60*60*24*7*1000));

      if(dt >= oneWeekAgo){
        var graphItem = total.filter(function(t) { return t.date == ele.time; });
        if (graphItem.length == 0) {
          total.push({
            day: dt.getDate(),
            date: ele.time,
            Android: (ele.breakdowns.client === "android") ? parseInt(ele.value) : 0,
            iPhone: (ele.breakdowns.client === "ios_unknown") ? parseInt(ele.value) : 0,
            Total: parseInt(ele.value)
          });
        } else {
          graphItem[0].Android += ((ele.breakdowns.client === "android") ? parseInt(ele.value) : 0);
          graphItem[0].iPhone += ((ele.breakdowns.client === "ios_unknown") ? parseInt(ele.value) : 0);
          graphItem[0].Total += parseInt(ele.value);
        }
      }

      return total;
    },[]);

    return(
      <div className='smallstat-container smallchart-container'>
        <div className='title'>{this.props.title}</div>

        <ResponsiveContainer height={200}>
          <LineChart data={mydata}>
            <XAxis dataKey="day" />
            <YAxis domain={[-5, 100]} />

            <Tooltip/>
            <Legend />

            <Line type="monotone" dataKey="Total" stroke="darkblue" />
            <Line type="monotone" dataKey="iPhone" stroke="lightblue" />
            <Line type="monotone" dataKey="Android" stroke="green" />
          </LineChart>
        </ResponsiveContainer>
      </div>
    );
  }
});
