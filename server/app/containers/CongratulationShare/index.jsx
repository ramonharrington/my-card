import React from 'react';
import styles from './CongratulationShare.sass';
export default class CongratulationShare extends React.Component {

  render() {
    return (
      <div className='congratulation-share'>
		<div className='logo'>
			<img src="/images/facebook-app-logo.png"></img>
		</div>
		<div className='headline'>
			Have you tried sharing to Facebook?
		</div>
		<div className='content'>
			Connect to a larger audience and give your card more visibility. Add <strong>#mycard</strong> to be seen in our social community.
		</div>
	  </div>
    )
  }
}