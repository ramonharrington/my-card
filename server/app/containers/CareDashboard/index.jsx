import React from 'react';
import styles from './CareDashboard.scss'
import axios from 'axios'
import io from 'socket.io-client';
import { Link } from 'react-router';

import { Search, Grid, Header, Table, Button, Icon } from 'semantic-ui-react'
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps";
import {default as MarkerClusterer} from 'react-google-maps/lib/addons/MarkerClusterer'

import SmallStat from '../../components/SmallStat/index.jsx';
import SmallChart from '../../components/SmallChart/index.jsx';
import UserMap from '../../components/UserMap/index.jsx';
import UserSearch from '../../components/UserSearch/index.jsx';

import { browserHistory } from 'react-router';

var utils = require('../../utils/index.jsx');

module.exports = React.createClass({
  displayName: "Dashboard",

  componentDidMount: function(){
    this.updateData();

    var socket = io.connect(window.location.protocol + '//'+window.location.host, {
      transports: ['websocket']
    });

    socket.on('dashUpdates', function(data){
      this.updateData();
    }.bind(this));

    setTimeout(function(){
      this.updateData();
    }.bind(this), 1000 * 60 * 60 * 1); // update every hour
  },

  updateData: function(){
    axios.get('/api/admin/stats/sends')
      .then(function(resp){
        this.setState({sends: parseInt(resp.data)});
      }.bind(this));

    axios.get('/api/admin/stats/opens')
      .then(function(resp){
        this.setState({opens: parseInt(resp.data)});
      }.bind(this));

    axios.get('/api/admin/stats/monthlyUsers')
      .then(function(resp){
        this.setState({active_users: parseInt(resp.data)});
      }.bind(this));

    utils.getUsers(0, 20, function(err, users){
      this.setState({users: users});
    }.bind(this));


    axios.get('/api/admin/stats/locations')
      .then(function(resp){
        this.setState({coords: resp.data});
      }.bind(this));

    // Facebook data
    axios.get('https://graph.facebook.com/v2.9/1783015601724931/app_insights/app_event?event_name=fb_mobile_first_app_launch&period=daily&breakdowns[0]=client&access_token=1783015601724931|abe456cf36368e04265078b40ea376b2&aggregateBy=COUNT')
      .then(function(resp){
        this.setState({daily_installs: resp.data.data});
      }.bind(this));

    axios.get('https://graph.facebook.com/v2.9/1783015601724931/app_insights/app_event?event_name=Clicked%20Share%20Card&period=daily&breakdowns[0]=client&access_token=1783015601724931|abe456cf36368e04265078b40ea376b2&aggregateBy=COUNT')
      .then(function(resp){
        this.setState({daily_sends: resp.data.data});
      }.bind(this));
  },

  getInitialState: function(){
    return {
      users: [],
      daily_installs: [],
      daily_sends: [],
      active_users: 0,
      sends: 0,
      opens: 0,
      searchTimerId: null,
      searchTimeout: null,
      coords: [],
      showMapUser: '',
      searchResults: [],
    };
  },

  _gotoUser: function(userid){
    browserHistory.push('/care/user/'+userid);
  },

  removeUser: function(evt, userid){
    evt.preventDefault();
    evt.stopPropagation();

    if(confirm('Are you sure you want to remove this user?')){
      axios.delete('/api/admin/user/'+userid)
        .then(function(resp){
          axios.get('/api/admin/users')
            .then(function(resp){
              this.setState({users: resp.data});
            }.bind(this));
        }.bind(this));
    }
  },

  getMapUser: function(userid){
    axios.get('/api/v1/user/' + userid)
      .then(function(resp){
        this.setState({showMapUser: resp.data.default_card.front_img});
      }.bind(this));

    return true;
  },

  render: function(){
    let users_row = [];

    for(let i=0; i<this.state.users.length && i <= 10; i++){
      let user = this.state.users[i];
      let image = <div>--- not chosen ---</div>

      if(typeof user.default_card !== 'undefined'){
        image = <img src={user.default_card.front_img} />
      }

      var platform_icon = <div></div>;
      if(user.platform){
        platform_icon = <Icon name={user.platform === 'ios' ? 'apple' : 'android'} />
      }


      users_row.push(
        <Table.Row key={user.userid} className='user-row'>
          <Table.Cell onClick={() => this._gotoUser(user.userid) } style={{textAlign: 'center'}}>
            { image }
            { platform_icon }
          </Table.Cell>
          <Table.Cell onClick={() => this._gotoUser(user.userid) }>
            <div className='remove-user' onClick={(e) => this.removeUser(e, user.userid) }>
              x
            </div>

            <div>
              <span style={{display: 'inline-block', width: '40px'}}>
                <img src='/images/dashboard/user.svg' style={{width: '24px', height: '18px'}} />
              </span>
              {user.first_name} {user.last_name}
            </div>
            <div>
              <span style={{display: 'inline-block', width: '40px'}}>
                <img src='/images/dashboard/send.svg' style={{width: '24px', height: '18px'}} />
              </span>
              {user.history.length}
            </div>
            <div>
              <span style={{display: 'inline-block', width: '40px'}}>
                <img src='/images/dashboard/pin.svg' style={{width: '12px', height: '24px'}} />
              </span>
              {user.history.length > 0 ? user.history[user.history.length-1].location.value : 'no history'}
            </div>
          </Table.Cell>
        </Table.Row>
      );
    }

    let showMapUser = <div></div>;

    if(this.state.showMapUser !== ''){
      showMapUser = <div className='map-user' onClick={ () => this.setState({showMapUser: ''}) }>
        <img src={this.state.showMapUser} />
      </div>;
    }

    let installCount = this.state.daily_installs.reduce(function(val, day){ return val + parseInt(day.value) }, 0);




    return <div id='care-dashboard-wrapper'>
      <Grid columns={3} centered>
          <Grid.Column width={15} style={{paddingTop: '30px'}}>
            <img style={{maxWidth: '250px', marginBottom: 10}} src='/images/logo-lg.png' />
          </Grid.Column>
      </Grid>

      <Grid padded>
        <Grid.Row columns={4}>

          <Grid.Column>
            <SmallStat icon='/images/dashboard/phone.svg'   title='First Launches'  value={installCount} footer={'Last ' + this.state.daily_installs.length + ' days'} />
          </Grid.Column>

          <Grid.Column>
            <SmallStat icon='/images/dashboard/send.svg'    title='Cards Sent'     value={this.state.sends} footer='Lifetime' />
          </Grid.Column>

          <Grid.Column>
            <SmallStat icon='/images/dashboard/opens.svg'   title='Cards Opened'   value={this.state.opens} footer='Lifetime' />
          </Grid.Column>

          <Grid.Column>
            <SmallStat icon='/images/dashboard/user.svg'    title='Monthly Active' value={this.state.active_users} />
          </Grid.Column>

        </Grid.Row>
      </Grid>



      <Grid padded>
        <Grid.Row columns={2}>

          <Grid.Column>
            <SmallChart title='Daily Installs' value={this.state.daily_installs} />
          </Grid.Column>
          <Grid.Column>
            <SmallChart title='Daily Sends' value={this.state.daily_sends} />
          </Grid.Column>
        </Grid.Row>
      </Grid>

      <Grid padded>
        <Grid.Row columns={2}>
          <Grid.Column>
            <div>

              <UserSearch onResultSelect={ userid => browserHistory.push('/care/user/'+userid) } />
              <Table className='no-padding' celled compact selectable>
                <Table.Body>
                  { users_row }
                </Table.Body>
              </Table>
            </div>
          </Grid.Column>

          <Grid.Column>
            <UserMap coords={this.state.coords} />
          </Grid.Column>

        </Grid.Row>
      </Grid>

    </div>
  }
})

