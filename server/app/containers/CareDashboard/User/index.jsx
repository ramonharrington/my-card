import React from 'react';
import styles from './User.scss'
import axios from 'axios'
import io from 'socket.io-client';
import { Link, browserHistory } from 'react-router';
import ReactJson from 'react-json-view';

import DashboardUserBox from '../../../components/Dashboard/UserBox/index.jsx'
import HistoryBox from '../../../components/Dashboard/History/index.jsx'

import { Accordion, Grid, Header, Table, Button, Icon, Radio, Dimmer, Segment, Modal, Loader } from 'semantic-ui-react'

var utils = require('../../../utils/index.jsx');

module.exports = React.createClass({

  componentDidMount: function(){

    this.getInitialData();

    var socket = io.connect(window.location.protocol + '//'+window.location.host);
    socket.on('dashUpdates', function(data){
      this.getInitialData();
    }.bind(this));
  },

  getInitialState: function(){
    return {
      currUser: {
        default_card: {},
        business_cards: [],
        tracking_data: [],
        history: [],
        unordered_business_cards: [],
        badges: {
          collected: [],
          available: []
        },
        referrals: [],
      },
      loginToken: '',
      vCards: [],
      users: [],
      message: '',
      messageStatus: '',
      cashMessage: 'Add $5 Cash',
      refreshCardsModalOpen: false,
    };
  },

  getInitialData: function(){
    axios.get('/api/admin/user/' + this.props.params.userid)
      .then(function(resp){
        this.setState({currUser: resp.data});

        axios.get('/api/admin/token/' + encodeURIComponent(this.state.currUser.email))
          .then( resp => this.setState({loginToken: resp.data}) )
          .catch( err => this.setState({loginToken: 'no token generated'}) )

        axios.get('/api/v1/vcard/' + resp.data.userid)
         .then(function(resp2){
           this.setState({vCards: resp2.data});
          }.bind(this));
        }.bind(this));


  },

  _addCash: function(){
    this.setState({cashMessage: ' ... '});

    axios({
      method: 'POST',
      url: '/api/admin/vpcash',
      data: {
        userid: this.props.params.userid,
      },
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(function(resp){
        if(resp.data.status){
          this.setState({cashMessage: 'cash added'});
        }else{
          this.setState({cashMessage: 'failed to add cash'});
        }
      }.bind(this))
      .catch(function(err){
        this.setState({cashMessage: 'error: ' + err});
      }.bind(this));
  },

  _markJoined: function(referral){
    let data = {
      userid: null,
      email: this.state.currUser.email,
    };

    axios({
      method: 'POST',
      url: '/api/v1/referral',
      data: data,
      headers: {
        'Content-Type': 'application/json'
      }
    }).catch(function(resp){
      alert('ERROR: ' + resp);
    });
  },

  _createPush: function(){
    var data = {
      userid: this.state.currUser.userid,
      message: this.state.message,
      send_dt: new Date(),
    };

    axios({
      method: 'POST',
      url: '/api/v1/push',
      data: data,
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(function(resp){
        this.setState({messageStatus: 'sent'});
        setTimeout(function(){
          this.setState({messageStatus: 'sent'});
        }.bind(this), 3000);
        setTimeout(function(){
          this.setState({messageStatus: ''});
        }.bind(this), 3000);
      }.bind(this))
      .catch(function(err){
        this.setState({messageStatus: err});
        alert('error: ' + err);

        setTimeout(function(){
          this.setState({messageStatus: ''});
        }.bind(this), 3000);
      }.bind(this));
  },

  forceFeedback: function(){
    axios({
      method: 'POST',
      url: '/api/admin/forceAppReview',
      data: {
        userid: this.props.params.userid,
        state: !this.state.currUser.forceAppFeedback
      },
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(function(resp){
        if(resp.data.status){
          this.getInitialData();
        }else{
          alert('force failed');
        }
      }.bind(this))
      .catch(function(err){
        alert('force failed');
      }.bind(this));
  },

  editUser: function(edits){
    var user = this.state.currUser;

    if(edits.namespace.length === 0){
      user[edits.name] = edits.new_value;
    }else{
      user[edits.namespace][edits.name] = edits.new_value;
    }

    axios({
      method: 'POST',
      url: '/api/admin/userUpdate',
      data: user,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(function(resp){
      this.setState({currUser: resp.data});
    }.bind(this)).catch(function(err){
      alert('error updating user');
    }.bind(this));
  },

  updateCardCache: function(force){

    var url = '/api/admin/user/' + this.state.currUser.email + '/updateCards';
    if(force){
      url = url + '?force=true';
    }

    this.setState({refreshCardsModalOpen: true});
    axios({
      method: 'POST',
      url: url,
    })
    .then(function(resp){
      setTimeout( () => {
        this.getInitialData();

        setTimeout( () => {
          this.setState({refreshCardsModalOpen: false});
          window.location.reload(true);
        }, 2000);
      }, 10000);
    }.bind(this))
    .catch(function (error) {
      alert('Error update cache');
    });;
  },

  render: function(){

    if(this.state.currUser === null){
      return <div id='care-dashboard-user-wrapper'>
        <div className='go-back' onClick={ browserHistory.goBack }>
          <Icon name='arrow left' />
        </div>
        <Grid columns={1} centered>
          <Grid.Column width={12} style={{paddingTop: '30px'}}>
            <img style={{maxWidth: '150px'}} src='/images/logo-lg.png' />
          </Grid.Column>
        </Grid>
      <Grid columns={1} centered>
        <h1 style={{marginTop: 30}}>No user found</h1>
      </Grid>
    </div>
    }



    let s3bucket = /mycard\.vistaprint\.com/.test(window.location.host) ? 'bizcards' : 'bizcards_dev';
    let s3host = 'https://s3.amazonaws.com/vpmycard/' + s3bucket + '/';

    let bizcards = this.state.currUser.business_cards.map(function(bizcard){
      if((!this.state.currUser.default_card) || this.state.currUser.default_card && bizcard.doc_id !== this.state.currUser.default_card.doc_id){

        let combined_images = Object.keys(bizcard.combined_images).map( ci => {
          return <div className='combined_images'>
            <img src={bizcard.combined_images[ci]} />
            </div>
        });

        let vcard = this.state.vCards.filter( (b) => b.alt_doc_id === bizcard.alt_doc_id );
        vcard = vcard.length < 0 ? null : vcard[0];
        let vcard_container = <div></div>
        if(vcard){
          vcard_container = <div className='vcard_container'>
            <h4>Card Info</h4>
            <div style={{textAlign: 'left'}}>
              <div>Primary Side: {vcard.is_front_primary ? 'front' : 'back'}</div>
              <div>Name: {vcard.name}</div>
              <div>Email: {vcard.email}</div>
              <div>Business: {vcard.business}</div>
              <div>Website: {vcard.website}</div>
            </div>
          </div>
        }


        return(
          <div className='bizcard'>
            <img src={s3host + bizcard.alt_doc_id + '/combined.png'} style={{maxWidth: '75%', maxHeight: '300px', border: '1px solid black'}} />
            <div>
               <a href={'http://www.us.vpcustomercare.com/vp/manager/customercare_ns/results.aspx?institution_id=1&criteria=alt_doc_id&scriteria=' + bizcard.alt_doc_id} target='_blank'>{bizcard.alt_doc_id}</a>
            </div>

            {combined_images}
            {vcard_container}
          </div>
        );
      }
    }.bind(this));

    let unordered_cards = this.state.currUser.unordered_business_cards.map(function(bizcard){
      if((!this.state.currUser.default_card) || this.state.currUser.default_card && bizcard.doc_id !== this.state.currUser.default_card.doc_id){
        return(
          <div className='unordered-card'>
            <div className='card-veil'></div>
            <img src={s3host + bizcard.alt_doc_id + '/combined.png'} style={{maxWidth: '75%', maxHeight: '300px', border: '1px solid black'}} />
            <div>
               <a href={'http://www.us.vpcustomercare.com/vp/manager/customercare_ns/results.aspx?institution_id=1&criteria=alt_doc_id&scriteria=' + bizcard.alt_doc_id} target='_blank'>{bizcard.alt_doc_id}</a>
            </div>
          </div>
        );
      }
    }.bind(this));

    let referrals = [];
    if(this.state.currUser.referrals){
      referrals = this.state.currUser.referrals.map(function(td){
        var dt = new Date(td.created);
        var dts = dt.getMonth()+1 + '/' + dt.getDate();
        var state = td.state;

        if(td.state === 'pending'){
          state = <Button primary style={{display: 'block', margin: '10px auto'}} onClick={ () => this._markJoined(td) }>
            mark as joined
            </Button>
        }

        return <div className={'referral-row ' + td.state}>
          <span style={{float: 'right', marginRight: 10}}>
          { state }
        </span>
          <div className='title'>{td.alias || 'unknown'}</div>
          <div className='referral-row-details'>
          <span>{dts}</span>
          <span style={{marginLeft: 30}}>{td.phone}</span>
          </div>
          </div>
      }.bind(this));
    }


    let vp_contacts = [];
    if(this.state.currUser.vpContacts){
      vp_contacts = this.state.currUser.vpContacts.map(function(contact){
        return <div>{contact.phone} - <a target='_blank' href={'http://www.us.vpcustomercare.com/vp/manager/customercare_ns/results.aspx?institution_id=1&criteria=email&scriteria='+contact.email}>{contact.email}</a></div>
      });
    }

    let cash_awards = <div>No record found</div>;
    if(this.state.currUser.vpCash){
      cash_awards = this.state.currUser.vpCash.map(function(vpcash){
        return(
          <Table.Row>
            <Table.Cell>{utils.dateFormat(vpcash.created, 'yyyy-mm-dd')}</Table.Cell>
            <Table.Cell>{utils.dateFormat(vpcash.requestdata.RewardExpirationDate, 'yyyy-mm-dd')}</Table.Cell>
            <Table.Cell>{vpcash.status}</Table.Cell>
          </Table.Row>
        );
      });
    }

    return <div id='care-dashboard-user-wrapper'>

      <Modal
        open={this.state.refreshCardsModalOpen}
        basic
        size='small'
      >
        <Header icon='refresh' content='Refreshing Card Images' />
        <Modal.Content>
          <p>Please wait while we refresh this users business card images</p>
        </Modal.Content>
      </Modal>

      <div className='go-back' onClick={ () => browserHistory.push('/care') }>
        <Icon name='arrow left' />
      </div>
      <Grid columns={1} centered>
          <Grid.Column width={12} style={{paddingTop: '30px'}}>
            <img style={{maxWidth: '150px'}} src='/images/logo-lg.png' />
          </Grid.Column>
      </Grid>

      <Grid columns={3} centered>
        <Grid.Column width={4} style={{backgroundColor: 'white', borderRadius: '6px'}}>
          <div>
            <img src={this.state.currUser.default_card ? s3host + this.state.currUser.default_card.alt_doc_id + '/combined_wood.png' : ''} style={{maxWidth: '100%', border: '1px solid black'}} />
            <a href={'http://www.us.vpcustomercare.com/vp/manager/customercare_ns/results.aspx?institution_id=1&criteria=alt_doc_id&scriteria=' + (this.state.currUser.default_card ? this.state.currUser.default_card.alt_doc_id : '')} target='_blank'>{this.state.currUser.default_card ? this.state.currUser.default_card.alt_doc_id : 'no card selected'}</a>
          </div>

          <div>
            <h4>Cards</h4>
           { bizcards }
           { unordered_cards }
          </div>

        </Grid.Column>
        <Grid.Column width={8}>
          <DashboardUserBox
            user={this.state.currUser}
            token={this.state.loginToken}
            requestToken={this.requestToken}
            admin={false}
          />
          {this.state.currUser.history.length > 0 ? <iframe src={'/'+this.state.currUser.userid+'/map'} style={{width: '100%', height: '600px'}} /> : <div></div>}

        </Grid.Column>
        <Grid.Column width={4}>

          <h2>Card Cache</h2>
          <div className='white-container'>
            <div style={{width: '100%', textAlign: 'center'}}>
              <Button color='green' onClick={ () => this.updateCardCache(true) }>update users cached images</Button>
            </div>
          </div>

          <HistoryBox user={this.state.currUser} />
        </Grid.Column>
      </Grid>
    </div>
  }
});



