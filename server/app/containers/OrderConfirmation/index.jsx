import React from 'react';
import styles from './OrderConfirmation.sass';

import AppStoreLinks from '../../components/AppStoreLinks/index.jsx';
import LinkSmsInput from '../../components/LinkSmsInput/index.jsx';

export default class OrderConfirmation extends React.Component {
  constructor(props) {
    super(props);

    let bcPreview = null;
    if (props.location.query.altDocId) {
      bcPreview =  'https://www.vistaprint.com/lp.aspx?nochrome=1&width=300&alt_doc_id=' + props.location.query.altDocId;
    }

    let smsEnabled = true;
    if (document.referrer.indexOf('.com/') == -1) {
      smsEnabled = false;
    }

    this.state = {
      bcPreview: bcPreview,
      smsEnabled: smsEnabled
    };
  }

  render() {

    let preview = null;
    if (this.state.bcPreview) {
      preview = <img src={this.state.bcPreview} />;
    }

    return (
      <div className='order-confirmation'>
        <link rel="stylesheet" type="text/css" href="/monolith-iframe.css"></link>
        <div className='mockup'>
          <div className='bc-preview-frame'>{preview}</div>
        </div>
        <div className='floating-links'>
          <AppStoreLinks />
        </div>
        <div className='content'>
          <div className='headline'>Don't want to wait for your business cards to arrive?</div>
          <div className='subheadline'>Use them now by sharing your cards with our new app.</div>
          <h4 className='intro'>Introducing</h4>
          <div><a href="http://mycard.vistaprint.com" target="_blank"><img className="logo" src="/images/mycard-logo-2.png" /></a></div>
          <div className='tagline'>
            <div className='free-callout'>FREE to download and use -<a className='learn-more' href="https://www.vistaprint.com/mycard/about?xnav=MycardOrderConfTile" target="_blank">Learn more</a></div></div>
          <div className={this.state.smsEnabled ? '' : 'hide-sms'}>
            <LinkSmsInput />
          </div>
          <div className='mobile-links'>
            <AppStoreLinks />
          </div>
        </div>
      </div>
    )
  }
}