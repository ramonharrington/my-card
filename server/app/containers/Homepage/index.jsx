import React from 'react';
import styles from './Homepage.sass';
import axios from 'axios';
import classNames from 'classnames';

import VideoModal from '../../components/VideoModal/index.jsx';
import AppStoreLinks from '../../components/AppStoreLinks/index.jsx';
import LinkSmsInput from '../../components/LinkSmsInput/index.jsx';

export default class Homepage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showVideo: false,
      faqExpanded: false
    };
  }

  __toggleFaq() {
    this.setState({faqExpanded: !this.state.faqExpanded});
  }

  render() {
    return(
      
<div className="mycard homepage">
  <link rel="stylesheet" type="text/css" href="/monolith-iframe.css"></link>
  <div className="background"></div>
  <div className="small-background"></div>
  <div className="content">
    <div className="header-content default-margin"><img className="logo" src="/images/mycard-logo.png" />
      <div className="tagline">Your business cards just learned how to text.</div>
      <div className="description">Easily share your Vistaprint business card with new contacts.</div>
      <LinkSmsInput />
      <AppStoreLinks />
      <div className="play-video" onClick={ () => this.setState({showVideo: true})}>
        <img src="/images/pp/play-button.svg"/>
        <div className="play-link"></div>
      </div>
    </div>
    <div className="how-it-works">
      <div className="section-name">How it works</div>
      <div className="steps">
        <div className="step">
          <div className="image-container">
            <img alt="Select" className="select-image" src="/images/pp/hiw1.svg"/>
          </div>
          <div className="name">Select</div>
          <div className="description">Log into your Vistaprint account and choose from any of your previously created cards.</div>
        </div>
        <div className="step">
          <div className="image-container">
            <img alt="Share" className="share-image" src="/images/pp/hiw2.svg"/>
          </div>
          <div className="name">Share</div>
          <div className="description">Share a professional image of your business card by text, email, or social media.</div>
        </div>
        <div className="step">
          <div className="image-container">
            <img alt="Grow" className="grow-image" src="/images/pp/hiw3.svg"/>
          </div>
          <div className="name">Grow</div>
          <div className="description">Connect to a larger audience and give your card more visibility in the digital space.</div>
        </div>
      </div>
    </div>
    <div className="usage default-margin">
      <div className="usage-content">
        <div className="section-heading">Always open for business</div>
        <div className="description">Mycard goes where you go, so you’re always ready to represent and grow your business.</div>
        <ul>
          <li>No need to wait for your cards to arrive in the mail to start handing them out</li>
          <li>Keep looking professional - you card won't be damaged or lost because customers can easily find it in their phone</li>
          <li>Spread the word - your contacts can help you grow your business through word-of-mouth by forwarding your business card to others</li>
        </ul>
        <div className="cta">
          <div className="cta-heading">Never be without a business card again</div>
          <div className="cta-subheading">Receive a link to try mycard for FREE</div>
          <LinkSmsInput />
        </div>
      </div>
    </div>
    <div className={classNames({faq: true, expanded: this.state.faqExpanded})}>
      <div className="faq-content default-margin">
        <div className="section-heading" onClick={this.__toggleFaq.bind(this)}>Have a question? Read the FAQs<i className="fa-li fa fa-square"></i></div>
        <div className="questions-and-answers">
          <div className="section">
            <div className="section-title">Installation and setup</div>
            <div className="q-and-a-block">
              <div className="q-and-a">
                <div className="question">What is mycard?</div>
                <div className="answer">mycard is a new mobile business card companion app from Vistaprint. It offers a new way to send virtual business cards from your phone.  Mycard is available from <a href='https://play.google.com/store/apps/details?id=com.vistaprint.mycard'>Google Play store</a> and <a href='https://itunes.apple.com/us/app/mycard-your-business-cards-mobile-companion/id1244185969?ls=1&mt=8' target='_blank'>Apple App Store</a>.</div>
              </div>
              <div className="q-and-a">
                <div className="question">Does the recipient also have to have mycard installed?</div>
                <div className="answer">No. The recipient does not need to have any additional apps installed to receive your mobile business card. mycard sends the card by text message or email depending on the contact information provided by the sender. </div>
              </div>
              <div className="q-and-a">
                <div className="question">Do I need to have a Vistaprint business card to get started?</div>
                <div className="answer">Yes. You need to have ordered at least one set of Vistaprint business cards in the past. Vistaprint offers a variety of different business cards with thousands of design templates to choose from. It only takes a few minutes to place an order. After the order is placed, the business card will become available for use within mycard.  The three most recently ordered business cards are shown in the app.</div>
              </div>
              <div className="q-and-a">
                <div className="question">What is mycard?</div>
                <div className="answer">mycard is a new mobile business card companion app from Vistaprint. It offers a new way to send virtual business cards from your phone.</div>
              </div>
              <div className="q-and-a">
                <div className="question">I want to use mycard, but I do not have a login to Vistaprint.com. How do I get started?</div>
                <div className="answer">You can create a free acount with Vistaprint.com <a href='http://www.vistaprint.com/customer-care/customer-care-center-subject.aspx?SubjectId=64'>here</a>.</div>
              </div>
              <div className="q-and-a">
                <div className="question">Can I create new cards through mycard?</div>
                <div className="answer">mycard is about sharing your business card. You'll need to create and order your business card through Vistaprint.com before it will be available for use in mycard.</div>
              </div>
              <div className="q-and-a">
                <div className="question">I recently ordered physical business cards from Vistaprint.com, but haven’t received them yet? Can I still use mycard?</div>
                <div className="answer">Yes. The ordered business cards are immediately available for selection within mycard.</div>
              </div>
            </div>
          </div>
          <div className="section">
            <div className="section-title">Using mycard</div>
            <div className="q-and-a-block">
              <div className="q-and-a">
                <div className="question">How do I import my business card into mycard?</div>
                <div className="answer">Use your Vistaprint.com credentials when prompted to enter a username and a password. If you are logging in for the first time, you will be presented with a short tutorial and then given the option to import one of the business cards you recently ordered. The top three most recently ordered business cards are shown. It is possible to switch between multiple cards within the app at any time.</div>
              </div>
              <div className="q-and-a">
                <div className="question">How many business cards can I send?</div>
                <div className="answer">You can send an unlimited number of virtual business cards using mycard. The app is absolutely free for existing Vistaprint customers. Standard message and data rates may apply by your wireless cell phone service provider.</div>
              </div>
              <div className="q-and-a">
                <div className="question">Can I use multiple business cards within mycard?</div>
                <div className="answer">It is possible to switch between multiple cards within the app at any time. </div>
              </div>
              <div className="q-and-a">
                <div className="question">Does the other person need to have mycard installed to receive a virtual business card?</div>
                <div className="answer">No, the recipients of the business cards sent from mycard do not need to have the app installed. The virtual business cards are sent as an image via text or email.</div>
              </div>
              <div className="q-and-a">
                <div className="question">What should I write as my message when I send my business card?</div>
                <div className="answer">It really depends on your business and the use case. Here are some ideas for what to write - “I look forward to learning more about your project”, “Please call our office to setup an appointment”, “I will follow up in a few days with the initial estimate for my work”, “It was nice to meet you. Here is my business card for your future reference.”</div>
              </div>
              <div className="q-and-a">
                <div className="question">What does my business card look like when someone receives it?</div>
                <div className="answer">The person getting your business card receives an image and a unique URL link. When they click on the URL, they see a simple page with your business card image, an option to call and to forward your card to someone else.</div>
              </div>
              <div className="q-and-a">
                <div className="question">Is it possible for the recipient of my business card to forward it to someone else?</div>
                <div className="answer">Yes. Each virtual business card generated by mycard comes with a unique URL link that has additional info about your business and the option to forward the virtual card to someone else.</div>
              </div>
            </div>
          </div>
          <div className="section">
            <div className="section-title">Customer Support</div>
            <div className="q-and-a-block">
              <div className="q-and-a">
                <div className="question">How do I get notified of the new updates? </div>
                <div className="answer">Please join <a href='https://www.facebook.com/mycard'>mycard Facebook page</a> for new updates, application tips & tricks, marketing best practices and business card etiquette.</div>
              </div>
              <div className="q-and-a">
                <div className="question">How do I reach support with questions?</div>
                <div className="answer">mycard is in beta and we are actively working to make the app better. If you have any additional questions or feedback please send an email to <a href='mailto:mycard@vistaprint.com'>mycard@vistaprint.com</a>.</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
    );
  }
}
