import React from 'react';
import {Link} from 'react-router';
import styles from './UserMap.scss';
import axios from 'axios';

import { withGoogleMap, GoogleMap, Marker } from "react-google-maps";
import {default as MarkerClusterer} from 'react-google-maps/lib/addons/MarkerClusterer'


export default class UserMap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
    };

    axios.get('/api/v1/user/'+this.props.params.userid).then(function(resp){
      this.setState({user: resp.data});
    }.bind(this));
  }

  render() {
    if(typeof this.state.user.history === 'undefined'){
      return(<div>--- no location data found ---</div>);
    }

    let totalPnts = 0;
    let sumLat = 0;
    let sumLng = 0;

    var coords = this.state.user.history.map(function(hist){
      if(hist.value !== 'unknown' && hist.location.rawLocation){
        sumLat += hist.location.rawLocation.coords.latitude;
        sumLng += hist.location.rawLocation.coords.longitude;
        totalPnts++;

        return <Marker
          key={hist.ts}
          position={{lat: hist.location.rawLocation.coords.latitude, lng: hist.location.rawLocation.coords.longitude}}
          icon='/images/pin.png?'
        />
      }
    });

    let avgLat = (sumLat / totalPnts);
    let avgLng = (sumLng / totalPnts);

    const GettingStartedGoogleMap = withGoogleMap(props => (
      <GoogleMap
        ref={props.onMapLoad}
        defaultZoom={11}
        defaultCenter={{ lat: avgLat, lng: avgLng }}
        onClick={props.onMapClick}
      >
      <MarkerClusterer clusterClass='cluster-group' imagePath='/images/pin.png?' >
        { coords }
      </MarkerClusterer>
      </GoogleMap>
    ));


    return (
      <div>
        <GettingStartedGoogleMap
          containerElement={
            <div style={{height: '300px', minHeight: '100vh', minWidth: '400px', width: '100%'}} />
          }
          mapElement={
            <div style={{ height: '100%', maxHeight: '900px', borderRadius: '5px' }} />
          }
        />
      </div>
    );
  }
}
