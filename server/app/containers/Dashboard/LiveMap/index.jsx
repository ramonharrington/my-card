import React from 'react';
import styles from './LiveMap.scss'
import axios from 'axios'
import io from 'socket.io-client';

import ReactMapboxGl, { Layer, Feature, Popup, Marker } from "react-mapbox-gl";

var utils = require('../../../utils/index.jsx');

const Map = ReactMapboxGl({
    accessToken: "pk.eyJ1IjoiamdhdWRldHRldnAiLCJhIjoiY2o3ajZmYW9oMGF1dDJ3dDZxcjFyM3AyMyJ9.eyUyyeEiwkr3PmwcMySCwg"
});


module.exports = React.createClass({
  displayName: "Users Map",

  componentDidMount: function(){

    var socket = io.connect(window.location.protocol + '//'+window.location.host, {
      transports: ['websocket']
    });

    this.updateData();
    setInterval( () => {
      this.updateData();
    }, 1000 * 60 * 10);

    socket.on('connect', () => {
      this.setState({socketStatus: socket.connected ? 'connected' : 'connection lost'});
    });

    socket.on('activity-live-updates', (datastr) => {
      console.log("activity: " + datastr);

      let activity = this.state.activity;

      activity.push(
        <div dangerouslySetInnerHTML={{__html: datastr.replace(/</g, '<a href="').replace(/\|/g, '">').replace(/>/g, '</a>')}} />
      );

      this.setState({activity});
    });

    socket.on('beacon-live-updates', function(datastr){
      let data = JSON.parse(datastr);
      let beacons = this.state.beacons;
      let new_beacons = [];
      let found = false;

      for(let i=0; i<beacons.length; i++){
        let beacon = beacons[i];
        let oneHourAgo = new Date(Date.now() - (1000 * 60 * 60))

        // if created < 10 minutes ago remove it
        if(oneHourAgo > beacon.created){
          found=true;
        }else if(beacon.userid === data.userid){
          new_beacons.push(data);
          found = true;
        }else{
          new_beacons.push(beacon);
        }
      }

      if(!found){
        new_beacons.push(data);
      }

      this.setState({beacons: new_beacons});
    }.bind(this));
  },

  updateData: function(){
    axios.get('/api/admin/beacons')
      .then(function(resp){
        this.setState({beacons: resp.data});
      }.bind(this));
  },

  getInitialState: function(){
    return {
      beacons: [],
      center: [0, 20],
      zoom: [2],
      socketStatus: 'pending',
      activity: [],
    };
  },

  render: function(){
    var features = this.state.beacons.map(function(beacon){
      let coord = beacon.currentLocation.rawLocation.coords;

      var pin = '/images/pin.png';
      if(beacon.platform === 'android'){
        pin = '/images/pin-android.png';
      }else if(beacon.platform === 'ios'){
        pin = '/images/pin-ios.png';
      }
      
      return <Marker key={beacon.userid} coordinates={[coord.longitude, coord.latitude]}><img src={pin} /></Marker>
    }.bind(this));

    return(<div className='livemap-wrapper'>
      <Map
        style="mapbox://styles/mapbox/dark-v9"
        zoom={this.state.zoom}
        renderWorldCopies={false}
        center={this.state.center}
        onZoom={ (map, evt) => { this.setState({zoom: [map.getZoom()]}) } }
        onMove={ (map, evt) => { this.setState({center: [map.getCenter().lng, map.getCenter().lat]}) } }
        containerStyle={{
          height: "100vh",
          width: "100vw"
        }}>

        {features}
      </Map>

      <div className={'connection-status' + (!this.state.socketStatus ? ' error' : '')}> {this.state.socketStatus}</div>
      <div className='live-feed'>
        {this.state.activity}
      </div>
    </div>
    );
  }
})

