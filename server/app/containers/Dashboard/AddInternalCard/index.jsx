import React from 'react';
import styles from './AddInternalCard.scss'
import axios from 'axios'
import { Table } from 'semantic-ui-react'

module.exports = React.createClass({

  getInitialState: function(){
    return {
      altDocId: "",
      docId: "",
      email: "",
      firstName: "",
      lastName: "",
      title: "",
      phoneNumber: "",
      templateId: "3409705869",
      errorMsg: ""
    };
  },

  getInitialData: function(){
    axios.get('/api/admin/users')
      .then(function(resp){
        this.setState({users: resp.data});

        for(let i=0; i<resp.data.length; i++){
          if(resp.data[i].userid === this.props.params.userid){
            this.setState({currUser: resp.data[i]});
          }
        }
      }.bind(this));
  },

  addEmployeeCard: function() {
    var data = {
      "Email": this.state.email,
      "DisplayEmail": this.state.displayEmail,
      "FirstName": this.state.firstName,
      "LastName": this.state.lastName,
      "Title": this.state.title,
      "PhoneNumber": this.state.phoneNumber,
      "DocumentTemplateId": this.state.templateId
    };

    axios({
      method: 'POST',
      url: '/api/admin/createemployeecard',
      data: data,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(function(resp){
      this.setState({errorMsg: "", altDocId: resp.data.altDocId, docId: resp.data.docId});
    }.bind(this))
    .catch(function(error) {
      this.setState({errorMsg: "Unable to add card: " + error, altDocId: "", docId: ""});
    }.bind(this));
  },

  render: function(){
    let cardCreationMessage = <div></div>;
    let altDocId = <div></div>;
    let docId = <div></div>;

    if (this.state.altDocId != "") {
      cardCreationMessage = <div className="result-message">Card created for {this.state.email}:<br /></div>;
      altDocId = <div>AltDocId: {this.state.altDocId}<br /></div>;
    }
    if (this.state.docId != "") {
      docId = <div>DocId: {this.state.docId}<br /></div>;
    }

    let styleVpCard = "card";
    let styleCimpressCard = "card";
    let errorMsg = <div></div>;
    
    if (this.state.errorMsg != "") {
      errorMsg = <div>{this.state.errorMsg}</div>;
    }

    if (this.state.templateId == "3409705869") {
      styleVpCard = "highlighted-card";
      styleCimpressCard = "card";
    } else if (this.state.templateId == "3414816814") {
      styleVpCard = "card";
      styleCimpressCard = "highlighted-card";
    }
    return (
      <div>
        <Table celled compact selectable>
          <Table.Body>
            <Table.Row className='user-row'>
              <Table.Cell>
                Vistaprint.com Account E-mail Address
              </Table.Cell>
              <Table.Cell>
                <input type='text' defaultValue={this.state.email} onChange={(evt) => this.setState({email: evt.target.value}) } />
              </Table.Cell>
            </Table.Row>
            <Table.Row className='user-row'>
              <Table.Cell>
                E-mail to Display on Card
              </Table.Cell>
              <Table.Cell>
                <input type='text' defaultValue={this.state.displayEmail} onChange={(evt) => this.setState({displayEmail: evt.target.value}) } />
              </Table.Cell>
            </Table.Row>
            <Table.Row className='user-row'>
              <Table.Cell>
                First Name
              </Table.Cell>
              <Table.Cell>
                <input type='text' defaultValue={this.state.firstName} onChange={(evt) => this.setState({firstName: evt.target.value}) } />
              </Table.Cell>
            </Table.Row>
            <Table.Row className='user-row'>
              <Table.Cell>
                Last Name
              </Table.Cell>
              <Table.Cell>
                <input type='text' defaultValue={this.state.lastName} onChange={(evt) => this.setState({lastName: evt.target.value}) } />
              </Table.Cell>
            </Table.Row>
            <Table.Row className='user-row'>
              <Table.Cell>
                Title
              </Table.Cell>
              <Table.Cell>
                <input type='text' defaultValue={this.state.title} onChange={(evt) => this.setState({title: evt.target.value}) } />
              </Table.Cell>
            </Table.Row>
            <Table.Row className='user-row'>
              <Table.Cell>
                Phone Number
              </Table.Cell>
              <Table.Cell>
                <input type='text' defaultValue={this.state.phoneNumber} onChange={(evt) => this.setState({phoneNumber: evt.target.value}) } />
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>
                Template
              </Table.Cell>
              <Table.Cell>
                <img src="/images/3409705869.jpg" className={styleVpCard} onClick={(evt) => this.setState({templateId: "3409705869"}) } />
                <img src="/images/3414816814.jpg" className={styleCimpressCard} onClick={(evt) => this.setState({templateId: "3414816814"}) } />
              </Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
        <input type="button" onClick={ this.addEmployeeCard } value="Create Card" />
        {errorMsg}
        {altDocId}
        {docId}
      </div>
    );
  }
});
