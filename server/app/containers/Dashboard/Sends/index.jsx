import React from 'react';
import styles from './Sends.scss'
import axios from 'axios'
import io from 'socket.io-client';
import { Link } from 'react-router';

import { Accordion, Grid, Header, Table, Button, Icon } from 'semantic-ui-react'
import DashboardNav from '../../../components/DashboardNav/index.jsx';

module.exports = React.createClass({

  componentDidMount: function(){
    axios.get('/api/admin/users')
      .then(function(resp){
        this.setState({users: resp.data});
      }.bind(this));

    var socket = io.connect(window.location.protocol + '//'+window.location.host);
    socket.on('dashUpdates', function(data){
      axios.get('/api/admin/users')
        .then(function(resp){
          this.setState({users: resp.data});
        }.bind(this));
    }.bind(this));
  },

  getInitialState: function(){
    return {
      users: [],
    };
  },

  parseDate: function(dt){
    let newDate = '' + dt.getFullYear();

    let month = dt.getMonth();
    if(month < 10){
      newDate = newDate + '-0' + month;
    }else{
      newDate = newDate + '-' + month;
    }

    let day = dt.getDate();
    if(day < 10){
      newDate = newDate + '-0' + day;
    }else{
      newDate = newDate + '-' + day;
    }

    let hour = dt.getHours();
    if(hour < 10){
      newDate = newDate + ' 0' + hour;
    }else{
      newDate = newDate + ' ' + hour;
    }

    let minutes = dt.getMinutes();
    if(minutes < 10){
      newDate = newDate + ':0' + minutes;
    }else{
      newDate = newDate + ':' + minutes;
    }

    return newDate;
  },

  render: function(){

    let histories = this.state.users.map(function(user){
      return user.history.map(function(history){
        return Object.assign(user, history);
      });
    });


    // Sort and format the sends
    histories.sort(function(a,b){ return (a.ts > b.ts) ? 1 : -1; });
    let rows = [];

    for(let i=0; i<histories.length; i++){
      let history = histories[i][0];

      if(typeof history === 'undefined'){
        continue;
      }

      rows.push(<Table.Row>
        <Table.Cell>{history.userid}</Table.Cell>
        <Table.Cell>{history.first_name} {history.last_name}</Table.Cell>
        <Table.Cell>{ this.parseDate(new Date(history.ts)) }</Table.Cell>
        <Table.Cell>{history.recipient}</Table.Cell>
        <Table.Cell>{history.message}</Table.Cell>
        <Table.Cell>{history.location.value}</Table.Cell>
      </Table.Row>)
    }

    return <div id='dashboard-wrapper'>
      <DashboardNav active='Sends' />

      <Grid columns={3} centered>
          <Grid.Column width={3} style={{paddingTop: '30px'}}>
            <img style={{maxWidth: '150px'}} src='/images/logo-lg.png' />
          </Grid.Column>
          <Grid.Column width={10} style={{paddingTop: '40px'}}>
            <Header as='h1' content='All Card Sends' />
          </Grid.Column>
          <Grid.Column floated={'right'} width={3} style={{paddingTop: '30px'}}>
            <Button negative onClick={this.__logout}>Logout</Button>
          </Grid.Column>
      </Grid>

      
      <Grid columns={1} centered>
        <Grid.Column width={12}>
          <Table>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>UserID</Table.HeaderCell>
                <Table.HeaderCell>User</Table.HeaderCell>
                <Table.HeaderCell>Send Date</Table.HeaderCell>
                <Table.HeaderCell>Recipient</Table.HeaderCell>
                <Table.HeaderCell>Message</Table.HeaderCell>
                <Table.HeaderCell>Location</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              { rows }
            </Table.Body>
          </Table>
        </Grid.Column>
      </Grid>
    </div>
  }
});



