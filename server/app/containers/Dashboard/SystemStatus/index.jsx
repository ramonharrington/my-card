import React from 'react';
import styles from './SystemStatus.scss'
import axios from 'axios'
import io from 'socket.io-client';
import { Link } from 'react-router';
import DashboardNav from '../../../components/DashboardNav/index.jsx';

import { Search, Grid, Header, Table, Button, Icon } from 'semantic-ui-react'

import SmallStat from '../../../components/SmallStat/index.jsx';

import { browserHistory } from 'react-router';

var utils = require('../../../utils/index.jsx');

module.exports = React.createClass({
  displayName: "System Status",

  getInitialState: function(){
    return {
      migration: {},
      total_users: 0,
      total_sends: 0,
      amMigrating: false,

      webservers: {
        webserver1: {
          status: 'a..',
          address: 'https://webserver1.mycard.vistaprint.io:3001'
        },
        webserver2: {
          status: 'b..',
          address: 'https://webserver2.mycard.vistaprint.io:3001'
        },
        webserver3: {
          status: 'c..',
          address: 'https://webserver3.mycard.vistaprint.io:3001'
        },
        webserver4: {
          status: 'd..',
          address: 'https://webserver4.mycard.vistaprint.io:3001'
        }
      },
    };
  },

  startMigration: function(){
    axios.post('/api/admin/migrateNow');
    this.setState({amMigration: true});
  },

  componentDidMount: function(){
    this.updateData();

    var socket = io.connect(window.location.protocol + '//'+window.location.host);
    socket.on('dashUpdates', function(data){
      this.updateData();
    }.bind(this));

    setTimeout(function(){
      this.updateData();
    }.bind(this), 1000 * 60 * 60); // update every minute
  },

  updateData: async function(){
    axios.get('/api/admin/stats/migration')
      .then( resp => {
        this.setState({migration: resp.data});
      });

    axios.get('/api/admin/stats/users')
      .then( resp => {
        this.setState({total_users: resp.data});
      });

    axios.get('/api/admin/stats/sends')
      .then( resp => {
        this.setState({total_sends: resp.data});
      });

    // Get status of webservers http server
    for(var serverid in this.state.webservers){
      let server = this.state.webservers[serverid];

      await axios.get(server.address + '/api/v1/status')
        .then( (resp) => {
          let servers = this.state.webservers;
          servers[serverid].status = resp.status;
          this.setState({webservers: servers});
        })
        .catch(function(err){
          let servers = this.state.webservers;
          servers[serverid].status = err.message;
          this.setState({webservers: servers});
        }.bind(this));
    }
  },

  render: function(){

    let serverStatus = Object.keys(this.state.webservers).map(serverid => {
      let server = this.state.webservers[serverid];

      return <Grid.Column key={serverid}>
        <SmallStat icon='/images/dashboard/phone.svg' title={serverid} value={server.status} />
      </Grid.Column>
    });

    return <div id='dashboard-wrapper'>
      <DashboardNav active='System Status' />

      <Grid columns={3} centered>
          <Grid.Column width={15} style={{paddingTop: '30px'}}>
            <img style={{maxWidth: '250px', marginBottom: 10}} src='/images/logo-lg.png' />
          </Grid.Column>
      </Grid>

      <Grid columns={3} centered>
          <Grid.Column width={15} style={{paddingTop: '30px'}}>
            <h1>System Status</h1>
          </Grid.Column>
      </Grid>

      <Grid padded>
        <Grid.Row columns={4}>
          { serverStatus }
        </Grid.Row>
      </Grid>




      <Grid columns={3} centered>
        <Grid.Column width={3} style={{paddingTop: '30px'}}></Grid.Column>

        <Grid.Column width={9} style={{paddingTop: '30px'}}>
          <h1>
            Migration Status
            <span style={{float: 'right'}}>
            </span>
          </h1>
        </Grid.Column>

        <Grid.Column width={3} style={{paddingTop: '30px'}}>
          <Button loading={this.state.amMigrating} size='tiny' color='green' onClick={this.startMigration}>start migrating</Button>
        </Grid.Column>
      </Grid>

      <Grid padded>
        <Grid.Row columns={4}>
          <Grid.Column>
            <SmallStat icon='/images/dashboard/user.svg' title='Users' value={this.state.migration.total_users} footer={this.state.total_users} />
          </Grid.Column>
          <Grid.Column>
            <SmallStat icon='/images/dashboard/user.svg' title='Sends' value={this.state.migration.total_sends} footer={this.state.total_sends}  />
          </Grid.Column>
          <Grid.Column>
            <SmallStat icon='/images/dashboard/user.svg' title='Pushes' value={this.state.migration.total_pushes} />
          </Grid.Column>
          <Grid.Column>
            <SmallStat icon='/images/dashboard/user.svg' title='Events' value={this.state.migration.total_events} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  }
})

