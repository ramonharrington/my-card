import React from 'react';
import styles from './User.scss'
import axios from 'axios'
import io from 'socket.io-client';
import { Link, browserHistory } from 'react-router';
import ReactJson from 'react-json-view';
import DashboardNav from '../../../components/DashboardNav/index.jsx';

import { Accordion, Grid, Header, Table, Button, Icon, Radio, Dimmer, Segment, Modal } from 'semantic-ui-react'

import DashboardUserBox from '../../../components/Dashboard/UserBox/index.jsx'
import HistoryBox from '../../../components/Dashboard/History/index.jsx'

var utils = require('../../../utils/index.jsx');

module.exports = React.createClass({

  componentDidMount: function(){

    this.getInitialData();

    var socket = io.connect(window.location.protocol + '//'+window.location.host);
    socket.on('dashUpdates', function(data){
      this.getInitialData();
    }.bind(this));
  },

  getInitialState: function(){
    return {
      currUser: {
        default_card: {},
        business_cards: [],
        tracking_data: [],
        history: [],
        unordered_business_cards: [],
        badges: {
          collected: [],
          available: []
        },
      },
      loginToken: '',
      users: [],
      message: '',
      messageStatus: '',
      cashMessage: 'Add $5 Cash',
      refreshCardsModalOpen: false,
      errors: [],
      vcards: [],
    };
  },

  getInitialData: function(){
    axios.get('/api/admin/user/' + this.props.params.userid)
      .then(function(resp){
        this.setState({currUser: resp.data});

        axios.get('/api/admin/token/' + encodeURIComponent(this.state.currUser.email))
          .then( resp => this.setState({loginToken: resp.data}) )
          .catch( err => this.setState({loginToken: 'no token generated'}) )

        axios.get('/api/admin/bugsnag?email=' + encodeURIComponent(resp.data.email))
          .then( resp => this.setState({errors: resp.data}) );

        axios.get('/api/v1/vcard/' + resp.data.userid)
          .then( resp => this.setState({vcards: resp.data}) );

      }.bind(this));

  },

  requestToken: function(){
    axios({
      method: 'POST',
      url: '/api/admin/token',
      data: {
        email: this.state.currUser.email,
      },
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then( resp => {
        this.setState({loginToken: resp.data.token});
      })
      .catch( err => {
        alert('error requesting token');
      });
  },

  _addCash: function(){
    this.setState({cashMessage: ' ... '});

    axios({
      method: 'POST',
      url: '/api/admin/vpcash',
      data: {
        userid: this.props.params.userid,
      },
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(function(resp){
        if(resp.data.status){
          this.setState({cashMessage: 'cash added'});
        }else{
          this.setState({cashMessage: 'failed to add cash'});
        }
      }.bind(this))
      .catch(function(err){
        this.setState({cashMessage: 'error: ' + err});
      }.bind(this));
  },

  _markJoined: function(referral){
    let data = {
      userid: null,
      email: this.state.currUser.email,
    };

    axios({
      method: 'POST',
      url: '/api/v1/referral',
      data: data,
      headers: {
        'Content-Type': 'application/json'
      }
    }).catch(function(resp){
      alert('ERROR: ' + resp);
    });
  },

  forceFeedback: function(){
    axios({
      method: 'POST',
      url: '/api/admin/forceAppReview',
      data: {
        userid: this.props.params.userid,
        state: !this.state.currUser.forceAppFeedback
      },
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(function(resp){
        if(resp.data.status){
          this.getInitialData();
        }else{
          alert('force failed');
        }
      }.bind(this))
      .catch(function(err){
        alert('force failed');
      }.bind(this));
  },

  editUser: function(edits){
    var user = this.state.currUser;

    if(edits.namespace.length === 0){
      user[edits.name] = edits.new_value;
    }else{
      user[edits.namespace][edits.name] = edits.new_value;
    }

    axios({
      method: 'POST',
      url: '/api/admin/userUpdate',
      data: user,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(function(resp){
      this.setState({currUser: resp.data});
    }.bind(this)).catch(function(err){
      alert('error updating user');
    }.bind(this));
  },

  updateCardCache: function(force){

    var url = '/api/admin/user/' + this.state.currUser.email + '/updateCards';
    if(force){
      url = url + '?force=true';
    }

    this.setState({refreshCardsModalOpen: true});
    axios({
      method: 'POST',
      url: url,
    })
    .then(function(resp){
      setTimeout( () => {
        this.getInitialData();

        setTimeout( () => {
          this.setState({refreshCardsModalOpen: false});
          window.location.reload(true);
        }, 2000);
      }, 10000);
    }.bind(this))
    .catch(function (error) {
      alert('Error update cache');
    });;
  },

  render: function(){

    if(this.state.currUser === null){
      return <div id='dashboard-user-wrapper'>
        <div className='go-back' onClick={ browserHistory.goBack }>
          <Icon name='arrow left' />
        </div>
        <Grid columns={1} centered>
          <Grid.Column width={12} style={{paddingTop: '30px'}}>
            <img style={{maxWidth: '150px'}} src='/images/logo-lg.png' />
          </Grid.Column>
        </Grid>
      <Grid columns={1} centered>
        <h1 style={{marginTop: 30}}>No user found</h1>
      </Grid>
    </div>
    }





    let s3bucket = /mycard\.vistaprint\.com/.test(window.location.host) ? 'bizcards' : 'bizcards_dev';
    let s3host = 'https://s3.amazonaws.com/vpmycard/' + s3bucket + '/';

    let bizcards = this.state.currUser.business_cards.map(function(bizcard){
      if((!this.state.currUser.default_card) || this.state.currUser.default_card && bizcard.doc_id !== this.state.currUser.default_card.doc_id){

        let combined_images = Object.keys(bizcard.combined_images).map( ci => {
          return <div className='combined_images'>
            <img src={bizcard.combined_images[ci]} />
            </div>
        });

        let vcard = {};
        for(let i=0; i<this.state.vcards.length; i++){
          if(this.state.vcards[i].alt_doc_id === bizcard.alt_doc_id){
            vcard = this.state.vcards[i];
          }
        }

        return(
          <div className='bizcard'>
            <img src={s3host + bizcard.alt_doc_id + '/combined.png'} style={{maxWidth: '75%', maxHeight: '300px', border: '1px solid black'}} />
            <div>
               <a href={'http://www.us.vpcustomercare.com/vp/manager/customercare_ns/results.aspx?institution_id=1&criteria=alt_doc_id&scriteria=' + bizcard.alt_doc_id} target='_blank'>{bizcard.alt_doc_id}</a>
            </div>

            {combined_images}
          </div>
        );
      }
    }.bind(this));

    let unordered_cards = this.state.currUser.unordered_business_cards.map(function(bizcard){
      let vcard = {};
      for(let i=0; i<this.state.vcards.length; i++){
        if(this.state.vcards[i].alt_doc_id === bizcard.alt_doc_id){
          vcard = this.state.vcards[i];
        }
      }
            //<div style={{textAlign: 'left'}}>
              //<div><strong>Name:</strong>          {vcard.name}</div>
              //<div><strong>Email:</strong>         {vcard.email}</div>
              //<div><strong>Business:</strong>      {vcard.business_name}</div>
              //<div><strong>Website:</strong>       {vcard.website}</div>
              //<div><strong>Front primary:</strong> {vcard.is_front_primary ? 'yes' : 'no'}</div>
            //</div>

      if((!this.state.currUser.default_card) || this.state.currUser.default_card && bizcard.doc_id !== this.state.currUser.default_card.doc_id){
        return(
          <div className='unordered-card'>
            <div className='card-veil'></div>
            <img src={s3host + bizcard.alt_doc_id + '/combined.png'} style={{maxWidth: '75%', maxHeight: '300px', border: '1px solid black'}} />
            <div>
               <a href={'http://www.us.vpcustomercare.com/vp/manager/customercare_ns/results.aspx?institution_id=1&criteria=alt_doc_id&scriteria=' + bizcard.alt_doc_id} target='_blank'>{bizcard.alt_doc_id}</a>
            </div>
          </div>
        );
      }
    }.bind(this));

    let errors = this.state.errors.map(function(err){
      let app = err.meta_data.App;
      let device = err.meta_data.Device;
      let exceptions = err.exceptions.map( (exception) => { return exception.message }).join(',');

      return <div className={'error ' + err.severity}>
        <a href={err.html_url} target='_blank'>{err.received_at}</a> - {exceptions}
      </div>
    });

    return <div id='dashboard-user-wrapper'>
      <DashboardNav />
      <Modal
        open={this.state.refreshCardsModalOpen}
        basic
        size='small'
      >
        <Header icon='refresh' content='Refreshing Card Images' />
        <Modal.Content>
          <p>Please wait while we refresh this users business card images</p>
        </Modal.Content>
      </Modal>

      <div className='go-back' onClick={ () => browserHistory.push('/dashboard') }>
        <Icon name='arrow left' />
      </div>
      <Grid columns={1} centered>
          <Grid.Column width={12} style={{paddingTop: '30px'}}>
            <img style={{maxWidth: '150px'}} src='/images/logo-lg.png' />
          </Grid.Column>
      </Grid>

      <Grid columns={3} centered>
        <Grid.Column width={4} style={{backgroundColor: 'white', borderRadius: '6px'}}>
          <div>
            <img src={this.state.currUser.default_card ? s3host + this.state.currUser.default_card.alt_doc_id + '/combined_wood.png' : ''} style={{maxWidth: '100%', border: '1px solid black'}} />
            <a href={'http://www.us.vpcustomercare.com/vp/manager/customercare_ns/results.aspx?institution_id=1&criteria=alt_doc_id&scriteria=' + (this.state.currUser.default_card ? this.state.currUser.default_card.alt_doc_id : '')} target='_blank'>{this.state.currUser.default_card ? this.state.currUser.default_card.alt_doc_id : 'no card selected'}</a>
          </div>

          <div>
            <h4>Cards</h4>
           { bizcards }
           { unordered_cards }
          </div>

        </Grid.Column>
        <Grid.Column width={8}>
          <DashboardUserBox
            user={this.state.currUser}
            token={this.state.loginToken}
            requestToken={this.requestToken}
            admin={true}
          />

          {this.state.currUser.history.length > 0 ? <iframe src={'/'+this.state.currUser.userid+'/map'} style={{width: '100%', height: '600px'}} /> : <div></div>}

          <h2>Raw Data</h2>
          <div style={{maxHeight: 400, overflowY: 'scroll'}} className='white-container'>
            <ReactJson collapsed={1} src={this.state.currUser} onEdit={ (edits) => this.editUser(edits) } />
          </div>
        </Grid.Column>
        <Grid.Column width={4}>

          <h2>Force Feedback Pop:</h2>
          <div className='white-container'>
            <Dimmer.Dimmable>
              <Dimmer active={this.state.currUser.appVersion && parseInt(this.state.currUser.appVersion.replace(/\./g,'')) < 33}>
                <Icon name='lock' /> User upgrade required
              </Dimmer>
              <Radio
                toggle
                onClick={this.forceFeedback}
                checked={this.state.currUser.forceAppFeedback}
                label='Force prompt on next sync'
              />


              <br />
              last forced: {this.state.currUser.lastShownAppFeedback ? this.state.currUser.lastShownAppFeedback : 'never'}
              <br /><br />

              <h4>Card Cache</h4>
              <div style={{width: '100%', textAlign: 'center'}}>
              <Button.Group>
                <Button color='green' onClick={ () => this.updateCardCache(false) }>update</Button>
                <Button.Or />
                <Button color='red' onClick={ () => this.updateCardCache(true) }>force</Button>
              </Button.Group>
              </div>
            </Dimmer.Dimmable>
          </div>

          <h2>Client Errors</h2>
          <div className='white-container'>
            {errors}
          </div>

          <HistoryBox user={this.state.currUser} />


        </Grid.Column>
      </Grid>

    </div>
  }
});



