import React from 'react';
import styles from './Referral.scss'
import axios from 'axios'
import io from 'socket.io-client';
import { Link, browserHistory } from 'react-router';
import DashboardNav from '../../../components/DashboardNav/index.jsx';

import { Accordion, Grid, Header, Table, Button, Icon } from 'semantic-ui-react'

var utils = require('../../../utils/index.jsx');

module.exports = React.createClass({

  componentDidMount: function(){

    this.getInitialData();

    var socket = io.connect(window.location.protocol + '//'+window.location.host);
    socket.on('dashUpdates', function(data){
      this.getInitialData();
    }.bind(this));
  },

  getInitialState: function(){
    return {
      referrals: [],
    };
  },

  getInitialData: function(){
    axios.get('/api/admin/referrals')
      .then(function(resp){
        this.setState({referrals: resp.data});
      }.bind(this));
  },

  render: function(){

    let referrals = this.state.referrals.map(function(referral){
      return(
        <Table.Row className={'refer-row ' + referral.state}>
          <Table.Cell>
            {referral.user ? 
              <Link to={'/dashboard/user/'+referral.user.userid}>{referral.user.first_name} {referral.user.last_name}</Link>
                : 'unknown'}
          </Table.Cell>
          <Table.Cell>{referral.alias}</Table.Cell>
          <Table.Cell>{referral.phone}</Table.Cell>
          <Table.Cell>{referral.state}</Table.Cell>
          <Table.Cell>{utils.formatDate(referral.created, 'mm-dd')}</Table.Cell>
        </Table.Row>
      );
    });


    return <div id='dashboard-user-wrapper'>
      <DashboardNav active='Referrals' />

      <div className='go-back' onClick={ () => browserHistory.push('/dashboard') }>
        <Icon name='arrow left' />
      </div>
      <Grid columns={1} centered>
          <Grid.Column width={12} style={{paddingTop: '30px'}}>
            <img style={{maxWidth: '150px'}} src='/images/logo-lg.png' />
          </Grid.Column>
      </Grid>

      <Grid columns={12} centered><h1>Referrals</h1></Grid>

      <Grid columns={12} centered>
        <Grid.Column width={12} style={{backgroundColor: 'white', borderRadius: '6px'}}>
          <div>

            <Table>
              <Table.Header>
                <Table.HeaderCell>Referred By</Table.HeaderCell>
                <Table.HeaderCell>Referred Alias</Table.HeaderCell>
                <Table.HeaderCell>Referred Phone</Table.HeaderCell>
                <Table.HeaderCell>Status</Table.HeaderCell>
                <Table.HeaderCell>Referred Date</Table.HeaderCell>
              </Table.Header>
              <Table.Body>
                { referrals }
              </Table.Body>
            </Table>
          </div>

        </Grid.Column>
      </Grid>
    </div>
  }
});



