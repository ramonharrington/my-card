import React from 'react';
import styles from './Pushes.scss'
import axios from 'axios'
import io from 'socket.io-client';
import { Link } from 'react-router';
import DashboardNav from '../../../components/DashboardNav/index.jsx';

import { Accordion, Grid, Header, Table, Button, Icon } from 'semantic-ui-react'

module.exports = React.createClass({

  componentDidMount: function(){
    axios.get('/api/v1/push')
      .then(function(resp){
        this.setState({pushes: resp.data});
      }.bind(this));

    axios.get('/api/v1/push/enabledUsers')
      .then(function(resp){
        this.setState({devices: resp.data.named_users || []});
      }.bind(this));

    var socket = io.connect(window.location.protocol + '//'+window.location.host);
    socket.on('dashUpdates', function(data){
      axios.get('/api/v1/push')
        .then(function(resp){
          this.setState({pushes: resp.data});
        }.bind(this));
    }.bind(this));
  },

  getInitialState: function(){
    return {
      pushes: {new: [], complete: [], failed: [], queued: []},
      devices: [],
    };
  },

  _sendAlert: function(_id){
    axios.post('/api/v1/push/' + _id + '/force')
      .then(function(resp){
        axios.get('/api/v1/push')
          .then(function(resp){
            this.setState({pushes: resp.data});
          }.bind(this));
      }.bind(this));
  },

  _resetAlert: function(_id){
    axios.post('/api/v1/push/' + _id + '/reset')
      .then(function(resp){
        axios.get('/api/v1/push')
          .then(function(resp){
            this.setState({pushes: resp.data});
          }.bind(this));
      }.bind(this));
  },

  _removeReminder: function(_id){
    axios.delete('/api/v1/push/' + _id)
      .then(function(resp){
        axios.get('/api/v1/push')
          .then(function(resp){
            this.setState({pushes: resp.data});
          }.bind(this));
      }.bind(this));
  },

  render: function(){
    let pushes_new = this.state.pushes.new.map(function(reminder){
      var devices = this.state.devices.map(function(device){
        if(device.named_user_id === reminder.userid){
          return device.channels.map(function(channel){
            return <div key={channel.channel_id}>
              <Icon name={channel.device_type === 'ios' ? 'apple' : 'android'} />
              {channel.channel_id} - {channel.last_registration}
            </div>
          });
        }
      });

      return <Table.Row key={reminder._id} className='reminder-row'>
        <Table.Cell>{reminder.userid}</Table.Cell>
        <Table.Cell>{reminder.send_dt}</Table.Cell>
        <Table.Cell>{reminder.message}</Table.Cell>
        <Table.Cell>
          { devices }
        </Table.Cell>
        <Table.Cell>
          <Button size='mini' onClick={ () => this._sendAlert(reminder._id) }>send now</Button>
          <Button size='mini' onClick={ () => this._removeReminder(reminder._id) } negative>remove</Button>
        </Table.Cell>
      </Table.Row>
    }.bind(this));

    let pushes_queued = this.state.pushes.queued.map(function(reminder){
      var devices = this.state.devices.map(function(device){
        if(device.named_user_id === reminder.userid){
          return device.channels.map(function(channel){
            return <div key={channel.channel_id}>
              <Icon name={channel.device_type === 'ios' ? 'apple' : 'android'} />
              {channel.channel_id} - {channel.last_registration}
            </div>
          });
        }
      });

      return <Table.Row key={reminder._id} className='reminder-row'>
        <Table.Cell>{reminder.userid}</Table.Cell>
        <Table.Cell>{reminder.send_dt}</Table.Cell>
        <Table.Cell>{reminder.message}</Table.Cell>
        <Table.Cell>{reminder.state}</Table.Cell>
        <Table.Cell>
          { devices }
        </Table.Cell>
        <Table.Cell>
          <Button size='mini' onClick={ () => this._sendAlert(reminder._id) }>send now</Button>
          <Button size='mini' onClick={ () => this._resetAlert(reminder._id) }>reset</Button>
          <Button size='mini' onClick={ () => this._removeReminder(reminder._id) } negative>remove</Button>
        </Table.Cell>
      </Table.Row>
    }.bind(this));

    let pushes_complete = this.state.pushes.complete.map(function(reminder){
      return <Table.Row key={reminder._id} className='reminder-row'>
        <Table.Cell>{reminder.userid}</Table.Cell>
        <Table.Cell>{reminder.send_dt}</Table.Cell>
        <Table.Cell>{reminder.message}</Table.Cell>
        <Table.Cell>{reminder.state}</Table.Cell>
        <Table.Cell>
          <Button size='mini' onClick={ () => this._removeReminder(reminder._id) }>remove</Button>
        </Table.Cell>
      </Table.Row>
    }.bind(this));

    let devices = this.state.devices.map(function(device){
      var channels = device.channels.map(function(channel){
        return <div key={channel.channel_id}>
            <Icon name={channel.device_type === 'ios' ? 'apple' : 'android'} />
              {channel.channel_id} - {channel.last_registration} - {channel.opt_in.toString()}
          </div>
      });

      return <Table.Row key={device.named_user_id} className='device-row'>
        <Table.Cell>{device.named_user_id}</Table.Cell>
        <Table.Cell>
          { channels }
        </Table.Cell>
      </Table.Row>
    }.bind(this));

    return <div id='dashboard-wrapper'>
      <DashboardNav active='Pushes' />

      <Grid columns={3} centered>
          <Grid.Column width={3} style={{paddingTop: '30px'}}>
            <img style={{maxWidth: '150px'}} src='/images/logo-lg.png' />
          </Grid.Column>
          <Grid.Column width={10} style={{paddingTop: '40px'}}>
            <Header as='h1' content='Push Notification Scheduler' />
          </Grid.Column>
          <Grid.Column floated={'right'} width={3} style={{paddingTop: '30px'}}>
            <Button negative onClick={this.__logout}>Logout</Button>
          </Grid.Column>
      </Grid>

      
      <Header as='h2' content='New' />
      <Grid columns={1} centered>
        <Grid.Column width={12}>
          <Table>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>UserID</Table.HeaderCell>
                <Table.HeaderCell>Send Dt</Table.HeaderCell>
                <Table.HeaderCell>Message</Table.HeaderCell>
                <Table.HeaderCell>Devices</Table.HeaderCell>
                <Table.HeaderCell>Actions</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            { pushes_new }
          </Table>
        </Grid.Column>
      </Grid>

      <Header as='h2' content='In Progress' />
      <Grid columns={1} centered>
        <Grid.Column width={12}>
          <Table>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>UserID</Table.HeaderCell>
                <Table.HeaderCell>Send Dt</Table.HeaderCell>
                <Table.HeaderCell>Message</Table.HeaderCell>
                <Table.HeaderCell>State</Table.HeaderCell>
                <Table.HeaderCell>Devices</Table.HeaderCell>
                <Table.HeaderCell>Actions</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            { pushes_queued }
          </Table>
        </Grid.Column>
      </Grid>


      <Header as='h2' content='Completed' />
      <Grid columns={1} centered>
        <Grid.Column width={12}>
          <Table>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>UserID</Table.HeaderCell>
                <Table.HeaderCell>Send Dt</Table.HeaderCell>
                <Table.HeaderCell>Message</Table.HeaderCell>
                <Table.HeaderCell>Status</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            { pushes_complete }
          </Table>
        </Grid.Column>
      </Grid>
    </div>
  }
});



