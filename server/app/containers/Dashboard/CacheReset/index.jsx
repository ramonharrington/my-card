import React from 'react';
import styles from './CacheReset.scss'
import axios from 'axios'
import { Table } from 'semantic-ui-react'
import classNames from 'classnames'

module.exports = React.createClass({

  getInitialState: function(){
    return {
      email: "",
      cards: [],
      isWaiting: false,
      isError: false
    };
  },

  resetAccountCache: function(){
    if (this.state.email == "") {
      return;
    }

    this.setState({isWaiting: true, isError: false});

    axios({
      method: 'POST',
      url: '/api/admin/user/' + this.state.email + '/updateCards'
    })
    .then(function(resp){
      this.setState({cards: resp.data});
      this.setState({isWaiting: false, isError: false});
    }.bind(this))
    .catch(function (error) {
      this.setState({isWaiting: false, isError: true});
    });;
  },

  render: function(){
    let response = <div></div>;
    if (this.state.cards.length != 0) {
      response = <div>Updated account {this.state.email} with {this.state.cards.length} cards.</div>;
    }

    return (
      <div className='reset-cache'>
        <Table celled compact selectable>
          <Table.Body>
            <Table.Row className='user-row'>
              <Table.Cell>
                Vistaprint.com Account E-mail Address
              </Table.Cell>
              <Table.Cell>
                <input type='text' defaultValue={this.state.email} onChange={(evt) => this.setState({email: evt.target.value}) } />
              </Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
        <input type="button" onClick={ this.resetAccountCache } value="Reset Account Cache" />
        {response}
        <div className={classNames({hidden: !this.state.isWaiting})}><i className="fa fa-spinner fa-spin" ariaHidden="true"></i> Attempting Refresh</div>
        <div className={classNames({hidden: !this.state.isError})}>Error refreshing cards</div>
      </div>
    );
  }
});
