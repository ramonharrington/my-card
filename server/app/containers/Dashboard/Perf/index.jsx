import React from 'react';
import styles from './Perf.scss'
import axios from 'axios'
import io from 'socket.io-client';
import { Link, browserHistory } from 'react-router';
import DashboardNav from '../../../components/DashboardNav/index.jsx';

import { Accordion, Grid, Header, Table, Button, Icon } from 'semantic-ui-react'

module.exports = React.createClass({

  componentDidMount: function(){

    this.getInitialData();

    var socket = io.connect(window.location.protocol + '//'+window.location.host);
    socket.on('dashUpdates', function(data){
      this.getInitialData();
    }.bind(this));
  },

  getInitialState: function(){
    return {
      perf: [],
    };
  },

  getInitialData: function(){
    axios.get('/api/admin/perfmons')
      .then(function(resp){
        this.setState({perf: resp.data});
      }.bind(this));
  },

  render: function(){
    var formatDate = function(dts){
      var dt = new Date(dts);

      var formatted = dt.getFullYear() + '-';
      if(dt.getMonth() < 10){
        formatted += '-0' + dt.getMonth();
      }else{
        formatted += '-' + dt.getMonth();
      }

      if(dt.getDate() < 10){
        formatted += '-0' + dt.getDate();
      }else{
        formatted += '-' + dt.getDate();
      }

      if(dt.getHours() < 10){
        formatted += ' 0' + dt.getHours();
      }else{
        formatted += ' ' + dt.getHours();
      }

      if(dt.getMinutes() < 10){
        formatted += ':0' + dt.getMinutes();
      }else{
        formatted += ':' + dt.getMinutes();
      }

      if(dt.getSeconds() < 10){
        formatted += ':0' + dt.getSeconds();
      }else{
        formatted += ':' + dt.getSeconds();
      }

      return formatted;
    };

    let perfrows = this.state.perf.map(function(rec){
      let subevent = rec.events.map(function(event, i){
        let elapsedTime = i === 0 ? 0 : Math.round((new Date(event.created) - new Date(rec.events[i-1].created)) / 1000);
        let timeClass = elapsedTime > 5 ? 'high' : elapsedTime > 3 ? 'medium' : 'low';

        return <Table.Row>
            <Table.Cell>
              {formatDate(event.created)} - <span className={'elapsed-time ' + timeClass}>+ {elapsedTime}</span>
            </Table.Cell>
            <Table.Cell>{event.msg}</Table.Cell>
          </Table.Row>
      });

      let timeClass = rec.total_sec > 5 ? 'high' : rec.total_sec > 3 ? 'medium' : 'low';
      return <Table.Row>
          <Table.Cell>{rec.name}</Table.Cell>
          <Table.Cell>{rec.email}</Table.Cell>
          <Table.Cell><span className={'elapsed-time ' + timeClass}>{rec.total_sec}</span></Table.Cell>
          <Table.Cell>
            <Table compact size='small'>
              <Table.Body>
                {subevent}
              </Table.Body>
            </Table>
          </Table.Cell>
          <Table.Cell>{formatDate(rec.created)}</Table.Cell>
        </Table.Row>
    }.bind(this));

    return <div id='dashboard-user-wrapper'>
      <DashboardNav active='Login Performance' />

      <div className='go-back' onClick={ () => browserHistory.push('/dashboard') }>
        <Icon name='arrow left' />
      </div>
      <Grid columns={1} centered>
          <Grid.Column width={12} style={{paddingTop: '30px'}}>
            <img style={{maxWidth: '150px'}} src='/images/logo-lg.png' />
          </Grid.Column>
      </Grid>

      <Grid columns={12} centered>
        <Grid.Column width={12} style={{backgroundColor: 'white', borderRadius: '6px'}}>
          <Table celled size='small'>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Event Nmae</Table.HeaderCell>
                <Table.HeaderCell>Email</Table.HeaderCell>
                <Table.HeaderCell>Seconds</Table.HeaderCell>
                <Table.HeaderCell>SubEvents</Table.HeaderCell>
                <Table.HeaderCell>Created</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {perfrows}
            </Table.Body>
          </Table>
        </Grid.Column>
      </Grid>

    </div>
  }
});



