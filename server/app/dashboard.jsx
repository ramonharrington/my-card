import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link, browserHistory } from 'react-router'

import UserMap from './containers/UserMap/index.jsx';
import Homepage from './containers/Homepage/index.jsx';
import OrderConfirmation from './containers/OrderConfirmation/index.jsx';

import Dashboard from './containers/Dashboard/index.jsx';
import LiveMap from './containers/Dashboard/LiveMap/index.jsx';
import Pushes from './containers/Dashboard/Pushes/index.jsx';
import Sends from './containers/Dashboard/Sends/index.jsx';
import User from './containers/Dashboard/User/index.jsx';
import Perf from './containers/Dashboard/Perf/index.jsx';
import AddInternalCard from './containers/Dashboard/AddInternalCard/index.jsx';
import CacheReset from './containers/Dashboard/CacheReset/index.jsx';
import Referral from './containers/Dashboard/Referral/index.jsx';

import CareDashboard from './containers/CareDashboard/index.jsx';
import CareUser from './containers/CareDashboard/User/index.jsx';

import SystemStatus from './containers/Dashboard/SystemStatus/index.jsx';

require("babel-core/register");
require("babel-polyfill");

ReactDOM.render((
  <Router history={browserHistory}>
    <Route path="/care"                           component={CareDashboard} />
    <Route path="/care/user/:userid"              component={CareUser} />

    <Route path="/dashboard"                      component={Dashboard} />
    <Route path="/dashboard/pushes"               component={Pushes} />
    <Route path="/dashboard/sends"                component={Sends} />
    <Route path="/dashboard/referrals"            component={Referral} />
    <Route path="/dashboard/status"               component={SystemStatus} />
    <Route path="/dashboard/user/:userid"         component={User} />
    <Route path="/dashboard/perf"                 component={Perf} />
    <Route path="/dashboard/live"                 component={LiveMap} />
    <Route path="/dashboard/support/reset"        component={CacheReset} />
    <Route path="/dashboard/support/add-internal" component={AddInternalCard} />
  </Router>
), document.getElementById('root'))
