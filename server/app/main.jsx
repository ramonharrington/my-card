import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link, browserHistory } from 'react-router'

require("babel-core/register");
require("babel-polyfill");

import UserMap from './containers/UserMap/index.jsx';
import Homepage from './containers/Homepage/index.jsx';
import OrderConfirmation from './containers/OrderConfirmation/index.jsx';
import CongratulationShare from './containers/CongratulationShare/index.jsx';

import AccessDenied from './containers/AccessDenied/index.jsx';

ReactDOM.render((
  <Router history={browserHistory}>
    <Route path="/"                               component={Homepage} />

    <Route path="/noaccess"                       component={AccessDenied} />
    <Route path="/order-confirmation"             component={OrderConfirmation} />
    <Route path="/:userid/map"                    component={UserMap} />
	<Route path="/congratulation-share"           component={CongratulationShare} />
  </Router>
), document.getElementById('root'))

