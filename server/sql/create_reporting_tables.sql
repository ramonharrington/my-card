

IF OBJECT_ID('*|PREFIX|*users', 'U') IS NOT NULL
  drop table *|PREFIX|*users;

CREATE TABLE *|PREFIX|*users(
	[userid] [varchar](100) NULL,
	[email] [varchar](max) NULL,
	[created_dt] [datetime] NULL,
	[send_count] [int] NULL,
	[bitly_view_count] [int] NULL,
	[microsite_views] [int] NULL,
	[default_card_alt_doc_id] [varchar](50) NULL,
	[modified_dt] [datetime] NULL,
	[shopperkey] [int] NULL,
	[firstname] [varchar](100) NULL,
	[lastname] [varchar](100) NULL,
	[platform] [varchar](20) NULL,
	[app_version] [varchar](20) NULL,
	[build_version] [varchar](20) NULL,
	[feedback_ts] [datetime] NULL,
	[rating] [varchar](max) NULL,
	[feedback] [varchar](max) NULL,
  [referral_user] [varchar](max) NULL,
  [timezone_offset] int NULL
);

IF OBJECT_ID('*|PREFIX|*sends', 'U') IS NOT NULL
  drop table *|PREFIX|*sends;

CREATE TABLE *|PREFIX|*sends(
	[userid] [varchar](100) NULL,
	[shopperkey] [int] NULL,
	[created] [datetime] NULL,
	[alias] [varchar](100) NULL,
	[recipient] [varchar](100) NULL,
	[message] [varchar](max) NULL,
	[altitude] [decimal](16, 10) NULL,
	[latitude] [decimal](16, 10) NULL,
	[longitude] [decimal](16, 10) NULL,
  [speed] [decimal](16,10) NULL,
  [heading] [int] NULL,
  [accuracy] [int] NULL,
	[adminArea] [varchar](max) NULL,
	[location] [varchar](max) NULL,
	[alt_doc_id] [varchar](50) NULL
);

IF OBJECT_ID('*|PREFIX|*events', 'U') IS NOT NULL
  drop table *|PREFIX|*events;

CREATE TABLE *|PREFIX|*events(
	userid [varchar](100) NULL,
	created [datetime] NULL,
	event_type [varchar](20) NULL,
	event [varchar](max) NULL
);

IF OBJECT_ID('*|PREFIX|*contacts', 'U') IS NOT NULL
  drop table *|PREFIX|*contacts;

CREATE TABLE *|PREFIX|*contacts(
  userid varchar(100) NULL,
  first_name varchar(max) NULL,
  last_name varchar(max) NULL,
  email_label varchar(max) NULL,
  email_address varchar(max) NULL,
  phone_label varchar(max) NULL,
  phone_number varchar(max) NULL
);

IF OBJECT_ID('*|PREFIX|*pushes', 'U') IS NOT NULL
  drop table *|PREFIX|*pushes;

CREATE TABLE *|PREFIX|*pushes(
  userid varchar(100) NULL,
  send_dt [datetime] NULL,
  state varchar(100) NULL,
  type varchar(100) NULL,
  message varchar(max) NULL,
  created datetime NULL
);


IF OBJECT_ID('*|PREFIX|*referrals', 'U') IS NOT NULL
  drop table *|PREFIX|*referrals;

CREATE TABLE *|PREFIX|*referrals(
  userid varchar(100) NULL,
  state varchar(100) NULL,
  alias varchar(max) NULL,
  phone varchar(100) NULL,
  created datetime NULL
);



-- Indexes

-- create index *|PREFIX|*_mcu1 on mycard_users(userid);
-- create index *|PREFIX|*_mce1 on mycard_events(created);
-- create index *|PREFIX|*_mce2 on mycard_events(userid);
-- create index *|PREFIX|*_mce3 on mycard_events(userid,created);
