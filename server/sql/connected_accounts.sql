use scratch;
begin tran
	create table #mycardPhoneValidation
	(
		userid varchar(20) NULL,
		origPhone varchar(30) NULL,
		phone varchar(30) NULL,
	);

	/* INSERTS INTO TEMPORARY TABLE */
	-- insert into #mycardPhoneValidation values('b224b565', '+1 774-242-2270', null);
	*|INSERT_STMTS|*

	/* SCRUB DATA */
	update #mycardPhoneValidation set phone=REPLACE(origPhone, '-', '');
	update #mycardPhoneValidation set phone=REPLACE(phone, '+', '');
	update #mycardPhoneValidation set phone=REPLACE(phone, '(', '');
	update #mycardPhoneValidation set phone=REPLACE(phone, ')', '');
	update #mycardPhoneValidation set phone=REPLACE(phone, ',', '');
	update #mycardPhoneValidation set phone=REPLACE(phone, '.', '');
	update #mycardPhoneValidation set phone=REPLACE(phone, ' ', '');
	update #mycardPhoneValidation set phone=right(phone, len(phone)-(len(phone)-10));

	/* FIND MATCHES */
	select distinct mp.userid, va.email, mp.origPhone
	from reporting..vp_address va with (nolock) join #mycardPhoneValidation mp
		on va.phone = mp.phone
		and len(mp.phone) = 10
    and va.email not like '%vpguest.com'
	order by mp.origPhone

rollback;

