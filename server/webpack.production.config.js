'use strict';

var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var StatsPlugin = require('stats-webpack-plugin');

const autoprefixer = require('autoprefixer');

module.exports = {
  entry: {
    main: [
      'babel-polyfill',
      path.join(__dirname, 'app/main.jsx')
    ],
    dashboard: [
      'babel-polyfill',
      path.join(__dirname, 'app/dashboard.jsx')
    ],
  },
  output: {
    path: path.join(__dirname, '/dist/'),
    filename: '[name]-[hash].min.js',
    publicPath: '/'
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new HtmlWebpackPlugin({
      template: 'app/index.tpl.html',
      chunks: ['main'],
      inject: 'body',
      filename: 'main.html'
    }),
    new HtmlWebpackPlugin({
      template: 'app/index.tpl.html',
      chunks: ['dashboard'],
      inject: 'body',
      filename: 'dashboard.html'
    }),
    new ExtractTextPlugin('[name]-[hash].min.css'),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false,
        screw_ie8: true
      }
    }),
    new StatsPlugin('webpack.stats.json', {
      source: false,
      modules: false
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    }),
    new webpack.LoaderOptionsPlugin({
      options: {
        context: __dirname,
        postcss: [
          autoprefixer
        ]
      }
    })
  ],
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          "presets": ["es2015", "stage-0", "react"]
        }
      },
      {
        test: /\.json?$/,
        loader: 'json-loader'
      },
      {
        test: /\.s(a|c)ss/,
        loaders: ['style-loader', 'css-loader', 'sass-loader']
      },
      // CSS inlining and post-css filters
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader?-minimize!postcss-loader',
      }
    ]
  }
};