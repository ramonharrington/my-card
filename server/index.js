/* eslint no-console: 0 */
require('newrelic');

const path = require('path');
const express = require('express');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const config = require('./webpack.config.js');
const http = require('http');
const cluster = require('cluster');
const chalk = require('chalk');

const isDeveloping = ['production','test'].indexOf(process.env.NODE_ENV) < 0;
const port = process.env.PORT || 3001;
const app = express();

const expressSession = require('express-session');
const cors = require('express-cors');
const mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(expressSession);

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');

var cfg = require('./config');
var numCPUs = cfg.forks || 1;

process.on('uncaughtException', function(err) {
  console.log("uncaught exception: " + err);
  console.log(err.stack);
  process.exit(1);
});

if(cluster.isMaster){
  cluster.on('exit', function(worker){
    console.log("worker: " + worker.id + " is dead :-x");
    cluster.fork();
  });

  for(var i=0; i < numCPUs; i++){
    cluster.fork();
  }
}else{



  var logger = require('./lib/logger');

  var io = require('./lib/io.js');

  var db = require('./models/db');

  var server = http.createServer(app);
  server.listen(port);
  io.attach(server);

  app.use(bodyParser.json({limit: '50mb'}));
  app.use(bodyParser.urlencoded({extended: true}));
  app.use(cors());

  app.engine('handlebars', exphbs({defaultLayout: 'main'}));
  app.set('view engine', 'handlebars');


  app.use(express.static('public'))


  // Register the api routes
  //
  // Route search order
  //   api
  //   user microsites
  //   react router
  app.use('/api/v1/search', require('./routes/api/v1/search'));

  app.use('/api/v1/vcard', require('./routes/api/v1/vcard'));
  app.use('/api/v1/push', require('./routes/api/v1/push'));
  app.use('/api/v1', require('./routes/api/v1'));

  app.use('/api/admin', require('./routes/api/admin'));
  app.use('/u', require('./routes/microsites'));

  // nova sharing
  app.use('/card', require('./routes/novashare'));


  if (isDeveloping) {
    const compiler = webpack(config);
    const middleware = webpackMiddleware(compiler, {
      publicPath: config.output.publicPath,
      contentBase: 'src',
      stats: {
        colors: true,
        hash: false,
        timings: true,
        chunks: false,
        chunkModules: false,
        modules: false
      }
    });

    app.use(middleware);
    app.use(webpackHotMiddleware(compiler));
    app.get(/\/dashboard|\/login|\/care/, function response(req, res) {
      res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist/dashboard.html')));
      res.end();
    });
    app.get('*', function response(req, res) {
      res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist/main.html')));
      res.end();
    });
  } else {
    app.use(express.static(__dirname + '/dist'));

    app.get(/\/dashboard|\/login|\/care/, function response(req, res) {
      res.sendFile(path.join(__dirname, 'dist/dashboard.html'));
    });

    app.get('*', function response(req, res) {
      res.sendFile(path.join(__dirname, 'dist/main.html'));
    });
  }


  logger.log('info', chalk.green('*** Node Server and Socket.IO running on port ') + chalk.red(port));
}

