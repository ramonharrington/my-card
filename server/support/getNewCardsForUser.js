/*******************************
 * Get new business cards for a user and save the user
 *
 * NOTE:  We are not currently doing this as a background job
 * because it will change as part of the larger
 * one bitly link per send strategy
 *
*******************************/
var db = require('../models/db');
var User = require('../models/user');

var reporting = require('../lib/vpreporting');

User.findOne({email: 'dnaylor@vistaprint.com'}, function(err, user){

  reporting.getBusinessCards(user.email, function(reportingUser){
    console.log('Found ' + reportingUser.business_cards.length + ' cards: was ' + user.business_cards.length);

    if(reportingUser.business_cards.length > user.business_cards.length){
      user.business_cards = reportingUser.business_cards;

      console.log("updating users cards");
      user.save(function(err, newUser){
        process.exit();
      });
    }else{
      process.exit();
    }

  });
});
