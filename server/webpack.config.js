'use strict';

var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var precss = require('precss');
var autoprefixer = require('autoprefixer');
const DashboardPlugin = require('webpack-dashboard/plugin');


module.exports = {
  devtool: 'eval-source-map',
  entry: {
    main: [
      'babel-polyfill', 'webpack-hot-middleware/client?reload=true',
      path.join(__dirname, 'app/main.jsx')
    ],
    dashboard: [
      'babel-polyfill', 'webpack-hot-middleware/client?reload=true',
      path.join(__dirname, 'app/dashboard.jsx')
    ],
  },
  output: {
    path: path.join(__dirname, '/dist/'),
    filename: '[name].js',
    publicPath: '/'
  },
  plugins: [
    new DashboardPlugin(),
    new HtmlWebpackPlugin({
      template: 'app/index.tpl.html',
      chunks: ['main'],
      inject: 'body',
      filename: 'main.html'
    }),
    new HtmlWebpackPlugin({
      template: 'app/index.tpl.html',
      chunks: ['dashboard'],
      inject: 'body',
      filename: 'dashboard.html'
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development')
    })
  ],
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          "presets": ["react", "es2015", "stage-0", "react-hmre"]
        }
      },
      {
        test: /\.json?$/,
        loader: 'json-loader'
      },
      {
        test: /\.s(a|c)ss/,
        loaders: ['style-loader', 'css-loader', 'sass-loader']
      },
      // CSS inlining and post-css filters
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader?-minimize!postcss-loader',
      }
    ]
  }
};