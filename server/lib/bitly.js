var cfg = require('../config');
var axios = require('axios');

var BitlyAPI = require("node-bitlyapi");
var Bitly = new BitlyAPI({
  client_id: cfg.bitly.clientid,
  client_secret: cfg.bitly.secret,
});
Bitly.setAccessToken(cfg.bitly.token);


module.exports = {

  getClicks: function(link, callback){
    console.log("looking for link: " + link);
    Bitly.getLinkClicks({link: link}, function(err, results) {
      var rdata = JSON.parse(results);
      callback(rdata.data.link_clicks);
    });
  },

};





