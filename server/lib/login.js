var Nightmare = require('nightmare');		
var logger = require('../lib/logger');

var Xvfb = require('xvfb');
var env = process.env.NODE_ENV || 'development'

var tokenAuth = require('../lib/tokenAuth');
var slack = require('../lib/slack');

module.exports = {
  login: function(user, pass, token, callback){



    // token auth
    if(typeof token !== 'undefined' && token !== null){
      tokenAuth.testUserToken(user, token, function(isValid){
        if(!isValid){
          slack.sendMsg('Token login failed: ' + user);
          callback({error: 'Invalid token : ' + token}, null);
          return;
        }

        // valid token login
        slack.sendMsg('Token login success: ' + user);
        callback(null, {email: user});
        return;
      });

      return;
    }

    var electron = require('electron');
    var xvfb = new Xvfb();

    if(env === 'production'){
      electron = '/home/ubuntu/electron/electron';
      xvfb.startSync();
    }


    var nightmare = Nightmare({
      show: env === 'production' ? false : true,
      typeInterval: 20,
      electronPath: electron,
      switches: {
        'ignore-certificate-errors': true,
      }
    });

    logger.log('info', 'User logging in: ' + user);
    nightmare
      .goto('https://secure.vistaprint.com/vp/ns/sign_in.aspx?rurl=%2Fhatchery%2Frpc%2Fmycard%2Fgetshopperdetails')
      .wait(2000)
      .type('#txtEmail', user)
      .type('#txtSignInPassword', pass)
      .click('.textbutton-inner-submit')
      .wait(2000)
      .evaluate(function () {
        return document.body.innerText;
      })
    .end()
    .then(function (result) {
      logger.log('info', 'Login successful');
      logger.log('info', result);

      if(env === 'production'){
        xvfb.stopSync();
      }

      try{
        callback(null, JSON.parse(result));
      }catch(ex){
        callback({error: "Invalid username or password"}, null);
      }
      return;
    })
    .catch(function (error) {
      logger.log('error', error);

      if(env === 'production'){
        xvfb.stopSync();
      }
      callback({error: "Invalid username or password"}, null);
    });
  }
}

