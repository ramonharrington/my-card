var cfg = require('../config');
var axios = require('axios');
var metrics = require('../lib/metrics');

var mongoose = require('mongoose');
var db = require('../models/db');
var User = mongoose.model('User');
var Push = mongoose.model('Push');

var io = require('../lib/io.js');

var apiurl = 'https://go.urbanairship.com/api/';


module.exports = {
  getNamedUsers: function(callback){
    var instance = axios.create({
      baseURL: apiurl,
      headers: {
        'Accept': 'application/vnd.urbanairship+json; version=3',
        'Authorization': 'Basic QXg5NkYzYm5UclNTZkx3QjhsTzdKZzpHWl9yU0ZYRFJZT0ZuVEZCeGJlQTlR',
      }
    });

    instance.get('named_users').then(function(resp){
      callback(resp.data);
    });
  },

  createPush: function(userid, message, send_dt, callback){
    let push = new Push();
    push.userid = userid,
    push.send_dt = send_dt,
    push.state = 'new',
    push.type = 'default',
    push.message = message,
    push.created = new Date(),

    push.save(function(err, newPush){
      io.sockets.emit('dashUpdates', JSON.stringify(newPush));

      if(typeof callback !== 'undefined' && callback !== null){
        metrics.inc('create_push');
        callback(newPush);
      }
    });
  },

  sendPush: function(userid, message, callback){
    var instance = axios.create({
      baseURL: apiurl,
      headers: {
        'Accept': 'application/vnd.urbanairship+json; version=3',
        'Authorization': 'Basic QXg5NkYzYm5UclNTZkx3QjhsTzdKZzpHWl9yU0ZYRFJZT0ZuVEZCeGJlQTlR',
      },
    });

    instance.get('named_users?id='+userid)
    .then(function(resp){
      let channels = [];

      for(let i=0; i<resp.data.named_user.channels.length; i++){
        let channel = resp.data.named_user.channels[i];

        if(channel.device_type === 'android'){
          channels.push({android_channel: channel.channel_id});
        }else{
          channels.push({ios_channel: channel.channel_id});
        }
      }

      if(channels.length === 0){
        console.log("No channels to send push to");
        throw('no channels found for push: ' + userid + ' : ' + message);
      }

      console.log("push sending");
      instance.post('push', JSON.stringify({
        audience: {
          or: channels,
        },
        notification: {
          alert: message,
        },
        device_types: 'all',
      })).then(function(resp){
        metrics.inc('send_push_success');
        callback(true);
      }).catch(function(err){
        metrics.inc('send_push_error');
        console.log("ERROR");
        callback(false, err);
      });
    })
    .catch(function(err){
      callback(false, 'no devices found for user ' + userid);
    });;
  }
};

