var mongoose = require('mongoose');
var db = require('../models/db');
var User = mongoose.model('User');
var Push = mongoose.model('Push');
var UserToken = mongoose.model('UserToken');

var logger = require('../lib/logger');


module.exports = {
  createUserToken: function(email, callback){
    var token = Math.round(Math.random()*1000000);

    UserToken.findOne({ email: email }, function(err, uts){
      if(uts !== null){
        callback(uts);
        return;
      }

      var ut = new UserToken({
        email: email,
        token: token,
      });

      ut.save(function(err,nut){
        callback(nut);
        return;
      });

    });
  },

  testUserToken: function(email, token, callback){

    UserToken.findOne({ email: email }, function(err, ut){
      if(typeof ut === 'undefined' || ut === null){
        logger.log('error', "couldnt find a usertoken with that email: " + email);
        callback(false);
        return;
      }

      if(ut.token === token){
        logger.log('info', "successful token login for email: " + email);
        callback(true);
        return;
      }

      logger.log('info', "failed token login for email: " + email);
      callback(false);
    });
  },
};


