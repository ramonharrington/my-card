var fs = require('fs');

var mongoose = require('mongoose');
var db = require('../models/db');
var User = mongoose.model('User');
var Push = mongoose.model('Push');
var Referral = mongoose.model('Referral');
var AddressBook = mongoose.model('AddressBook');

var ProgressBar = require('progress');

var sql = require('mssql');
var host = 'mssql://vistaread:gitthe411@prdrptadh.vistaprint.svc/scratch';
var sqlconfig = {
  user: 'vistaread',
  password: 'gitthe411',
  server: 'prdrptadh.vistaprint.svc',
  database: 'scratch',
  requestTimeout: 300000,
};

var config = require('../config');
var logger = require('../lib/logger');

function getSQLHeader(){
  var db = 'scratch';
  var table_prefix = config.env === 'production' ? 'mycard_' : ('mycard_' + config.env + '_');
  var inserts = 'use ' + db + '; ';

  return {header: inserts, table_prefix: table_prefix};
}

function insertUserData(offset, limit){
  if(!offset){
    offset = 0;
  }
  if(!limit){
    limit = 500;
  }

  let headers = getSQLHeader();
  let table_prefix = headers.table_prefix;
  let inserts = headers.header;

  return new Promise( resolve => {
    User.find({}).skip(offset).limit(limit).populate(['referrals', 'referralUser']).exec(function(err, users){

      if(users.length === 0){
        resolve(null);
      }

      for(let i=0; i<users.length; i++){
        let user = users[i];

        let shopper_data = {
          userid: user.userid,
          shopperkey: user.shopperkey,
          email: user.email,
          firstname: user.first_name,
          lastname: user.last_name,
          created_dt: user.created ? new Date(user.created) : null,
          platform: user.platform,
          send_count: user.history.length,
          bitly_view_count: user.shorturl_clicks,
          microsite_views: user.microsite_views,
          default_card_alt_doc_id: user.default_card ? user.default_card.alt_doc_id : null,
          modified_dt: new Date(user.modified),
          app_version: user.appVersion,
          build_version: user.buildVersion,
          feedback_ts: user.feedback.length > 0 ? user.feedback[0].ts : null,
          rating: user.feedback.length > 0 ? user.feedback[0].rating : null,
          feedback: user.feedback.length > 0 ? user.feedback[0].message : null,
          referral_user: user.referralUser ? user.referralUser.userid : null,
          timezone_offset: user.timezoneOffset,
        };

        inserts = inserts + ' ' + insert(table_prefix + 'users', shopper_data);

        for(let j=0; j < user.history.length; j++){
          let send = user.history[j];

          let send_data = {
            userid: user.userid,
            shopperkey: user.shopperkey,
            alt_doc_id: send.card,
            created: new Date(send.ts),
            alias: send.alias,
            recipient: send.recipient,
            message: send.message,
            altitude: send.location && send.location.updated ? send.location.rawLocation.coords.altitude : null,
            latitude: send.location && send.location.updated ? send.location.rawLocation.coords.latitude : null,
            longitude: send.location && send.location.updated ? send.location.rawLocation.coords.longitude : null,
            speed: send.location && send.location.updated ? send.location.rawLocation.coords.speed : null,
            heading: send.location && send.location.updated ? send.location.rawLocation.coords.heading : null,
            accuracy: send.location && send.location.updated ? send.location.rawLocation.coords.accuracy : null,
            adminArea: send.location && send.location.updated ? send.location.geoLocation.adminArea : null,
            location: send.location && send.location.updated ? send.location.value : null,
          };

          inserts = inserts + ' ' + insert(table_prefix + 'sends', send_data);
        }

        //for(let j=0; j < user.tracking_data.length; j++){

          //let evt = user.tracking_data[j];

          //inserts = inserts + ' ' + insert(table_prefix + 'events', {
            //userid: user.userid,
            //created: evt.ts,
            //event_type: evt.event.split('_')[0],
            //'event': evt.event,
          //});
        //}

        //for(let j=0; j<user.referrals.length; j++){
          //let evt = user.referrals[j];

          //inserts = inserts + ' ' + insert(table_prefix + 'referrals', {
            //userid: user.userid,
            //phone: evt.phone,
            //alias: evt.alias,
            //state: evt.state,
            //created: evt.created,
          //});
        //}
      }

      resolve(inserts);
    });
  });
}

function writeSQL(query){

  return new Promise( resolve => {
    sql.connect(sqlconfig).then(function(){
      new sql.Request().query(query).then(function(recordset){
        sql.close();
        resolve();
      });
    });
  });
}

function querySQL(query, callback){
  sql.close();
  return new Promise( resolve => {
    sql.connect(sqlconfig).then(function(){
      new sql.Request().query(query).then(function(recordset){
        resolve(recordset.recordset);
        sql.close();
      });
    });
  });
}

function insertPushes(offset, limit){
  let headers = getSQLHeader();
  let table_prefix = headers.table_prefix;
  let inserts = headers.header;

  return new Promise( resolve => {
    Push.find({}, function(err, pushes){
      let inserts = '';

      for(let i=0; i<pushes.length; i++){
        inserts = inserts + ' ' + insert(table_prefix + 'pushes', {
          userid: pushes[i].userid,
          send_dt: pushes[i].send_dt,
          state: pushes[i].state,
          type: pushes[i].type,
          message: pushes[i].message,
          created: pushes[i].created,
        });
      }

      resolve(inserts);
    });
  });
}

function insertContacts(offset, limit){
  let headers = getSQLHeader();
  let table_prefix = headers.table_prefix;
  let inserts = headers.header;

  return new Promise( resolve => {
    AddressBook.find({}, function(err, addressBooks){
      for(let i=0; i<addressBooks.length; i++){
        let user = addressBooks[i];

        for(let j=0; j<user.addressBook.length; j++){
          let contact = user.addressBook[j];


          for(let k=0; k<contact.emailAddresses.length; k++){
            inserts = inserts + ' ' + insert(table_prefix + 'contacts', {
              userid: user.userid,
              first_name: contact.givenName,
              last_name: contact.familyName,
              email_label: contact.emailAddresses[k].label,
              email_address: contact.emailAddresses[k].email,
            });
          }


          for(let k=0; k<contact.phoneNumbers.length; k++){
            inserts = inserts + ' ' + insert(table_prefix + 'contacts', {
              userid: user.userid,
              first_name: contact.givenName,
              last_name: contact.familyName,
              phone_label: contact.phoneNumbers[k].label,
              phone_number: contact.phoneNumbers[k].number,
            });
          }
        }
      }

      resolve(inserts);
    });
  });
}

function getCreateStmts(){
  var db = 'scratch';
  var table_prefix = config.env === 'production' ? 'mycard_' : ('mycard_' + config.env + '_');

  var createStmts = fs.readFileSync('./sql/create_reporting_tables.sql').toString().replace(/\*\|PREFIX\|\*/g,table_prefix);
  return 'use ' + db + '; ' + createStmts;
}

function insert(table, obj){
  var values = [];

  for(let i=0; i<Object.keys(obj).length; i++){
    var val = obj[Object.keys(obj)[i]];
    if(val === '' || val === null || typeof val === 'undefined'){
      val = 'null'
    }else if(typeof val.getDate !== 'undefined'){
      val = val.getFullYear() + '-' + (val.getMonth()+1) + '-' + val.getDate() + ' ' + val.getHours() + ':' + val.getMinutes();
    }

    if((typeof val === 'string' || typeof val === 'object') && val !== 'null'){
      val = val.replace(/'/g, "''");
      values.push("'" + val + "'");
    }else{
      values.push(val);
    }
  }

  var query = 'insert into ' + table + '(' + Object.keys(obj).join(',') + ') values(' + values.join(',') + ');\n';
  return query;
}

async function insertAllUserData(isConsole){
  var totalUsers = await User.count({});

  var bar = null;
  if(isConsole){
    bar = new ProgressBar('  inserting user data (' + totalUsers + ')  [:bar] :percent :etas', {
      complete: '=',
      incomplete: ' ',
      width: 20,
      total: totalUsers,
    });
  }

  let i = 0;
  var limit = 500;
  var userInserts = await insertUserData(i, limit);

  while(userInserts !== null){
    if(isConsole){
      bar.tick(limit);
    }

    await writeSQL(userInserts);

    i += limit;
    userInserts = await insertUserData(i, limit);
  }
}

module.exports = {
  status: async function(){

    var table_prefix = config.env === 'production' ? 'mycard_' : ('mycard_' + config.env + '_');
    var stat = null;
    var stats = {};

    stat = await querySQL('select count(*) \'total\' from ' + table_prefix + 'users');
    stats.total_users = stat[0].total;

    stat = await querySQL('select count(*) \'total\' from ' + table_prefix + 'sends');
    stats.total_sends = stat[0].total;

    stat = await querySQL('select count(*) \'total\' from ' + table_prefix + 'events');
    stats.total_events = stat[0].total;

    stat = await querySQL('select count(*) \'total\' from ' + table_prefix + 'pushes');
    stats.total_pushes = stat[0].total;

    stat = await querySQL('select count(*) \'total\' from ' + table_prefix + 'contacts');
    stats.total_contacts = stat[0].total;

    return new Promise( resolve => {
      resolve(stats);
    });
  },

  execute: async function(isConsole, callback){

    var start_time = new Date();
    console.log("Start time: " + start_time);

    var createStmts = getCreateStmts();
    console.log("Recreating table structure");
    await writeSQL(createStmts);

    await insertAllUserData(isConsole);

    var total_pushes = await Push.count({});
    console.log("  inserting push data (" + total_pushes + ")");
    let pushes = await insertPushes();
    await writeSQL(pushes);

    //var total_contacts = await AddressBook.count({});
    //console.log("  inserting contact data (" + total_contacts + ")");
    //let contacts = await insertContacts();
    //await writeSQL(contacts);

    console.log("End time: " + new Date());

    var runtime_sec = Math.floor((new Date() - start_time) / 1000);
    var runtime_min = Math.floor(runtime_sec / 60);
    runtime_sec = runtime_sec - (runtime_min * 60);

    console.log("Runtime: 0:" + runtime_min + ':' + runtime_sec);

    callback();


  }
}

