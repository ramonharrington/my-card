var sql = require('mssql');
var fs = require('fs');
var request = require('request');

var config = require('../config');

module.exports = {
  init: function(){
    console.log("HI!");
  },

  getVPContacts: function(userid, contacts, callback){
    var query = fs.readFileSync('sql/connected_accounts.sql').toString();

    let inserts = '';
    for(let i=0; i<contacts.length; i++){
      var entry = contacts[i];

      if(entry.phoneNumbers){
        for(let k=0; k<entry.phoneNumbers.length; k++){
          inserts = inserts + ' insert into #mycardPhoneValidation values(\'' + userid + '\', \'' + entry.phoneNumbers[k].number + '\', null);';
        }
      }
    }

    /* Replace any sql variables we need */
    query = query.replace(/\*\|INSERT_STMTS\|\*/g,inserts);

    var results = [];

    sql.close();
    sql.connect('mssql://vistaread:gitthe411@db2.vistaprint.svc/scratch').then(function(){
      new sql.Request().query(query).then(function(recordset){
        for(let i=0; i<recordset.recordset.length; i++){
          results.push({
            userid: userid,
            email: recordset.recordset[i].email,
            phone: recordset.recordset[i].origPhone,
          });
        }

        callback(results);

      }).catch(function(err){
        console.dir(err);
      });
    });
  },
};
