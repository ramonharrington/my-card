var cp = require('child_process');
var env = process.env.NODE_ENV || 'development'

var StatsD = require('node-statsd');
var client = new StatsD({
  host: 'prometheus.mycard.vistaprint.io',
  port: '9125',
  prefix: env + '_',
});

module.exports = {
  inc: function(name){
    client.increment(name);
  },

  post: function(name, value){
    let baseurl = 'http://prometheus.mycard.vistaprint.io:9091/metrics/job/mycard_' + env;
    //console.log("posting to: " + baseurl + " data: " + name + ' ' + value);

    let cmd = 'echo "' + name + ' ' + value + '" | curl -s --data-binary @- ' + baseurl;
    //console.log(cmd);
    cp.exec(cmd, function(error, stdout, stderr){
    });
  },
};
