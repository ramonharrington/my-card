var axios = require('axios');
var config = require('../config');

module.exports = class{

  static hasBackside(docId){
    var url = config.monolithOriginurl + '/documentstest/api/DocumentTest/'+docId+'/pages/2';

    return new Promise( (resolve) => {
      axios.get(url).then( resp => {
        resolve(resp.data.Colorization !== 'Blank' && resp.data.IsBlank !== true);
      })
      .catch( err => {
        console.log("ERROR getting backside: " + docId + " - " + err);
        resolve(false);
      });
    });
  }
}

