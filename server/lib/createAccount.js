var Nightmare = require('nightmare');		
var logger = require('../lib/logger');

var Xvfb = require('xvfb');
var env = process.env.NODE_ENV || 'development'

var tokenAuth = require('../lib/tokenAuth');
var slack = require('../lib/slack');

module.exports = {
  createAccount: function(user, pass, emailOptIn, callback){

    var electron = require('electron');
    var xvfb = new Xvfb();

    if(env === 'production'){
      electron = '/home/ubuntu/electron/electron';
      xvfb.startSync();
    }


    var nightmare = Nightmare({
      show: env === 'production' ? false : true,
      typeInterval: 20,
      electronPath: electron,
      switches: {
        'ignore-certificate-errors': true,
      }
    });

    logger.log('info', 'Creating login: ' + user);
    nightmare
      
      .goto('http://www.vistaprint.com/vp/gateway.aspx?S=7164994172&preurl=/vp/ns/sign_in.aspx?rurl=%2Fhatchery%2Frpc%2Fmycard%2Fgetshopperdetails')
      .wait(2000)
      .type('#txtEmail', user)
      .click('#rblPasswordQuestion_0')
      .type('#txtEmailAgain', user)
      .type('#txtPassword', pass)
      .type('#txtPasswordAgain', pass)
      .wait(1000)
      .click(!emailOptIn ? '#noOptIn' : '.opt-in-radio-button')
      .wait(1000)
      .click('.textbutton-inner-submit')
      .wait(2000)
      .evaluate(function () {
        return document.body.innerText;
      })
    .end()
    .then(function (result) {
      logger.log('info', 'Account creation maybe successful');
      logger.log('info', result);

      if(env === 'production'){
        xvfb.stopSync();
      }

      try{
        callback(null, JSON.parse(result));
      }catch(ex){
        console.log("Error trying to parse result");
        callback({error: "Invalid username or password"}, null);
      }
      return;
    })
    .catch(function (error) {
      logger.log('error', error);

      if(env === 'production'){
        xvfb.stopSync();
      }
      callback({error: "Invalid username or password"}, null);
    });
  }
}

