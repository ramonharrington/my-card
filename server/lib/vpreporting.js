var sql = require('mssql');
var fs = require('fs');

var config = require('../config');
var queue = require('../lib/queue');


module.exports = {
  init: function(){
    console.log("HI!");
  },

  getBusinessCards: function(userOrEmail, callback){
    var query = fs.readFileSync('sql/bc_orders.sql').toString();
    let email = typeof userOrEmail === 'object' ? userOrEmail.email : userOrEmail;
    

    return new Promise( async (resolve) => {
      /*
       * Replace any sql variables we need
       */
      query = query.replace(/\*\|EMAIL\|\*/g,email);

      sql.close();
      await sql.connect('mssql://vistaread:gitthe411@db2.vistaprint.svc/reporting');
      let recordset = await new sql.Request().query(query);
      var json = {
        email: email,
      };

      if(recordset.recordset.length > 0){
        json.first_name = recordset.recordset[0].firstname;
        json.last_name = recordset.recordset[0].lastname;
        json.shopperkey = recordset.recordset[0].shopper_key;
      }

      json.business_cards = recordset.recordset.filter( (rec) => rec.order_id !== null).map(function(rec){

        if(rec.doc_id === null || rec.alt_doc_id === null){
          return;
        }

        // If a full user was passed in, check and see if there are any new docs, if there are
        // then let's refresh everything just incase.
        // If only an e-mail was passed in let's refresh everything no matter what
        //
        // Create a job to fetch the images and do our image processing on them
        let convertImage = false;

        if(typeof userOrEmail === 'object'){
          if(userOrEmail.business_cards.map((bc) => bc.alt_doc_id).indexOf(rec.alt_doc_id) < 0){
            console.log("couldn't find: " + rec.alt_doc_id);
            convertImage = true;
          }
        }else{
          convertImage = true;
        }

        if(convertImage){
          console.log("queueing: " + rec.alt_doc_id + ' - ' + rec.doc_id);
          queue.create('convert_images', {
            docid: rec.doc_id,
            altdocid: rec.alt_doc_id,
          });
        }

        let s3urlbase = 'https://s3.amazonaws.com/' + config.s3.bucket + '/' + config.s3.folder + '/' + rec.alt_doc_id;
        return {
          doc_id: rec.doc_id,
          alt_doc_id: rec.alt_doc_id,
          created: rec.created,
          front_img: config.urlbase + '/api/v1/proxy?url=http://www.vistaprint.com/any/lp.aspx?nochrome=1&alt_doc_id='+rec.alt_doc_id,
          back_img: config.urlbase + '/api/v1/proxy?url=http://www.vistaprint.com/any/lp.aspx?nochrome=1&alt_doc_id='+rec.alt_doc_id+'&page=2',
          combined_images: {
            overlap_diagonal: s3urlbase + '/combined.png',
            overlap_diagonal_rev: s3urlbase + '/combined_reverse.png',
            wood_vertical: s3urlbase + '/combined_wood.jpg',
            wood_vertical_rev: s3urlbase + '/combined_wood_reverse.jpg',
          }
        };
      });

      json.unordered_business_cards = recordset.recordset.filter( (rec) => rec.order_id === null).map(function(rec){

        // If a full user was passed in, check and see if there are any new docs, if there are
        // then let's refresh everything just incase.
        // If only an e-mail was passed in let's refresh everything no matter what
        //
        // Create a job to fetch the images and do our image processing on them
        let convertImage = false;

        if(typeof userOrEmail === 'object'){
          if(userOrEmail.unordered_business_cards && userOrEmail.unordered_business_cards.map((bc) => bc.alt_doc_id).indexOf(rec.alt_doc_id) < 0){
            console.log("couldn't find: " + rec.alt_doc_id);
            convertImage = true;
          }
        }else{
          convertImage = true;
        }

        if(convertImage){
          queue.create('convert_images', {
            docid: rec.doc_id,
            altdocid: rec.alt_doc_id,
          });
        }

        return {
          doc_id: rec.doc_id,
          alt_doc_id: rec.alt_doc_id,
          created: rec.created,
          front_img: config.urlbase + '/api/v1/proxy?url=http://www.vistaprint.com/any/lp.aspx?nochrome=1&alt_doc_id='+rec.alt_doc_id,
          back_img: config.urlbase + '/api/v1/proxy?url=http://www.vistaprint.com/any/lp.aspx?nochrome=1&alt_doc_id='+rec.alt_doc_id+'&page=2',
        };
      });

      if(callback){
        callback(json);
      }
      resolve(json);
    });
  },

  shopperExists: function(email, callback) {
    var query = fs.readFileSync('sql/shopper_id.sql').toString();

    /*
     * Replace any sql variables we need
    */
    query = query.replace(/\*\|EMAIL\|\*/g,email);

    sql.close();
    sql.connect('mssql://vistaread:gitthe411@db2.vistaprint.svc/reporting').then(function(){
      new sql.Request().query(query).then(function(recordset){
        callback(recordset.recordset.length > 0);
      });
    });
  },
};
