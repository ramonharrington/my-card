var cfg = require('../config');

var winston = require('winston');
var { SumoLogic } = require('winston-sumologic-transport');

winston.add(SumoLogic, {
  url: cfg.sumologic.endpoint,
});

module.exports = winston;

