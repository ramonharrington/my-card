var axios = require('axios');
var config = require('../config');

module.exports = {

  sendTemplate(passedData, callback){

    let baseData = {
      'key': config.mandrill.key,
      'headers': {
        'Reply-To': 'mycard@vistaprint.com'
      },
      'track_opens': true,
      'track_clicks': true
    };

    Object.assign(baseData, passedData);

    axios({
      method: 'POST',
      url: config.mandrill.url + config.mandrill.sendEmailUsingTemplate,
      data: baseData
    })
    .then(function(resp){
      if (resp.data.status == 'sent') {
        callback({error: null});
      } else {
        callback({error: 'Error sending email: ' + resp.data.code + ' - ' + resp.data.message});
      }
    });
  }
}

