var cfg = require('../config');
var axios = require('axios');

var fonz = require('fonz.js');
var twilio = require('twilio');


var twilioClient = twilio(cfg.twilio.sid, cfg.twilio.token);

module.exports = {
  isValidPhone: function(pn){
    if(fonz.validate(pn)){
      return true;
    }
    return false;
  },

  getPhoneDetails: function(pn, callback){
    if(this.isValidPhone(pn)){
      twilioClient.lookups.phoneNumbers(pn).fetch({type: 'carrier'}).then(function(resp, err){
        callback(resp);
      });
    }

    return false;
  },

  send: function(pn, msg){
    twilioClient.messages.create({
      body: msg,
      to: pn,
      from: cfg.twilio.pn
    });
  },

  voiceGreeting: function(){
    var twiml = new twilio.twiml.VoiceResponse();

    twiml.say('Thank you for contacting mycard.  Please leave your message after the beep - you can end the message by pressing any key.')

    twiml.record({
      maxLength: 120,
      action: '/api/v1/twilioVoicemail',
      transcribe: true,
      transcribeCallback: '/api/v1/twilioTranscribe',
    });

    return twiml.toString();
  },

  voiceRecordingEnd: function(){
    var twiml = new twilio.twiml.VoiceResponse();

    twiml.say('Thanks for leaving us a message.  We will respond within 2 business days.  Goodbye!', {
      voice:'alice'
    })

    twiml.hangup();

    return twiml.toString();
  },
};

