var axios = require('axios');
var logger = require('../lib/logger');

var mongoose = require('mongoose');
var db = require('../models/db');
var User = mongoose.model('User');
var VPCash = mongoose.model('VPCash');

var push = require('../lib/push');
var slack = require('../lib/slack');

var queue = require('../lib/queue');

module.exports = {
  createCashRecord: function(userid, callback){
    User.findOne({userid: userid}, function(err, user){
      if(err || !user){
        callback(false);
        logger.log('error', 'Unable to find user: ' + userid + ' err: ' + err);
      }

      let cash = new VPCash({
        user: user,
        status: 'pending',
        requestdata: {
          ShopperKey: parseInt(user.shopperkey),
          ClientCampaignExecutionKey: 0,
          ClientCampaignKey: 0,
          RewardCount: 1,
          RewardExpirationDate: new Date(Date.now() + (1000 * 60 * 60 * 24 * 30)), // 30 days
          RewardProgramId: 250,
        },
      });

      cash.save(function(err, newCash){
        if(err){
          callback(false);
          logger.log('error', 'Unable to save vpcash record: ' + err);
        }

        if(!user.vpcash){
          user.vpCash = [];
        }
        user.vpCash.push(newCash);

        user.save(function(err, newUser){

          var job = queue.create('grant_vistaprint_cash', { vp_cash_id: newCash._id});
          callback(newCash);
        });
      });
    });
  },

  grantCash: function(cashRecordId, callback){

    var instance = axios.create({
      baseURL: 'https://services.vistaprint.com',
      headers: {
        'ValidationToken': '7c508e50d613a87a3ad752164f3f01d5',
        'Content-Type': 'application/json',
      }
    });

    VPCash.findById(cashRecordId).populate('user').exec(function(err, cashRecord){
      var request = [cashRecord.requestdata];

      instance.post('/rewards/api/rewardrequests', request)
        .then(function(response){
          var rvalue = response.data;
          if(rvalue){
            slack.sendMsg(':heavy_dollar_sign: vistacash awarded to user: ' + cashRecord.user.email);
            push.createPush(
              cashRecord.user.userid,
              '$5 in Vistaprint Cash is now in your account',
              new Date(Date.now() + (1000 * 60 * 60 * 5)), // 1 hour
              null
            );

            cashRecord.status = 'granted';
            cashRecord.save();
          }else{
            slack.sendMsg('failed to award cash to user: ' + user.email);
          }

          if(callback){
            callback(rvalue);
          }
        })
      .catch(function(response){
        console.dir(response);
        if(callback){
          slack.sendMsg('failed to award cash to user: ' + user.email);
          callback(false);
        }
      });
    });
  },
}
