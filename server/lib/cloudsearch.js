var config = require('../config');

var mongoose = require('mongoose');
var db = require('../models/db');
var User = mongoose.model('User');

var $search = require('cloudsearch')({
  endpoint: config.cs.endpoint,
  accessKeyId: config.s3.accessKeyId,
  secretAccessKey: config.s3.secretAccessKeyId,
  region: 'us-east-1',
});


function uploadToCloudsearch(user){
  return new Promise( resolve => {
    $search.upload(user.userid, user, function(err, data){
      if(err){
        console.log('error', err);
      }

      resolve();
    });
  });
}

async function reindex(){
  var users = await User.find({}).select('-_id userid email firstname lastname').exec();

  console.log("users length: " + users.length);
  var promises = [];
  for(let i=0; i<users.length; i++){
    let user = users[i];
    promises.push(uploadToCloudsearch(user));
  }

  await Promise.all(promises);
  return users.length
}


module.exports = class CloudSearch{
  static reindex(){
    console.log("reindexing ...");
    return new Promise( resolve => {
      var users_length = reindex();
      resolve(users_length);
    });
  }

  static search(query){
    return new Promise( resolve => {
      $search
        .find(query)
        .query(function(err, data){
          var userids = Object.keys(data).filter( e => { if(e) return e } );

          User.find({userid: userids}, function(err, users){
            resolve(users);
          });
        });
    });
  }
}


