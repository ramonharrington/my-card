var config = require('../config');

var kue = require('../lib/queue').kue;
var queue = require('../lib/queue').queue;

var logger = require('../lib/logger');
var vpcash = require('../lib/vpcash');



queue.process('grant_vistaprint_cash', function(job, done){
  var domain = require('domain').create();
  domain.on('error', function(err){
    done(err);
  });
  domain.run(function(){
    logger.log('info', 'processing vpcash job: cash id: ' + job.data.vp_cash_id);

    vpcash.grantCash(job.data.vp_cash_id, function(resp){
      if(resp){
        logger.log('info', 'done processing job: cash id: ' + job.data.vp_cash_id);
        done();
      }else{
        done('error attempting to grant vistaprint cash');
      }
    });
  });
});

