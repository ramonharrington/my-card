var cp = require('child_process');
require('newrelic');

const cluster = require('cluster');
var config = require('../config');

var reporting = require('../lib/vpreporting.js');
var vpdocument = require('../lib/vpdocument.js');

var mongoose = require('mongoose');
var db = require('../models/db');
var User = mongoose.model('User');
var Push = mongoose.model('Push');

var metrics = require('../lib/metrics');
var io = require('../lib/io.js');
var tinify = require('tinify');
var s3 = require('s3');

var pushutils = require('../lib/push');
var mandrill = require('../lib/mandrill');

var queue = require('../lib/queue');

var logger = require('../lib/logger');
var migrateReporting = require('../lib/migrateReporting');


//queue.create('tinify_reporting', {}, 'global');
//setInterval(function(){
  //metrics.inc('worker_tinify_reporting');
  //queue.create('tinify_reporting', {});
//}, 1000 * 60 * 60 * 2);


function tinify_reporting(){
  return new Promise( (resolve) => {
    tinify.key = config.tinify.key;
    tinify.validate( (err) => {
      if(err){
        throw(err);
      }

      logger.log('info', "posting tinify compression count: " + tinify.compressionCount);
      metrics.post('tinifyMonthly', tinify.compressionCount);
      resolve();
    });
  });
}


async function update_card_cache(data){
  metrics.inc('worker_update_card_cache');
  logger.log('debug', 'processing job: update_card_cache : ' + data.email);

  let user = await User.findOne({email: data.email});
  let reportingUser = await reporting.getBusinessCards(user);

  // Check and see if the combined images are present, and the number we are expecting
  // TODO: No magic numbers, it'd be nice if this knew what templates we've added and
  // acted accordingly
  var hasCombinedImages = false;
  for(let i=0; i<user.business_cards.length; i++){
    let b = user.business_cards[i];

    if(typeof b.combined_images !== 'undefined' && Object.keys(b.combined_images).length === 2){
      hasCombinedImages = true;
    }
  }

  if(reportingUser.business_cards.length != user.business_cards.length || !hasCombinedImages){
    user.business_cards = reportingUser.business_cards;
  }

  if(reportingUser.unordered_business_cards.length != user.unordered_business_cards.length){
    user.unordered_business_cards = reportingUser.unordered_business_cards;
  }

  await user.save();
}

async function send_card_email(data){
  metrics.inc('send_card_email');
  logger.log('debug', 'processing job: send_card_email : ' + data.senderEmail);

  if ((typeof data.recipient == 'undefined' || data.recipient == '') ||
    (typeof data.cardUrl == 'undefined' || data.cardUrl == '') ||
    (typeof data.senderEmail == 'undefined' || data.senderEmail == '') ||
    (typeof data.senderName == 'undefined' || data.senderName == '') ||
    (typeof data.message == 'undefined' || data.message == '') ||
    (typeof data.userId == 'undefined' || data.userId == '')) {
      throw 'Missing required fields';
  }

  let data = {
    'template_name': config.mandrill.sendMycardEmailTemplate,
    'template_content': [
      { 'name': 'EMAIL', 'content': data.recipient },
      { 'name': 'CARD_URL', 'content': data.cardUrl },
      { 'name': 'MESSAGE', 'content': data.message },
      { 'name': 'USERID', 'content': data.userId }
    ],
    'message': {
      'subject': 'Here\'s my business card',
      'from_email': 'mycard@mycard.vistaprint.io',
      'from_name': data.senderName,
      'to': [
        {
          'email': data.recipient,
          'type': 'to'
        }
      ],
      'headers': {
        'Reply-To': data.senderEmail
      },
      'global_merge_vars': [
        { 'name': 'EMAIL', 'content': data.recipient },
        { 'name': 'CARD_URL', 'content': data.cardUrl },
        { 'name': 'MESSAGE', 'content': data.message },
        { 'name': 'USERID', 'content': data.userId }
      ],
    },
  };

  return new Promise( (resolve) => {
    mandrill.sendTemplate(data, function(status){
      if(status.error === null) {
        throw(status.error);
      }

      resolve();
    });
  });
}


function migrate_reporting_data(){
  logger.log('info', 'Starting migration');

  return new Promise( (resolve) => {
    migrateReporting.execute(false, function(){
      logger.log('info', 'Done migration');
      resolve();
    });
  });
}

async function convert_images(data){
  metrics.inc('worker_convert_images');
  logger.log('info', 'processing job: ' + JSON.stringify(data));

  await convertimage(data.docid, data.altdocid);
}

async function convertimage(docid, altdocid) {

  var cmd = 'bin/imageconverter.sh';

  var hasBackside = true;
  
  return new Promise( async (resolve, reject) => {
    if(docid){
      try{
        hasBackside = await vpdocument.hasBackside(docid);
      }catch(err){
        hasBackside = false;
      }

      if(!hasBackside){
        logger.log('info', "running single for: " + altdocid);
        cmd = 'bin/imageconverter_single.sh';
      }
    }
  
    cp.exec(cmd + ' ' + altdocid, async function(err, stdout, stderr){
      if(err){
        console.log("ERROR in convert image: " + err);
        //return done(new Error('ERROR: ' + err));
      }
      if(stderr){
        console.log("ERROR in convert image: " + stderr);
        //return done(new Error('ERROR: ' + stderr));
      }

      var json = JSON.parse(stdout);

      logger.log('info', 'starting image conversion for ' + json.altdocid);
      logger.log('info', 'uploading from: ' + json.tempdir);

      var promises = [];

      // Backwards compatibility - maybe can be deleted (testing req)
      promises.push(uploadS3('front.png', json.tempdir + '/page1_trimmed.png', json.altdocid));
      promises.push(uploadS3('back.png', json.tempdir + '/page2_trimmed.png', json.altdocid));
      promises.push(uploadS3('combined_wood.png', json.tempdir + '/combined_wood.png', json.altdocid));


      promises.push(uploadS3('combined.png', json.combined, json.altdocid));
      promises.push(uploadS3('combined_reverse.png', json.combined_reverse, json.altdocid));
      // New jpegs with better resolution and lower size
      promises.push(uploadS3('combined_wood.jpg', json.combined_wood, json.altdocid));
      promises.push(uploadS3('combined_wood_reverse.jpg', json.combined_wood_reverse, json.altdocid));
      promises.push(uploadS3('front.jpg', json.front, json.altdocid));
      promises.push(uploadS3('back.jpg', json.back, json.altdocid));

      await Promise.all(promises);

      logger.log('info', 'removing dir: ' + json.tempdir);
      cp.exec('rm -r ' + json.tempdir, function(err, stdout, stderr){
        resolve();
      });
    });
  });
}

function tinifyS3(key, localfile, altdocid, callback=null){
  return new Promise( resolve => {
    let source = tinify.fromFile(localfile);
    source.store({
      service: 's3',
      aws_access_key_id: config.s3.accessKeyId,
      aws_secret_access_key: config.s3.secretAccessKey,
      region: 'us-east-1',
      path: config.s3.bucket + '/' + config.s3.folder + '/' + altdocid + '/' + key,
    });

    resolve();
    if(typeof callback !== 'undefined' && callback !== null){
      callback();
    }
  });
}

function uploadS3(key, localfile, altdocid, callback=null){
  return new Promise( resolve => {
    var client = s3.createClient({
      s3Options: {
        accessKeyId: config.s3.accessKeyId,
        secretAccessKey: config.s3.secretAccessKey,
        region: 'us-east-1'
      }
    });

    var params = {
      localFile: localfile,
      s3Params: {
        Bucket: config.s3.bucket,
        Key: config.s3.folder + '/' + altdocid + '/' + key,
      }
    };

    var uploader = client.uploadFile(params);
    uploader.on('end', function(){
      resolve();

      if(typeof callback !== 'undefined' && callback !== null){
        callback();
      }
    });
  });
}


var numCPUs = config.forks || 1;

if(cluster.isMaster){
  cluster.on('exit', function(worker){
    logger.log('error', "worker: " + worker.id + " is dead :-x");
    cluster.fork();
  });

  for(var i=0; i < numCPUs; i++){
    cluster.fork();
  }

  logger.log('info', Object.keys(cluster.workers).length + ' active workers');

  setInterval( async () => {
    let size = await queue.queueSize();
    logger.log('debug', 'There are', size, 'messages on the queue. ' + Object.keys(cluster.workers).length + ' active workers');

    if(size > 500){
      logger.log('debug', 'Over 500 .... forking a new child (curr workers: ' + cluster.workers.length);
      cluster.fork();
    }
  }, 1000 * 60 * 10);
}else{
  queue.consumer( async (data, done) => {

    try{
      let job_name = data.MessageAttributes['job_name'].StringValue;
      var passed_data = JSON.parse(data.Body);
      logger.log('debug', "processing: " + job_name + ' : ' + data.Body);

      console.log("new job: " + job_name);
      switch(job_name){

        case 'tinify_reporting':
          await tinify_reporting();
          break;
        case 'update_card_cache':
          update_card_cache(passed_data);
          break;
        case 'send_card_email':
          await send_card_email(passed_data);
          break;
        case 'migrate_reporting_data':
          await migrate_reporting_data();
          break;
        case 'convert_images':
          await convert_images(passed_data);
          break;
        default:
          done("UNKNOWN JOB: " + job_name);
          throw("UNKNOWN JOB: " + job_name);
      }

      done();
    }catch(err){
      console.log("ERROR RUNNING JOB: " + err);
      done(err);
    }
  }).start();

  logger.log('debug', "Starting background job consumer");
}




