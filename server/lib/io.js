var io = require('socket.io')();
var sioredis = require('socket.io-redis');
var redis = require("redis"),
client = redis.createClient();

var mongoose = require('mongoose');
var db = require('../models/db');
var Beacon = mongoose.model('Beacon');


io.adapter(sioredis({ host: 'localhost', port: 6379 }));

io.sockets.on('connection', function (socket) {
  //console.log('Client is connected via socket.io!');
  //io.sockets.send('hi');
  //client.incr('users-online', function(err, reply){
    //console.log('   total users: ' + reply);
  //});

  socket.on('disconnect', function (socket) {
    //console.log('Client has left');
    //client.decr('users-online', function(err, reply){
      //console.log('   total users: ' + reply);
    //});
  });

  socket.on('beacon', function(data){
    Beacon.find({userid: data.userid}).remove().exec();
    new Beacon(data).save();
    io.sockets.emit('beacon-live-updates', JSON.stringify(data));
  });

  //io.sockets.emit('message', 'data');
  //socket.on('send', function (data) {
    //io.sockets.emit('message', data);
  //});
});


module.exports = io;
