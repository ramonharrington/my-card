var cfg = require('../config');
var axios = require('axios');
var io = require('../lib/io');


module.exports = {
  getBaseData: function(){
    return {
      channel: cfg.env === 'production' ? '#mycard-events' : '#mycard-events-dev',
      username: 'mycard',
      icon_url: 'https://mycard.vistaprint.com/images/app-icon.png',
    };
  },

  send(data){
    axios.post(cfg.connections.slack, data);
    io.sockets.emit('activity-live-updates', data.text);
  },

  sendMsg: function(msg){
    var data = this.getBaseData();
    data.text = msg;

    this.send(data);
  },

  sendDataMsg: function(msg){
    var data = this.getBaseData();
    Object.assign(data, msg);

    this.send(data);
  },

  userLoggedIn: function(user){
    var data = this.getBaseData();
    data.text = 'First time user: ' + user.email + '  ( ' + user.first_name + ' ' + user.last_name + ' )';
    this.send(data);
  },

  defaultBcSelected: function(user){
    var data = this.getBaseData();
    data.text = '<https://mycard.vistaprint.com/dashboard/user/' + user.userid + '|' + user.first_name + ' ' + user.last_name + '> selected a card';
    data.attachments = [{
      image_url:  'https://s3.amazonaws.com/' + cfg.s3.bucket + '/' + cfg.s3.folder + '/' + user.default_card.alt_doc_id + '/combined_wood.jpg'
    }];

    this.send(data);
  }
};

