var badges = [
  {
    title: 'The Icebreaker',
    description: 'First send',
    type: 'total_sends:1',
    icon: 'icebreaker.png',
  },
  {
    title: 'Beekeeper',
    description: '5 sends',
    type: 'total_sends:5',
    icon: 'beekeeper.png',
  },
  {
    title: 'The Barista',
    description: '10 sends',
    type: 'total_sends:10',
    icon: 'barista.png',
  },
  {
    title: 'Quarterback',
    description: '25 sends',
    type: 'total_sends:25',
    icon: 'quarterback.png',
  },
  {
    title: 'Hard Hat',
    description: '50 sends',
    type: 'total_sends:50',
    icon: 'hardhat.png',
  },
  {
    title: 'Flagship',
    description: '100 sends',
    type: 'total_sends:100',
    icon: 'flagship.png',
  },
  {
    title: 'Trekker',
    description: '4 different locations',
    type: 'unique_locations:4',
    icon: 'trekker.png',
  },
  {
    title: 'Referral',
    description: 'Customer shared card',
    type: 'shared_card:1',
    icon: 'referral.png',
  },
];

// Not a standard, but sorry I couldn't think of anything better
// 1 = exactly earns the badge (called at the right time, means they just got it)
// -1 = they got it earlier
// -1 < x > 1 = they don't have it, this is percentage that they have obtained
function sendTotalCalc(desired, numSends){
  if(typeof numSends === 'undefined' || numSends === null || numSends === 0) return 0;
  if(numSends === desired){return 1;} return (numSends / desired);
}

function locationTotalCalc(desiredLocations, history){
  let locations = history.map(function(hist){
    if(typeof hist.location === 'undefined' || hist.location === null || hist.location.length === 0){
      return null;
    }

    return hist.location.value;
  });

  let uniqueLocations = locations.unique().length;
  if(locations.contains('unknown')){
    uniqueLocations--;
  }

  if(uniqueLocations >= desiredLocations){
    return 1;
  }else{
    return uniqueLocations / desiredLocations;
  }
}


module.exports = {
  getBadges: function(){
    return {
      collected: [],
      available: badges,
    }
  },

  updateBadges: function(user){
    let available = user.badges.available;

    for(let i=0; i < available.length; i++){

      let badge = available[i];
      let type = badge.type.split(':')[0];
      let expValue = parseInt(badge.type.split(':')[1]);

      let earned = 0;

      switch(type){
        case 'total_sends':
          earned = sendTotalCalc(expValue, user.history.length);
          badge.goal = (expValue - user.history.length) + ' more sends needed';
          break;
        case 'unique_locations':
          earned = locationTotalCalc(expValue, user.history);
          badge.goal = (expValue - (earned * expValue)) + ' more locations needed';
          break;
        case 'shared_card':
          earned = 0;
          badge.goal = ' Shared card needed';
          break;
        default:
          earned = 0;
          break;
      }

      if(earned === 1){
        badge.earned_dt = Date.now();
        badge.percentage = 1;
        user.badges.collected.push(badge);
        user.badges.available.splice(i, 1);
      }else{
        badge.percentage = earned;
      }
    }

    return;
  },
}




/*** Monkey Patches ***/
Array.prototype.contains = function(v) {
  for(var i = 0; i < this.length; i++) {
    if(this[i] === v) return true;
  }
  return false;
};

Array.prototype.unique = function() {
  var arr = [];
  for(var i = 0; i < this.length; i++) {
    if(!arr.contains(this[i])) {
      arr.push(this[i]);
    }
  }
  return arr; 
}


