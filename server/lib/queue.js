var config = require('../config');
var logger = require('../lib/logger');

const Producer = require('sqs-producer');
const Consumer = require('sqs-consumer');
const AWS = require('aws-sdk');

AWS.config.update({
  region: 'eu-east-1',
  accessKeyId: config.s3.accessKeyId,
  secretAccessKey: config.s3.secretAccessKey
});

const producer = Producer.create({
    queueUrl: config.sqs.url,
    region: 'eu-east-1',
    accessKeyId: config.s3.accessKeyId,
    secretAccessKey: config.s3.secretAccessKeyId,
});


module.exports = {
  create: function(job_name, data, job_type){
    producer.send([
      {
        id: job_name,
        body: JSON.stringify(data),
        messageAttributes: {
          job_name:     { DataType: 'String', StringValue: job_name },
          job_location: { DataType: 'String', StringValue: (job_type || 'global') },
        }
      }], (err) => {
        if(err){
          console.log("ERROR CREATING JOB: " + err);
        }
      }
    );
  },

  consumer: function(work){
    if(!work || typeof work !== 'function'){
      console.log("ERROR: must pass in function");
      throw('ERROR: must pass in function');
    }

    return Consumer.create({
      queueUrl: config.sqs.url,
      attributeNames: ['All'],
      messageAttributeNames: ['job_name', 'job_location'],
      handleMessage: (message, done) => {
        // do some work with `message`
        work(message, done);
      },
      sqs: new AWS.SQS()
    });
  },

  queueSize: function(){
    return new Promise( (resolve) => {
      producer.queueSize( (err,size) => {
        resolve(size);
      });
    });
  },
}

