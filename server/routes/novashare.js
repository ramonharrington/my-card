var express = require('express');
var router = express.Router();

var cfg = require('../config');
var User = require('../models/user');
var VCard = require('../models/vcard');
var vcardsJS = require('vcards-js');

var metrics = require('../lib/metrics.js');
var fieldparser = require('./fieldparser.js');
var queue = require('../lib/queue').queue;
var slack = require('../lib/slack');
var axios = require('axios');

router.get('/:altdocid', function(req,res){
  metrics.inc('microsite_nova_view');
  res.render('novashare', {layout: null, altdocid: req.params.altdocid});
});

router.get('/:userid', function(req,res){
  metrics.inc('microsite_view');

  User.findOne({userid: req.params.userid}, function(err, user){
    if(user === null){
      metrics.inc('microsite_notfound');
      res.render('microsite/notfound', {layout: null});
      return;
    }

    recordViewHistory(req, user, 'bitly');
    user.save();

    res.render('microsite', {user: user, sendnum: 0, layout: null});
  });
});


async function getVCard(userid, sendnum, res){
  metrics.inc('microsite_vcard');

  var user = await User.findOne({userid: userid});

  if(user === null){
    metrics.inc('microsite_notfound');
    res.render('microsite/notfound', {layout: null});
    return;
  }

  //Default filename is initial user-submitted first+last name.
  var userFilename = user.first_name + (user.last_name === 'Customer' ? '' : user.last_name);

  let altdocid = null;
  if(sendnum == 0 || sendnum > user.history.length){
    if (user.default_card != null) {
      altdocid = user.default_card.alt_doc_id;
    }
  } else {
    altdocid = user.history[sendnum-1].card;
  }

  var vCard = vcardsJS();

  if(altdocid){
    console.log("altdocid: " + altdocid);
    var userVcard = await VCard.findOne({alt_doc_id: altdocid});

    if(userVcard){
      console.log("found custom vcard");
      //If there is a valid name on the sent card, that's the user's preferred name & we should use it for the filename.
      userFilename = userVcard.name ? userVcard.name : userFilename

      let userParts = userVcard.name ? userVcard.name.split(' ') : ['',''];

      vCard.firstName = userParts[0];
      vCard.lastName = userParts.splice(1).join(' '),

      vCard.url = userVcard.website || (cfg.urlbase + '/u/' + user.userid + '/' + sendnum);
      vCard.workUrl = vCard.url;
      vCard.workPhone = userVcard.phone;
      vCard.organization = userVcard.business_name;
      vCard.workEmail = userVcard.email;
    }else{
      console.log("no custom vcard");
      vCard = await scrapeAltDocIdContents(user, vCard, altdocid);
    }
  }else{
    console.log("legacy");
    // Legacy support
    var realLastName = user.last_name === 'Customer' ? '' : user.last_name;

    vCard.firstName = user.first_name;
    vCard.lastName = realLastName;
    vCard.url = cfg.urlbase + '/u/' + user.userid + '/' + sendnum;
    vCard.source = cfg.urlbase + '/u/' + user.userid + '/' + sendnum + '.vcf';
    vCard.workEmail = user.email;

    vCard = await scrapeAltDocIdContents(user, vCard, altdocid);
  }
    
  res.setHeader('Content-Type', 'text/x-vcard');
  res.setHeader('Content-Disposition', 'inline; filename="' + userFilename + '.vcf"');
  res.send(vCard.getFormattedString());
}

function scrapeAltDocIdContents(user, vCard, altdocid){
  return new Promise( (resolve) => {
    axios({
      method: 'GET',
      url: cfg.monolithurl + '/hatchery/rpc/mycard/getdocumentdetails?altDocId=' + altdocid,
      headers: {
        'Content-Type': 'application/json',
        'X-Hatch-Oath': 'This is my method. There are many like it, but this one is mine'
      }
    })
    .then(function(resp){
      if (resp != null && resp.response != null && resp.response.data != null) {
        vCard.email = fieldparser.getEmail(resp.response.data) || user.email;
        vCard.workUrl = fieldparser.getWebsite(resp.response.data);
        vCard.workPhone = fieldparser.getPhone(resp.response.data);
        // vCard.workAddress = fieldparser.getAddress(resp.response.data);
      }

      resolve(vCard);
    })
    .catch(function(resp){
      if (resp != null && resp.response != null && resp.response.data != null) {
        vCard.email = fieldparser.getEmail(resp.response.data) || user.email;
        vCard.workUrl = fieldparser.getWebsite(resp.response.data);
        vCard.workPhone = fieldparser.getPhone(resp.response.data);
        // vCard.workAddress = fieldparser.getAddress(resp.response.data);
      }

      resolve(vCard);
    });
  });
}

function recordViewHistory(req, user, key){
  if(typeof user.view_history !== 'object'){
    user.view_history = {};
  }
  if(!user.view_history[key]){
    user.view_history[key] = [];
  }
  user.view_history[key].push({
    ts: new Date(),
    ip: req.headers['x-real-ip'],
    ua: req.headers['user-agent'],
  });


  console.log('hit');
  if(/Slackbot|facebookexternalhit|bitlybot|TweetmemeBot/.test(req.headers['user-agent']) === false){
    console.log("double hit");
    if(!user.microsite_views){
      user.microsite_views = 0;
    }
    user.microsite_views++;
  }

  user.markModified('view_history');
}

module.exports = router;

