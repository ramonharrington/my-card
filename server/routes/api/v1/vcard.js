var express = require('express');
var router = express.Router();
var io = require('../../../lib/io.js');

var mongoose = require('mongoose');
var db = require('../../../models/db');
var User = mongoose.model('User');
var VCard = mongoose.model('VCard');

var config = require('../../../config/');

var logger = require('../../../lib/logger');
var phoneutils = require('../../../lib/phone');
var slack = require('../../../lib/slack');



/*
  userid: String,
  altdocid: String,
  frontPrimary: Boolean,
  email: String,
  business_name: String,
  phone: String,
  website: String,
*/
router.post('/', (req, res) => {

  var vcard = VCard.findOne({alt_doc_id: req.body.alt_doc_id}).then( vcard => {
    if(!vcard){
      vcard = new VCard();
    }

    Object.assign(vcard, req.body);
    console.log("saving vcard: " + JSON.stringify(vcard));
    vcard.save()
      .then( savedVcard => {
        res.send('ok');
      }).catch(err => logger.log('error', "VCARD ERROR: " + err) );
  });
});

router.get('/:userid', (req, res) => {
  VCard.find({userid: req.params.userid}).then( vcards => { res.json(vcards) });
});



module.exports = router;

