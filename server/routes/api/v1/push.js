var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var db = require('../../../models/db');
var User = mongoose.model('User');
var Push = mongoose.model('Push');

var io = require('../../../lib/io.js');

var phoneTool = require('../../../lib/phone');
var pushutils = require('../../../lib/push');

router.get('/enabledUsers', function(req, res){
  pushutils.getNamedUsers(function(namedUsers){
    res.status(200).send(namedUsers);
  });
});

router.post('/:id/force', function(req, res){
  Push.findById(req.params.id, function(err, push){
    push.send_dt = new Date();
    push.save(function(err,newPush){
      io.sockets.emit('dashUpdates', JSON.stringify(newPush));
      res.setHeader('Content-Type', 'application/json');
      res.status(200).send(JSON.stringify({status: 'ok'}));
    });
  });
});

router.post('/:id/reset', function(req, res){
  Push.findById(req.params.id, function(err, push){
    push.state = 'new';
    push.save(function(err,newPush){
      io.sockets.emit('dashUpdates', JSON.stringify(newPush));
      res.setHeader('Content-Type', 'application/json');
      res.status(200).send(JSON.stringify({status: 'ok'}));
    });
  });
});

router.delete('/:id', function(req, res){
  Push.findById(req.params.id, function(err, push){
    if(err || push === null){
      res.status(404).send('false');
    }else{
      Push.remove(push).exec();
    }
  });

  res.status(200).send("true");
});

router.get('/', function(req, res){
  Push.find(function(err, pushes){
    let buckets = {
      new: [],
      queued: [],
      complete: [],
    };

    if(pushes){
      for(let i=0; i<pushes.length; i++){
        if(pushes[i].state === 'new'){
          buckets.new.push(pushes[i]);
        }else if(pushes[i].state !== 'sent'){
          buckets.queued.push(pushes[i]);
        }else{
          buckets.complete.push(pushes[i]);
        }
      }
    }

    res.setHeader('Content-Type', 'application/json');
    res.status(200).send(JSON.stringify(buckets));
  });
});

router.post('/', function(req ,res){
  pushutils.createPush(req.body.userid, req.body.message, req.body.send_dt, function(newPush){
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send(JSON.stringify(newPush));
  });
});



module.exports = router;

