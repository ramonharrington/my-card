var express = require('express');
var router = express.Router();

var config = require('../../../config');
var cloudsearch = require('../../../lib/cloudsearch');

var logger = require('../../../lib/logger');
var io = require('../../../lib/io.js');

var mongoose = require('mongoose');
var db = require('../../../models/db');
var User = mongoose.model('User');

router.get('/', async function(req, res){
  let query = new RegExp(req.query.q, 'i');
  let queryfn = new RegExp(req.query.q.split(' ')[0], 'i');

  let users = await User.find({
    $or: [
      {email: query},
      {first_name: query},
      {first_name: queryfn},
      {last_name: query},
      {shopperkey: query}
    ]});

  res.setHeader('Content-Type', 'application/json');
  res.status(200).send(JSON.stringify(users));
});

router.get('/index', function(req, res){
  cloudsearch.reindex().then(function(total){
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send(JSON.stringify({total: total}));
  });
});


module.exports = router;

