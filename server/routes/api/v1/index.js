var express = require('express');
var router = express.Router();
var io = require('../../../lib/io.js');

var badges = require('../../../lib/badges');

var reporting = require('../../../lib/vpreporting.js');
var slack = require('../../../lib/slack');

var mongoose = require('mongoose');
var db = require('../../../models/db');
var User = mongoose.model('User');
var Push = mongoose.model('Push');
var AddressBook = mongoose.model('AddressBook');
var Perfmon = mongoose.model('Perfmon');
var Referral = mongoose.model('Referral');
var Beacon = mongoose.model('Beacon');

var uuid = require('uuid');
var config = require('../../../config/');

var child_process = require('child_process');
var metrics = require('../../../lib/metrics.js');
var queue = require('../../../lib/queue');

var login = require('../../../lib/login');
var tokenAuth = require('../../../lib/tokenAuth');
var logger = require('../../../lib/logger');
var phoneutils = require('../../../lib/phone');
var slack = require('../../../lib/slack');
var mandrill = require('../../../lib/mandrill');
var vpcontacts = require('../../../lib/vpcontacts');
var createAccount = require('../../../lib/createAccount');

var request = require('request');

var cors = require('cors');

router.get('/', function(req, res){
  res.send('Welcome to version 1 of the api');
});

router.get('/user/:userid', function(req, res){
  User.findOne({userid: req.params.userid}, { '_id': 0, vpContacts: 0, 'tracking_data': 0, 'view_history': 0}).populate(['referrals', 'vpCash']).exec(function(err, user){
    if(err || user === null){
      res.status(500).send("error finding user");
      return;
    }

    queue.create('update_card_cache', {email: user.email});

    res.setHeader('Content-Type', 'application/json');

    //backwards compatibility before v1
    user.referralUser = {};

    res.send(JSON.stringify(user));
  });
});

router.post('/token', function(req, res){
  reporting.shopperExists(req.body.email, function(exists) {
    if (!exists) {
      // No vistaprint shopper with this address.  Placeholder for now, maybe do something on the client
      logger.log('info', req.body.email + ' asked for a token but is not a vistaprint shopper');
    }

    tokenAuth.createUserToken(req.body.email, function(token){
      logger.log('info', 'token created for ' + req.body.email + ' : ' + token.token);
      slack.sendMsg('Sent new token to ' + req.body.email);

      let data = {
        'template_name': 'mycard-send-token',
        'template_content': [
          { 'name': 'TOKEN', 'content': token.token },
        ],
        'message': {
          'to': [
            {
              'email': req.body.email,
              'type': 'to'
            }
          ],
          'headers': {
            'Reply-To': 'mycard@vistaprint.com',
          },
          'global_merge_vars': [
            { 'name': 'TOKEN', 'content': token.token },
          ],
        },
      };

      mandrill.sendTemplate(data, function(status){

        res.setHeader('Content-Type', 'application/json');
        res.status(200).send(JSON.stringify({status: 'success'}));
      });
    });
  });
});


router.post('/login', function(req, res){
  metrics.inc('login_attempt');

  let perf = new Perfmon({
    email: req.body.email,
    name: 'login',
  });
  perf.events.push({ msg: 'starting login', created: new Date() });

  let email = req.body.email;
  let password = req.body.password;
  let token = req.body.token;

  // Lets test and see if this password is really an override token
  tokenAuth.testUserToken(email, password, (exists) => {
    if(exists){
      token = password;
      password = null;
    }

    login.login(email, password, token, function(err, json){
      perf.events.push({ msg: 'logged in', created: new Date() });

      if(err){
        logger.log('error', err);
        metrics.inc('login_error');
        res.setHeader('Content-Type', 'application/json');
        res.status(500).send(err);

        slack.sendMsg('Login failed for ' + email);
        return;
      }

      metrics.inc('login_success');
      let startTime = new Date().getTime();

      User.findOne({email: email.toLowerCase()}, function(err, user){
        if(user !== null){
          // always get the most recent business cards
          reporting.getBusinessCards(email, function(json) {
            perf.events.push({ msg: 'got business cards', created: new Date() });

            let totalMs = new Date().getTime() - startTime;
            if (totalMs > 5000) {
              logger.log('error', 'Retrieving business cards took ' + totalMs + 'ms')
                slack.sendMsg(':lightning: Retrieving business cards took ' + totalMs + 'ms');
            }

            user.business_cards = json.business_cards;
            user.unordered_business_cards = json.unordered_business_cards;
            if(user.default_card){
              for(let i=0; i<user.business_cards.length; i++){
                if(user.default_card.alt_doc_id === user.business_cards[i].alt_doc_id){
                  user.default_card = user.business_cards[i];
                }
              }
            }

            user.save().then( (dbUser) => {
              perf.save();

              user['_id'] = null;
              user.vpContacts = [];
              user['tracking_data'] = {};
              user['view_history'] = {};

              res.setHeader('Content-Type', 'application/json');
              res.send(JSON.stringify(user));
            });
          });
        } else {
          metrics.inc('login_new');

          reporting.getBusinessCards(email, function(json){
            perf.events.push({ msg: 'got business cards', created: new Date() });

            let totalMs = new Date().getTime() - startTime;
            if (totalMs > 5000) {
              logger.log('error', 'Retrieving business cards took ' + totalMs + 'ms')
                slack.sendMsg(':lightning: Retrieving business cards took ' + totalMs + 'ms');
            }
            user = new User(json);

            // User setup
            user.userid = uuid.v4().split('-')[0];
            user.history =[];
            user.default_card = {};
            user.badges = badges.getBadges();
            user.modified = Date.now();
            user.created = Date.now();
            user.shorturl = config.urlbase + '/u/' + user.userid;

            perf.save();
            user.save(function(err,savedUser){
              slack.userLoggedIn(savedUser);
              res.setHeader('Content-Type', 'application/json');
              res.send(JSON.stringify(savedUser));
            });
          });
        }
      });
    });
  });
});

router.post('/updateUser', function(req,res){
  // update badges
  let history = req.body.history;
  if(typeof history === 'undefined' || history === null){
    history = [];
  }

  badges.updateBadges(req.body);
  updateUser(req.body, function(newUser){
    // empty out the tracking data -- too much data to send to clients
    newUser.tracking_data = [];

    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(newUser));
  }, res);
});

async function updateUser(passedUser, callback, res){
  let existingUser = await User.findOne({userid: passedUser.userid}).populate('referrals').exec();

  if(!existingUser) return res.send(500, {error: err});

  // Update the business cards displayed on the user's app
  queue.create('update_card_cache', {email: existingUser.email});

  // Alert us to a new default bc selected
  try{
    if(
        typeof passedUser.default_card !== 'undefined' && passedUser.default_card !== null 
        && ((typeof existingUser.default_card === 'undefined' || existingUser.default_card === null) 
          || (passedUser.default_card.doc_id !== existingUser.default_card.doc_id)
          )){
      slack.defaultBcSelected(passedUser);
    }
  }catch(ex){
    logger.log('error sending slack message', ex);
  }

  // Alert us to a new send
  try{
    if( existingUser.history.length < passedUser.history.length ){
      let lastSend = passedUser.history[passedUser.history.length-1];

      let msg = ":incoming_envelope: new send by <https://mycard.vistaprint.com/dashboard/user/" + existingUser.userid + '|' + existingUser.first_name + ' ' + existingUser.last_name + '> - ' + lastSend.location.value;
      slack.sendMsg(msg);
    }
  }catch(ex){
    logger.log('error', ex);
  }

  // New version is detected
  let oldVersion = existingUser.buildVersion;
  let newVersion = passedUser.buildVersion;

  if(oldVersion && newVersion && (parseInt(oldVersion) !== parseInt(newVersion))){
    slack.sendMsg(':rocket: ' + existingUser.email + ' has upgraded from ' + existingUser.buildVersion + ' to ' + passedUser.buildVersion);

    // Force image regeneration
    existingUser.business_cards = [];
    existingUser.unordered_business_cards = [];

    let reportingUser = await reporting.getBusinessCards(existingUser);
    existingUser.business_cards = reportingUser.business_cards;
    existingUser.unordered_business_cards = reportingUser.unordered_business_cards;
  }


  // Here is where we cherrypick the updates to make
  existingUser.modified = Date.now();
  existingUser.badges = passedUser.badges;    // the only reason we are keeping this is because we already updated it above
  existingUser.history = passedUser.history;
  existingUser.settings = passedUser.settings;
  existingUser.default_card = passedUser.default_card;
  existingUser.platform = passedUser.platform;
  existingUser.appVersion = passedUser.appVersion;
  existingUser.buildVersion = passedUser.buildVersion;
  existingUser.timezoneOffset = passedUser.timezoneOffset;
  existingUser.feedback = passedUser.feedback;

  // Tracking data -- always appends to current record
  if(typeof existingUser.tracking_data ===  'undefined'){
    existingUser.tracking_data = [];
  }
  if(typeof passedUser.tracking_data === 'undefined'){
    passedUser.tracking_data = [];
  }
  existingUser.tracking_data = [...existingUser.tracking_data, ...passedUser.tracking_data];
  existingUser.markModified('tracking_data');

  // If the client has the force bit checked, and we have it, then remove it from us.
  // Assuming the user already saw the pop, and take this date as assuming when it
  // was shown to be sure we do not overwhelm the user no matter what
  if(existingUser.forceAppFeedback && passedUser.forceAppFeedback){
    existingUser.forceAppFeedback = false;
    existingUser.lastShownAppFeedback = new Date();
  }

  existingUser.save(function(err, savedUser){
    if(err) return res.send(500, {error: err});

    callback(savedUser);
    io.sockets.emit('dashUpdates', JSON.stringify(savedUser.userid));
    return;
  });
}

router.post('/feedback', function(req, res){
  slack.sendMsg(':star: new rating: <https://mycard.vistaprint.com/dashboard/user/' + req.body.userid + '|' + req.body.userid + ' - ' + req.body.email + '> :thumbs' + req.body.rating + ': ' + req.body.message);

  res.status(200).send({status: 'okay'});
});

router.get('/sendLink/:recipient', function(req, res){

  let link = 'http://onelink.to/rjmppd';
  let recipient = req.params.recipient;
  let status = 'fail';

  if(phoneutils.isValidPhone(recipient)){
    phoneutils.send(recipient, 'thanks for your interest in mycard!  The link to install is ' + link);
    status = 'okay';
  }else{
    if(/\@/.test(recipient)){
      status = 'okay';
    }
  }

  logger.log('info', 'sending app link to ' + recipient);
  slack.sendMsg('sending app link to ' + recipient);

  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify({status: status}));
});

router.get('/user/:userid/contacts', function(req, res){
  AddressBook.findOne({userid: req.params.userid}, function(err, addressBook){
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(addressBook));
  });
});


router.post('/contacts', function(req, res){
  var userid = req.body.userid;
  var email = req.body.email;
  var contacts = req.body.contacts;

  User.findOne({userid: userid}, function(err, user){
    if(!user){
    }

    vpcontacts.getVPContacts(user.userid, contacts, function(result){
      user.vpContacts = result;

      user.save(function(err, savedUser){
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({status: 'success'}));
      });
    });
  });
});

router.post('/slack', function(req, res){
  User.findOne({userid: req.body.userid}, function(err, user){
    slack.sendMsg('' + user.email + ' (<https://mycard.vistaprint.com/dashboard/user/' + user.userid + '|dash> | <http://www.us.vpcustomercare.com/vp/manager/customercare_ns/results.aspx?institution_id=1&criteria=email&scriteria=' + user.email + '|cct>) sent a messsage: ' + req.body.message);
  });


  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify({status: 'success'}));
});


router.post('/sendMycardEmail', function(req, res) {
  let newEmail = {
    'recipient': req.body.recipient,
    'cardUrl': req.body.cardUrl,
    'message': req.body.message,
    'senderEmail': req.body.senderEmail,
    'senderName': req.body.senderName,
    'userId': req.body.userId
  };

  queue.create('send_card_email', {email: newEmail});
  res.status(200).send();
});

router.post('/assignReferralUser', function(req, res){

  // first find the userid of the person that is saying they were referred
  User.findOne({userid: req.body.userid}, function(err, user){

    if(user === null){
      logger.log('error', 'Could not find referral user');
      res.setHeader('Content-Type', 'application/json');
      res.status(500).send(JSON.stringify({status: 'failure'}));
      return;
    }

    // Now, find the user that they are saying referred them
    User.findOne({email: req.body.email}, function(err, referralUser){
      if(referralUser === null || err){
        logger.log('error', 'Could not find user for email: ' + req.body.email);
        res.setHeader('Content-Type', 'application/json');
        res.status(500).send(JSON.stringify({status: 'failure'}));
        return;
      }

      // link the user to the person they said referred them
      user.referralUser = referralUser;
      user.save(function(err, newUser){
        if(err){
          logger.log('error', 'Error saving record for ' + req.body.userid);
          res.setHeader('Content-Type', 'application/json');
          res.status(500).send(JSON.stringify({status: 'failure'}));
          return;
        }

        //success
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(newUser));
        
      });
    });
  });
});

router.post('/referral', function(req, res){
  User.findOne({userid: req.body.userid}, function(err, user){
    
    var ref = new Referral({
      user: user,
      email: req.body.email,
      phone: req.body.phone,
      alias: req.body.alias,
      state: 'pending',
    });

    ref.save(function(err, newRef){
      user.referrals.push(ref);

      user.save(function(err, newUser){
        slack.sendMsg('New referral sent by ' + user.email + ' to ' + ref.phone + ' (' + ref.alias + ')');

        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({status: 'success'}));
      });
    });

    io.sockets.emit('dashUpdates', JSON.stringify(user.userid));
  });
});

// Proxy image requests so that we can avoid the image redirect
// bug in react-native
router.get('/proxy', function(req, res){
  var url = decodeURIComponent(req.originalUrl.split('?url=')[1]);
  request.get(url).pipe(res);

});


router.post('/datauri', function(req, res){
  var url = req.body.url;

  //url = 'https://s3.amazonaws.com/vpmycard/bizcards_dev/jontest.png';

  var requestSettings = {
    method: 'GET',
    url: url,
    encoding: null
  };

  request.get(requestSettings, function(err, response, body){
    let datauri = "data:" + response.headers["content-type"] + ";base64," + new Buffer(body).toString('base64');

    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({datauri: datauri}));
  });
});

router.post('/createAccount', function(req, res){
  metrics.inc('create_account_attempt');

  createAccount.createAccount(req.body.email, req.body.password, req.body.emailOptIn, function(err, userJson){
    if(err){
      console.log("sending error: " + JSON.stringify(err));
      metrics.inc('create_account_failure');
      res.setHeader('Content-Type', 'application/json');
      res.status(500).send(JSON.stringify(err));
      return;
    }

    slack.sendMsg(':money_with_wings: Created a new vistaprint account for ' + req.body.email);
    metrics.inc('create_account_success');
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(userJson));
  });
});

router.get('/status', cors(), function(req, res){
  res.json({status: 'online'});
});

router.get('/twilioIncoming', function(req, res){
  slack.sendMsg(':studio_microphone: ' + req.query.From + '( ' + req.query.FromCity + ' ' + req.query.FromState + ' ) sent us an SMS: * ' + req.query.Body + '*');

  res.setHeader('Content-Type', 'text/plain');
  res.send('Thank you for contacting mycard.  This phone number is not currently monitored.  For support please contact mycard@vistaprint.com');
});

router.get('/twilioIncomingVoice', function(req, res){
  res.setHeader('Content-Type', 'text/xml');
  res.send(phoneutils.voiceGreeting());
});

router.post('/twilioVoicemail', function(req, res){
  let sid = req.body.RecordingSid;

  res.setHeader('Content-Type', 'text/xml');
  res.send(phoneutils.voiceRecordingEnd());

  slack.sendMsg(':studio_microphone: ' + req.body.From + '( ' + req.body.FromCity + ' ' + req.body.FromState + ' ) called us');
});

router.post('/twilioTranscribe', function(req, res){
  slack.sendDataMsg({
    icon_url: 'https://twilio.com/marketing/bundles/marketing/img/logos/banner.png',
    attachments: [{
      title: "New voicemail",
      text: 'transcription: ' + req.body.TranscriptionText,

      fields: [
      {
        title: 'From',
        value: req.body.From,
        'short': 'true',
      },
      {
        title: 'Audio',
        value: '<' + req.body.RecordingUrl + '.mp3|download mp3>',
        'short': 'true',
      },
      ]
    }]
  });

  res.status(200).send();
});

router.post('/beacon', async (req, res) => {
  Beacon.find({userid: req.body.userid}).remove().exec();
  new Beacon(req.body).save();

  res.json({status: 'ok'});
});

module.exports = router;

