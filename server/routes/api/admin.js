var axios = require('axios');
var io = require('../../lib/io.js');

var express = require('express');
var router = express.Router();

var config = require('../../config/');
var reporting = require('../../lib/vpreporting.js');
var mongoose = require('mongoose');
var db = require('../../models/db');
var User = mongoose.model('User');
var Push = mongoose.model('Push');
var Referral = mongoose.model('Referral');
var Perfmon = mongoose.model('Perfmon');
var UserToken = mongoose.model('UserToken');
var Beacon = mongoose.model('Beacon');

var migrate = require('../../lib/migrateReporting.js');
var phoneTool = require('../../lib/phone');
var pushutils = require('../../lib/push');
var vpcash = require('../../lib/vpcash');
var tokenAuth = require('../../lib/tokenAuth');
var logger = require('../../lib/logger');

var queue = require('../../lib/queue');

router.get('/users', function(req, res){
  let limit = req.query.limit || 100;
  let offset = req.query.offset || 0;

  User.find({}, {events: 0}).sort({created: -1}).skip(parseInt(offset)).limit(parseInt(limit)).exec(function(err, users){
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send(JSON.stringify(users));
  });
});

router.get('/user/:userid', function(req, res){
  var conditions = {userid: req.params.userid};

  if(req.params.userid.indexOf('@') > 0){
    conditions = {email: req.params.userid};
  }

  User.findOne(conditions).populate(['referrals', 'referralUser', 'vpCash']).exec(function(err, user){
    if(err || user === null){
      res.status(500).send("no user found");
      return;
    }

    queue.create('update_card_cache', {email: user.email});

    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(user));
  });
});

router.delete('/user/:userid/', function(req, res){
  User.find({userid: req.params.userid}).remove().exec();

  res.setHeader('Content-Type', 'application/json');
  res.status(200).send(JSON.stringify({status: "ok"}));
});

router.get('/phone/:pn', function(req, res){
  phoneTool.getPhoneDetails(req.params.pn, function(details){
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send(JSON.stringify(details.carrier));
  });
});

router.post('/user/:email/updateCards', function(req, res) {
  User.findOne({email: req.params.email}, function(err, user){

    if(req.query.force){
      user.business_cards = [];
      user.unordered_business_cards = [];
    }

    reporting.getBusinessCards(user, function(reportingUser){
      let changed = false;

      if(reportingUser.business_cards.length != user.business_cards.length){
        user.business_cards = reportingUser.business_cards;
        changed = true;
      }
      if(reportingUser.unordered_business_cards.length != user.unordered_business_cards.length){
        user.unordered_business_cards = reportingUser.unordered_business_cards;
        changed = true;
      }

      if(changed){
        user.save(function(err, newUser){
          res.status(200).send(JSON.stringify(reportingUser.business_cards));
        });
      }else{
        res.status(200).send(JSON.stringify(user.business_cards));
      }
    });
  });
});

router.post('/createemployeecard', function(req, res) {
  axios({
    method: 'POST',
    url: config.monolithurl + '/hatchery/rpc/mycard/createemployeecard',
    data: req.body,
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(function(resp){
    res.status(200).send(resp.data);
  })
  .catch(function(resp){
    res.status(500).send(resp.data);
  });
});

router.post('/vpcash', function(req, res){
  vpcash.createCashRecord(req.body.userid, function(resp){
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send(JSON.stringify({status: resp}));

    io.sockets.emit('dashUpdates', JSON.stringify(resp));
  });
});

router.post('/updateReferral', function(req, res){

  Referral.findById(req.body._id, function(err, referral){
    console.log("referral found: " + JSON.stringify(referral));
    

    if(referral === null){
      res.setHeader('Content-Type', 'application/json');
      res.status(500).send(JSON.stringify({status: 'error - no records found'}));
      return;
    }

    Object.assign(referral, req.body);

    referral.save(function(err, ref2){
      res.setHeader('Content-Type', 'application/json');
      res.status(200).send(JSON.stringify(ref2));

      io.sockets.emit('dashUpdates', JSON.stringify(ref2));
    });
  });
});

router.get('/perfmons', function(req, res){
  Perfmon.find({}).sort({created: -1}).limit(200).exec(function(err, perfmons){
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send(JSON.stringify(perfmons));
  });
});

router.get('/referrals', function(req, res){
  Referral.find({}).populate('user', 'userid email first_name last_name').sort({created: -1}).limit(200).exec(function(err, referrals){
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send(JSON.stringify(referrals));
  });
});

router.post('/userUpdate', function(req, res){
  console.log("userid: ");
  let userid = req.body.userid;

  User.findOne({userid: userid}, function(err, user){
    console.log("found user: " + user);

    Object.assign(user, req.body);

    user.save(function(err, newUser){
      res.setHeader('Content-Type', 'application/json');
      res.status(200).send(JSON.stringify(newUser));
    });
  });
});

router.post('/forceAppReview', function(req, res){
  let userid = req.body.userid;
  let toStatus = req.body.state;

  let today = new Date();
  let oneMonthAgoApprox = new Date(today.getTime() - (60*60*24*30*1000));

  User.findOne({userid: userid}, function(err, user){

    // Turning off
    if(!toStatus){
      user.forceAppFeedback = false;
      user.save(function(err, newUser){
        res.setHeader('Content-Type', 'application/json');
        res.status(200).send(JSON.stringify({status: !err}));
      });
      return true;
    }

    if(!user.lastShownAppFeedback || user.lastShownAppFeedback < oneMonthAgoApprox){
      user.forceAppFeedback = true;

      user.save(function(err, newUser){
        res.setHeader('Content-Type', 'application/json');
        res.status(200).send(JSON.stringify({status: !err}));
      });

      return;
    }

    res.setHeader('Content-Type', 'application/json');
    res.status(200).send(JSON.stringify({status: false}));
  });
});




// STATS //

router.get('/stats/sends', function(req, res){
  //User.aggregate([
    //{$group: { _id: null, total_sends: {$sum: {$size: '$history'}} }},
  //], function(err, result){
    //if(err){
      //console.log("ERROR: " + err);
      //return;
    //}

    res.setHeader('Content-Type', 'text/plain');
    //res.status(200).send(result[0].total_sends.toString());
    res.status(200).send('1');
  //});
});

router.get('/stats/users', function(req, res){
  //User.aggregate([
    //{$group: { _id: null, total_users: {$sum: 1} }},
  //], function(err, result){
    //if(err){
      //console.log("ERROR: " + err);
      //return;
    //}

    res.setHeader('Content-Type', 'text/plain');
    //res.status(200).send(result[0].total_users.toString());
    res.status(200).send('1');
  //});
});

router.get('/stats/opens', function(req, res){
  //User.aggregate([
    //{$group: { _id: null, microsite_views: {$sum: '$microsite_views'} }},
  //], function(err, result){
    //if(err){
      //console.log("ERROR: " + err);
      //return;
    //}

    res.setHeader('Content-Type', 'text/plain');
    //res.status(200).send(result[0].microsite_views.toString());
    res.status(200).send('1');
  //});
});

router.get('/stats/monthlyUsers', function(req, res){
  //let today = new Date();
  //let oneMonthAgoApprox = new Date(today.getTime() - (60*60*24*30*1000));

  //User.aggregate([
    //{$project: { trial: {$gt: ['$modified', oneMonthAgoApprox.toString()]} }},
    //{$group: { _id: null, activeUsers: {$sum: 1} }},
  //], function(err, result){
    //if(err){
      //console.log("ERROR: " + err);
      //return;
    //}

    res.setHeader('Content-Type', 'text/plain');
    //res.status(200).send(result[0].activeUsers.toString());
    res.status(200).send('1');
  //});
});

router.get('/stats/locations', function(req, res){
  let today = new Date();
  let last24Hours = new Date(today.getTime() - (60*60*24*1000));

  User.find({modified: {$gt: last24Hours}}).select('-_id userid history.location.rawLocation.coords').exec(function(err, users){
    var total = [];

    res.setHeader('Content-Type', 'application/json');

    for(let i=0; i<users.length; i++){
      let user = users[i];

      for(let j=0; j<user.history.length; j++){
        let history = user.history[j];

        if(history.location && history.location.rawLocation && history.location.rawLocation.coords){
          total.push({
            key: i+'-'+j,
            userid: user.userid,
            lat: history.location.rawLocation.coords.latitude,
            lng: history.location.rawLocation.coords.longitude
          });
        }
      }
    }

    res.status(200).send(JSON.stringify(total));
  });
});

router.get('/stats/migration', function(req, res){
  migrate.status().then(function(resp){
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send(resp);
  });
});

router.get('/token/:email', function(req, res){
  var email = decodeURIComponent(req.params.email);

  UserToken.findOne({email: email}, function(err, token){
    if(err || !token){
      res.status(500).send();
      return;
    }

    res.setHeader('Content-Type', 'text/plain');
    res.status(200).send(token.token);
  });
});

router.post('/migrateNow', function(req, res){
  queue.create('migrate_reporting_data', {});
  res.json({status: 'started'});
});

router.post('/token', function(req, res){
  reporting.shopperExists(req.body.email, function(exists) {
    if (!exists) {
      // No vistaprint shopper with this address.
      res.status(500).json({status: 'failed'});
      return;
    }

    tokenAuth.createUserToken(req.body.email, function(token){
      logger.log('info', '[admin] token created for ' + req.body.email + ' : ' + token.token);

      res.json({email: req.body.email, token: token.token});
    });
  });
});

router.get('/beacons', async (req, res) => {
  res.json(await Beacon.find({}));
});

router.get('/bugsnag', async (req, res) => {
  var results = await axios.get('https://api.bugsnag.com/projects/597a0ecd530413002044fe9e/events?auth_token=42a5395f-5bd2-4ef6-9b35-1995f32adbf7&filters[event.since][0]=30d');

  if(req.query.email){
    res.json(results.data.filter( (ele) => { return ele.meta_data.User.email === req.query.email ? 1 : 0}));
    return;
  }

  res.json(results.data);
});


module.exports = router;

