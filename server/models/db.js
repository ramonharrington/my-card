var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

var cfg = require('../config');


mongoose.connect(cfg.connections.mongo, {
  useMongoClient: true
});

var db = mongoose.connection;
var User = require('../models/user.js');
var Push = require('../models/push.js');
var UserToken = require('../models/userToken.js');
var AddressBook = require('../models/addressBook.js');
var Perfmon = require('../models/perfmon.js');
var Referral = require('../models/referral.js');
var VPCash = require('../models/vpcash.js');
var VCard = require('../models/vcard.js');
var Beacon = require('../models/beacon.js');
var AppStat = require('../models/stat.js');
