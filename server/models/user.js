var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var userSchema = mongoose.Schema({
  email: String,
  first_name: String,
  last_name: String,
  shopperkey: String,
  userid: String,
  created: Date,
  modified: Date,
  business_cards: Array,
  unordered_business_cards: Array,
  shorturl: String,
  shorturl_clicks: Number,
  microsite_views: Number,
  default_card: Object,
  settings: Object,
  view_history: Object,
  history: Array,
  badges: Object,
  platform: String,
  appVersion: String,
  buildVersion: String,
  feedback: Array,
  tracking_data: Array,
  referralUser: { type: Schema.Types.ObjectId, ref: 'User' },
  referrals: [{ type: Schema.Types.ObjectId, ref: 'Referral' }],
  vpCash: [{ type: Schema.Types.ObjectId, ref: 'VPCash' }],
  vpContacts: Array,
  timezoneOffset: String,
  forceAppFeedback: { type: Boolean, default: false },
  lastShownAppFeedback: Date,
});

module.exports = mongoose.model('User', userSchema);

