var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var pushSchema = mongoose.Schema({
  userid: String,
  send_dt: Date,
  state: String,
  type: String,
  message: String,
  created: Date,
});

module.exports = mongoose.model('Push', pushSchema);

