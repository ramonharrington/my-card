var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var schema = mongoose.Schema({
  userid: String,
  currentLocation: Object,
  platform: String,
  appVersion: String,
  buildVersion: Number,
  created: { type: Date, default: Date.now, expires: '1h' },
});

module.exports = mongoose.model('Beacon', schema);

