var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var schema = mongoose.Schema({
  created: Date,
  stats: Object,
});

module.exports = mongoose.model('AppStat', schema);

