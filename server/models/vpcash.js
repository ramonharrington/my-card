var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var vpcashSchema = mongoose.Schema({
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  status: String,
  requestdata: Object,
  created: { type: Date, default: Date.now },
});

module.exports = mongoose.model('VPCash', vpcashSchema);

