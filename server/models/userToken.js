var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var userTokenSchema = mongoose.Schema({
  email: String,
  first_name: String,
  last_name: String,
  shopperkey: String,
  token: String,
  created: { type: Date, default: Date.now, expires: '1h' },
});

module.exports = mongoose.model('UserToken', userTokenSchema);

