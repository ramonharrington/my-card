var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var perfmonSchema = mongoose.Schema({
  userid: String,
  email: String,
  name: String,
  events: Array,
  total_sec: Number,
  created: { type: Date, default: Date.now },
});

perfmonSchema.pre('save', function(next){
  if(!this.created) this.created = new Date();
  this.total_sec = (Date.now() - this.created.getTime()) / 1000;

  next();
});

module.exports = mongoose.model('Perfmon', perfmonSchema);

