var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var vcardSchema = mongoose.Schema({
  userid: String,
  alt_doc_id: String,
  is_front_primary: Boolean,
  name: String,
  email: String,
  business_name: String,
  phone: String,
  website: String,
});

module.exports = mongoose.model('VCard', vcardSchema);

