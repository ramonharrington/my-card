var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var addressBookSchema = mongoose.Schema({
  userid: String,
  created: { type: Date, default: Date.now },
  addressBook: Array,
});

module.exports = mongoose.model('AddressBook', addressBookSchema);

