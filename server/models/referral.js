var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var referralSchema = mongoose.Schema({
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  phone: String,
  alias: String,
  state: String,
  created: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Referral', referralSchema);

