#!/usr/bin/env bash

# NOTE -x flag. This will print every command run which makes
# debugging much easier.
set -xeuo pipefail

sudo apt-get update -y
sudo apt-get -y install build-essential zlib1g-dev libssl-dev libreadline6-dev libyaml-dev python-pip jq awscli
#sudo apt-get upgrade -y
#sudo apt-get dist-upgrade -y

### RUBY -- need 2.3 version specifically for codedeploy ###
cd /tmp
wget https://cache.ruby-lang.org/pub/ruby/2.3/ruby-2.3.1.tar.gz
tar -xvzf ruby-2.3.1.tar.gz
cd ruby-2.3.1/
./configure --prefix=/usr/local
make
sudo make install

### NODEJS
sudo apt-get install -y build-essential

### CODE DEPLOY AGENT
wget https://aws-codedeploy-us-east-1.s3.amazonaws.com/latest/install
chmod +x install
sudo ./install auto

sudo touch /var/log/deploy.log
sudo chmod 666 /var/log/deploy.log

mkdir /home/ubuntu/.forever
touch /home/ubuntu/.forever/mycard-webserver.log
touch /home/ubuntu/.forever/mycard-workers.log

### NodeJS
cd /tmp
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs redis-server

# Rebuild startup script
sudo sh -c 'echo "#!/bin/sh -e" > /etc/rc.local'
sudo sh -c 'echo "bash /usr/local/bin/tags2env.sh" >> /etc/rc.local'
sudo sh -c 'echo "frontail /var/log/deploy.log /home/ubuntu/.forever/*.log &" >> /etc/rc.local'

# Install global npm packages
sudo npm install -g forever
sudo npm install -g frontail
sudo npm install -g yarn 


### Imagemagick
sudo apt-get install -y imagemagick pngquant

### Electron, Nightmare, and friends
sudo apt-get install -y libx11-xcb1 libgtk2.0-0 libxss1 libgconf2-4 libnss3 libasound2 unzip libxtst6 xvfb xorg
mkdir /home/ubuntu/electron
cd /home/ubuntu/electron
wget https://github.com/electron/electron/releases/download/v1.7.0/electron-v1.7.0-linux-x64.zip
unzip electron-v1.7.0-linux-x64.zip
sudo ln -s /home/ubuntu/electron/electron /usr/bin/electron
sudo chown -R ubuntu:ubuntu /home/ubuntu/electron




### Prometheus node exporter
sudo apt-get install prometheus-node-exporter -y
update-rc.d prometheus-node-exporter defaults
sudo /etc/init.d/prometheus-node-exporter start


### ubuntu:ubuntu should own everything
sudo chown -R ubuntu:ubuntu /home/ubuntu

## Get ready for next script
sudo chmod 777 /usr/local/bin
