#!/usr/bin/env node

var program = require('commander');
var exec = require('child_process').exec;
var chalk = require('chalk');
var Spinner = require('cli-spinner').Spinner;

program
  .version('0.0.1')
  .option('-b, --build <build>', 'Build Number')
  .option('-g, --group <group>', 'Deployment Group')
  .parse(process.argv);

if(!program.build){
  console.log('Build number missing');
  return 1;
}

if(!program.group){
  console.log('Deployment group missing:  DEV, TEST or PROD: got: ' + program.group);
  return 1;
}


console.log(chalk.green('*')  + ' Uploading new deploy to s3://vpmycard/builds/build_proxy_v' + program.build + '.zip');
exec('aws deploy push --no-ignore-hidden-files --application-name MyCardProxy --s3-location s3://vpmycard/builds/build_proxy_v' + program.build + '.zip --source .', function(error, stdout, stderr){
  if(error){
    console.log(chalk.red('ERROR: ') + stderr);
    return 1;
  }

  console.log(chalk.green('*')  + ' Deploying version ' + program.build + ' to ' + program.group);
  exec('aws deploy create-deployment --application-name MyCardProxy --s3-location bucket=vpmycard,key=builds/build_proxy_v' + program.build + '.zip,bundleType=zip --deployment-group-name ' + program.group, function(err, out, errmsg){

    if(err){
      console.log(chalk.red('ERROR: ') + errmsg);
      return 1;
    }

    var deployId = JSON.parse(out).deploymentId;

    var spinner = new Spinner(chalk.blue('\t* ') + ' deploying  %s');

    setInterval(function(){
      exec('aws deploy get-deployment --deployment-id ' + deployId, function(err1, out1, errmsg1){
        if(err1){
          console.err(chalk.red('ERROR: ') + errmsg1);
          process.exit(1);
        }

        var status = JSON.parse(out1);

        if(status.deploymentInfo.status === 'Failed'){
          spinner.stop();
          console.log(chalk.red('\n** DEPLOYMENT FAILED **  ') + status.deploymentInfo.errorInformation.message);
          process.exit(1);
        }

        if(status.deploymentInfo.status === 'Succeeded'){
          spinner.stop();
          console.log(chalk.green('\n** DEPLOYMENT SUCCESS **'));
          process.exit(0);
        }

        spinner.start();
      });
    }, 20000);
  });


});

