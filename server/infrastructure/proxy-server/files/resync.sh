HOST="mycard.vistaprint.com"
USER="ubuntu"
IDENTITY="~/.ssh/hatchery-ec2-nvirginia.pem"

ssh -i $IDENTITY $USER@$HOST "sudo tar czpf - /etc/letsencrypt" > letsencrypt.tgz
scp -i $IDENTITY $USER@$HOST:/etc/ssl/certs/dhparam.pem ./
scp -i $IDENTITY $USER@$HOST:/etc/nginx/sites-enabled/mycard-prod ./mycard-prod


