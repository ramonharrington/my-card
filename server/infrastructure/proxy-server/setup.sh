#!/usr/bin/env bash

# NOTE -x flag. This will print every command run which makes
# debugging much easier.
set -xeuo pipefail

sudo apt-get update -y
sudo apt-get -y install build-essential zlib1g-dev libssl-dev libreadline6-dev libyaml-dev python-pip

### RUBY -- need 2.3 version specifically for codedeploy ###
cd /tmp
wget https://cache.ruby-lang.org/pub/ruby/2.3/ruby-2.3.1.tar.gz
tar -xvzf ruby-2.3.1.tar.gz
cd ruby-2.3.1/
./configure --prefix=/usr/local
make
sudo make install

### CODE DEPLOY AGENT
wget https://aws-codedeploy-us-east-1.s3.amazonaws.com/latest/install
chmod +x install
sudo ./install auto

sudo touch /var/log/deploy.log
sudo chmod 666 /var/log/deploy.log

### Nginx and friends install and setup
sudo apt-get install -y nginx letsencrypt
sudo rm /etc/nginx/sites-enabled/default

### Prometheus node exporter
sudo apt-get install prometheus-node-exporter -y
update-rc.d prometheus-node-exporter defaults
sudo /etc/init.d/prometheus-node-exporter start

# example letsencrypt first time:
# sudo letsencrypt certonly -a webroot --webroot-path=/var/www/html -d mycard.vistaprint.com -d mycard.vistaprint.io -d api.mycard.vistaprint.io
