provider "aws" {
  access_key = "AKIAJI2L2ZXE45SOOJGQ"
  secret_key = "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"
  region     = "us-east-1"
}

resource "aws_instance" "mycard_nginx" {
  ami           = "ami-28b89f4d"
  instance_type = "t2.micro"
  subnet_id     = "subnet-9a53f7d3"
  associate_public_ip_address = "true"
  key_name      = "hatchery-ec2-nvirginia"
  vpc_security_group_ids = ["sg-a92829d3"]
  iam_instance_profile = "hatchery_ec2"

  tags {
    Name = "mycard-dev-proxy"
  }
}

resource "aws_instance" "mycard_webserver" {
  ami           = "ami-72c1b964"
  instance_type = "t2.small"
  subnet_id     = "subnet-61866c3a"
  associate_public_ip_address = "false"
  key_name      = "hatchery-ec2-nvirginia"
  vpc_security_group_ids = ["sg-5744332d"]
  iam_instance_profile = "hatchery_ec2"

  tags {
    Name = "mycard-dev-webserver1"
    HatcheryEnvironment = "mycard-dev-webserver"
  }
}
resource "aws_route53_record" "webserver-dev" {
  zone_id = "ZSF6ZVUELYC2D"
  name    = "webserver.dev"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.mycard_webserver.private_ip}"]
}



resource "aws_route53_record" "dev" {
  zone_id = "ZSF6ZVUELYC2D"
  name    = "dev"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.mycard_nginx.public_ip}"]
}

output "proxy_ip" {
  value = "${aws_instance.mycard_nginx.public_ip}"
}

output "webserver_ip" {
  value = "${aws_instance.mycard_webserver.private_ip}"
}

