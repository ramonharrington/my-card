provider "aws" {
  access_key = "AKIAJI2L2ZXE45SOOJGQ"
  secret_key = "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"
  region     = "us-east-1"
}

resource "aws_instance" "mycard_nginx" {
  ami           = "ami-02007a14"
  instance_type = "t2.small"
  subnet_id     = "subnet-9a53f7d3"
  associate_public_ip_address = "true"
  key_name      = "hatchery-ec2-nvirginia"
  vpc_security_group_ids = ["sg-a92829d3"]
  iam_instance_profile = "hatchery_ec2"

  tags {
    Name = "mycard-prod-proxy"
  }
}

resource "aws_instance" "mycard_webserver1" {
  ami           = "ami-7271cc08"
  instance_type = "t2.medium"
  subnet_id     = "subnet-61866c3a"
  associate_public_ip_address = "false"
  key_name      = "hatchery-ec2-nvirginia"
  vpc_security_group_ids = ["sg-5744332d"]
  iam_instance_profile = "hatchery_ec2"

  tags {
    Name = "mycard-prod-webserver1"
    HatcheryEnvironment = "mycard-prod-webserver"
    NODE_ENV = "production"
  }
}
resource "aws_route53_record" "webserver_1" {
  zone_id = "ZSF6ZVUELYC2D"
  name    = "webserver1"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.mycard_webserver1.private_ip}"]
}

resource "aws_instance" "mycard_webserver2" {
  ami           = "ami-7271cc08"
  instance_type = "t2.medium"
  subnet_id     = "subnet-61866c3a"
  associate_public_ip_address = "false"
  key_name      = "hatchery-ec2-nvirginia"
  vpc_security_group_ids = ["sg-5744332d"]
  iam_instance_profile = "hatchery_ec2"

  tags {
    Name = "mycard-prod-webserver2"
    HatcheryEnvironment = "mycard-prod-webserver"
    NODE_ENV = "production"
  }
}
resource "aws_route53_record" "webserver_2" {
  zone_id = "ZSF6ZVUELYC2D"
  name    = "webserver2"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.mycard_webserver2.private_ip}"]
}

resource "aws_instance" "mycard_webserver3" {
  ami           = "ami-8d010ff6"
  instance_type = "t2.medium"
  subnet_id     = "subnet-61866c3a"
  associate_public_ip_address = "false"
  key_name      = "hatchery-ec2-nvirginia"
  vpc_security_group_ids = ["sg-5744332d"]
  iam_instance_profile = "hatchery_ec2"

  tags {
    Name = "mycard-prod-webserver3"
    HatcheryEnvironment = "mycard-prod-webserver"
    NODE_ENV = "production"
  }
}
resource "aws_route53_record" "webserver_3" {
  zone_id = "ZSF6ZVUELYC2D"
  name    = "webserver3"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.mycard_webserver3.private_ip}"]
}

resource "aws_route53_record" "prod" {
  zone_id = "ZSF6ZVUELYC2D"
  name    = "."
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.mycard_nginx.public_ip}"]
}

output "proxy_ip" {
  value = "${aws_instance.mycard_nginx.public_ip}"
}

output "webserver1_ip" {
  value = "${aws_instance.mycard_webserver1.private_ip}"
}

output "webserver2_ip" {
  value = "${aws_instance.mycard_webserver2.private_ip}"
}

output "webserver3_ip" {
  value = "${aws_instance.mycard_webserver3.private_ip}"
}
