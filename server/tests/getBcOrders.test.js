var rep = require('../lib/vpreporting');

test('get two business cards for account', () => {
  rep.getBusinessCards('efgaudette@gmail.com', function(json){
    return expect(json.business_cards.length).toBe(2);
  });
});

test('images of cards have cimpress urls', () => {
  rep.getBusinessCards('efgaudette@gmail.com', function(json){
    return expect(json.business_cards[0].front_img).toContain('cimpress.io');
  });
});
